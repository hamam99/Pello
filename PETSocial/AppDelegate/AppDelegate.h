//
//  AppDelegate.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

typedef void(^myCompletion)(BOOL);


@property (strong, nonatomic) UIWindow *window;


- (void)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

-(void)pushTabBar:(UINavigationController *)navController;

@property (strong, nonatomic) UITabBarController *mainTab;
@property (strong, nonatomic) UIImageView *imagefooter;
@property (strong, nonatomic) NSDictionary *remoteNotif;
@property (nonatomic,strong) UIImageView *viewNewNotification;

@property (nonatomic,strong) UILabel *lblNotificationCount;
@property (nonatomic,strong) NSTimer *alertTimer;
@property (nonatomic,assign) BOOL isNotificationCount;
@property (nonatomic,readwrite) BOOL userHaveIntegrataedFacebookAccountSetup;
@property (nonatomic,strong) NSString *FBConnectionStatus;

@property (nonatomic,assign) BOOL isNewFeed;
@property (nonatomic,assign) BOOL isProfileEdited;
@property (nonatomic,strong) NSMutableURLRequest *objCurrentNotificationReq;

-(void)ShowHUDWith:(NSString*)strtitle;
-(void)ShowHUDForAlertWithErrorTitel:(NSString*)strtitle;
-(void)showHudForAlertWithSuccessTitle:(NSString*)strtitle;
-(void)ShareOthersPostOnFB;
-(void)startPostOnFB;



@property (strong, nonatomic) NSDictionary *shareData;
- (BOOL)openSessionWithFriends:(BOOL)allowLoginUI;
-(void)facebookCheckIN;
@end
