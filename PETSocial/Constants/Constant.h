
//
//  Constant.h
//  
//


#import <Foundation/Foundation.h>

#define MenuMainTitleHeight 65
#define MenuSubTitleHeight 56
#define BtnMainMenuHeight 75
#define TitleShare @"Share"

#define FromDiscoverLiveStream 1212
#define FromLiveStream 1313
#define FromNormalVideo 1414

#pragma mark - Application Usage -
#pragma mark - Application Usage -


// Client URL
//#define BASE_URL @"http://inheritx.dnsdynamic.com:8590/petsocialnew/index.php?do="
//http://inheritxdev.net
//#define BASE_URL @"http://inheritxdev.net/petsocialnew/index.php?do="
// Our testing URL
//#define BASE_URL @"http://inheritx.dnsdynamic.com:8590/petsocialinx/index.php?do="
// Live url
//#define BASE_URL @"http://inheritxdev.net/petsocialinx/index.php?do="

//Client Live URL
//#define BASE_URL @"http://admin.pelloapp.com/index.php?do="
#define BASE_URL @"http://52.34.205.159/index.php?do="
//#define BASE_URL @"http://192.168.0.194/index.php?do="

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


#define REAL_BASE_URL @"http://52.34.205.159"

#define kOAuthConsumerKey @"J3k4aS4dguNPFjJPuPRQUGPiK"
#define kOAuthConsumerSecret @"pkAJIIyMeDqafgx2fH9MCvTrCKTaqvSZoACBilmjAS8NgF6EUH"

#define FBSessionStateChangedNotification @"FBSessionStateChangedNotification"
#define FBFindFriendsNotification @"FBFindFriendsNotification"
#define DeletePost @"DeletePost"

#define kTwitterConsumerKey @"es3blqO3sFThiTEDoIsA25Jbj"
#define kTwitterConsumerSecret @"QlueiqEjbh6KFTc9GYzNtx1YxQfIeQEMK0HVVc1ZgFSUJCdQRu"
#define kTwitterAccessToken @"3712353253-E8hpyOQAhdpikxQ35HZAJpDZXlWkbEWbvQ9BsCo"
#define kTwitterAccessTokenSecret @"3P4a17nyYIgdcZQZ9NkZLMfw5GOsf1Uigse1RzlAR9FEr"

#define iPhone5 [[UIScreen mainScreen] bounds].size.height == 568.0f
#define iPhone4 [[UIScreen mainScreen] bounds].size.height == 480.0f

#define iOS7 [[UIDevice currentDevice].systemVersion floatValue] >= 7.0
#define iOS5 [[UIDevice currentDevice].systemVersion floatValue] >= 5.0 && [[UIDevice currentDevice].systemVersion floatValue] < 6.0

#define iOS6 [[UIDevice currentDevice].systemVersion floatValue] >= 6.0 && [[UIDevice currentDevice].systemVersion floatValue] < 7.0

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define FILEMANAGER [NSFileManager defaultManager]

#define APPDELEGATE (AppDelegate*)[[UIApplication sharedApplication] delegate]

#define KEYUSERDETAILS @"UserDetails"

#define KEYISLOGEDIN @"ISLOGEDIN"
#define KEYLOGEDINYES @"YES"
#define KEYLOGEDINNO @"NO"

#define APPDELEGATE (AppDelegate*)[[UIApplication sharedApplication] delegate]
#define DEFAULTFONT [UIFont fontWithName:@"DaxMedium" size:24]
#define DEFAULTTIMEOUT 60.0

#pragma mark DEFAULT COLOR
#define kDEFAULT_BLUE_COLOR [UIColor colorWithRed:51.0/255.0 green:176.0/255.0 blue:222.0/255.0 alpha:1.0] 

#pragma mark DEFAUTL VIDEO TIMER
#define kDefaultVideoDuration 15

#pragma mark ALERT MESGS

#define ALERTTITLE @"Pello"
#define ALERTIMAGE @"Please add photo"
#define ALERTEMAILBLANK @"Please enter e-mail address"
#define ALERTPASSWORDBLANK @"Please enter password"
#define ALERTUSERNAMEBLANK @"Please enter username"
#define ALERTEMAILINVALID @"Please enter valid e-mail address"
#define ALERTUSERNAMEEMAILBLANK @"Please enter username"

#define ALERTNETWORKCONNECTION @"No Internet Connection. Try After Sometime."
#define ALERTTWITTERMESG @"If you would like to Login, please Sign In with Twitter app first."
#define ALERTFORGOTPASSWORD @"Password reset link sent on your account"




#pragma mark - Request Keys -
#pragma mark KEYS

#define KEYERROR @"error"
#define KEYUSERINFO @"UserInfo"

#define likeBtnTag 123
#define disLikeBtnTag 321
#define collectionItems 15

#pragma mark API

#define APILOGIN @"/webservice/login"
#define APIFORGOTPASSWORD @"/webservice/forgotpassword"
#define APIREGISTRATION @"/webservice/register"
#define APIPOSTSTATUS @"/webservice/post"
#define APIPOSTVIDEO @"/webservice/postvideo"
#define APIPOSTLIST @"/webservice/postlist"
#define APILIKEDISLIKE @"/webservice/wslike&type=1"
#define APILIKER @"/webservice/wslike&type=2"
#define APILOCATION @"/webservice/locationpostlist"
#define APIGETCOMMENT @"/webservice/imagecomment&type=getcomment"
#define APIADDCOMMENT @"/webservice/imagecomment&type=postcomment"
#define APIDELETECOMMENT @"/webservice/imagecomment&type=deletecomment"
#define APINOTIFICATIONLIST @"/webservice/notification"
#define APIEXPLORERLIST @"/webservice/explorer"
#define APISINGLEPOST @"/webservice/photodetail"
#define APISEARCHPROFILE @"/webservice/searchprofile"
#define APISEATCHBYHASHTAG @"/webservice/explorer&type=tag"

#define APIUSERSPOSTLIST @"/webservice/userinfo&type=posted"
#define APITAGGESPOSTLIST @"/webservice/userinfo&type=tagged"
#define APIPOSTLISTHASHTAG @"/webservice/postlisthashtag"


#define APIFOLLOWING @"/webservice/follow&type=following"
#define APIFOLLOWER @"/webservice/follow&type=follower"

#define APIUSERSDETAIL @"/webservice/userinfo&type=detail"
#define APIBLOCKUSER @"/webservice/block"

//milap
#define APIFOllOWUNFOLLOW @"/webservice/follow"
#define APIEDITPROFILE @"/webservice/editprofile"
#define APICHANGEPASSWORD @"/webservice/changepassword"
#define APIBLOCKUSERLIST @"/webservice/block&type=list"

#define APIFBFriends @"/webservice/userinfo&type=fb_friends_details"
#define APINotificationCount @"/webservice/notification&type=noficication_count"
#define APINotificationCountVersion2 @"/index.php?do=/webservice/notification&type=noficication_count"
#define APISeenNotificationCount @"/webservice/notification&type=update"

#define APIDELETEPOST @"/webservice/photodetail&type=deletePost"

#define RESPONSESUCCESS @"status"
#define RESPONSEMESSAGE @"Error"


#pragma mark STATE

#define STATELOGIN @"StateLogin"
#define STATEFORGOTPASSWORD @"ForgotPassword"
#define STATEREGISTRATION @"Registration"
#define STATEPOSTSTATUS @"PostStatus"
#define STATEPOSTVIDEO @"PostVideo"
#define STATEREVERSEPLACESEARCH @"REVERSEGEOCODE"
#define STATEFEEDLIST @"SteteFeedList"
#define STATELIKEDISLIKE @"Like-Dislike"
#define STATELIKER @"Liker"
#define STATELOCATION @"LocationData"
#define STATEGETCOMMENT @"GetComment"
#define STATEGETCOMMENTWITHPHOTOID @"GetCommentWithPhotoID"
#define STATEADDCOMMENT @"Postcomment"
#define STATEDELETECOMMENT @"Deletecomment"
#define STATENOTIFICATON @"NotificationList"
#define STATEEXPLORER @"ExplorerList"
#define STATESINGLEPOST @"SinglePostData"
#define STATESEARCHBYNAME @"SearchByName"
#define STATESEARCHNYHASHTAG @"SearchByHashTag"
#define STATEALLPOST @"StateUserPost"
#define STATEALLPOSTHASHTAG @"StateUserPostHashtag"
#define STATETAGGEDPOST @"StateTaggedPost"
#define STATEFOLLOWING @"StateFollowing"
#define STATEFOLLOWER @"StateFollower"
#define STATEUSERDETAIL @"StateUSerDetail"
#define STATEBLOCKUSER @"BlockUser"

// Harshit
#define STATEREPORTABUSE @"ReportAbuse"

//harshit
#define APIREPORTABUSE @"/webservice/abuse&type=post"

// report
#define STATEGETREPORTCATEGORY @"ReportCategory"
#define APIREPORTCATEGORY @"/webservice/reportcategory"
#define STATEREPORTPOST @"ReportPost"
#define APIREPORTPOST @"/webservice/report"


//milap
#define STATEFOllOWUNFOLLOW @"follow"
#define STATEEDITPROFILE @"StateEditProfile"
#define STATECHANGEPASSWORD @"ChangePassword"
#define STATEBLOCKUSERLIST @"BlockUserList"
#define STATENOTIFICATIONCOUNT @"NotificationCount"
#define STATESEENNOTIFICATIONCOUNT @"SeenNotificationCount"

#define kDefaultConnectionError @"Request processing failed. Please try again later mate."

#define STATEFBFriends @"FBFriends"
#define STATEDELETEPOST @"DeletePost"
#pragma mark - Request Keys -


@interface Constant : NSObject
+(BOOL) validateEmail: (NSString *) candidate;
+(NSString *)makeTinyURL:(NSString *) originalURL;
+(NSString *)UTCtoDeviceTimeZone:(NSString *)UTCTime;
+(UIStatusBarStyle)preferredStatusBarStyle;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;
+ (NSString*) durationbetweendate: (NSString *) candidate;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (UIImage*)cropImage:(UIImage*)imageTaked;
@end
