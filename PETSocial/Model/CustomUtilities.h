//
//  CustomUtilities.h
//  PETSocial
//
//  Created by Habibi on 12/5/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomUtilities : NSObject
+ (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize;
+ (CGSize)getSizeForLabel:(UILabel *)theLabel forMaxSize:(CGSize)maxSize;
@end
