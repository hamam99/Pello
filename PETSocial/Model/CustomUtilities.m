//
//  CustomUtilities.m
//  PETSocial
//
//  Created by Habibi on 12/5/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import "CustomUtilities.h"

@implementation CustomUtilities

/*! Returns the size of the label to display the text provided
 @param text
 The string to be displayed
 @param width
 The width required for displaying the string
 @param fontName
 The font name for the label
 @param fontSize
 The font size for the label
 */
+ (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = MAXFLOAT;
    constraintSize.width = width;
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}

+ (CGSize)getSizeForLabel:(UILabel *)theLabel forMaxSize:(CGSize)maxSize {
    CGSize sized;
    
    //CGSize maxSize = CGSizeMake(212,NSUIntegerMax);
    if ([theLabel.text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineBreakMode:theLabel.lineBreakMode];
        [style setAlignment:theLabel.textAlignment];
        NSDictionary *attributes = @{ NSFontAttributeName:theLabel.font,
                                      NSParagraphStyleAttributeName:style
                                      };
        sized = [theLabel.text boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
    }
    else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        sized = [theLabel.text sizeWithFont:theLabel.font constrainedToSize:maxSize lineBreakMode:theLabel.lineBreakMode];
#pragma clang diagnostic pop
    }
    
    return sized;
}

@end
