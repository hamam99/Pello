
#import <Foundation/Foundation.h>

@interface NSArray (HAAddition)
- (id) arrayByReplacingNullsWithStrings;
@end
