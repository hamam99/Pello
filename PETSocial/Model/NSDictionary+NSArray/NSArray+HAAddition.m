

#import "NSArray+HAAddition.h"

@implementation NSArray (HAAddition)
- (id) arrayByReplacingNullsWithStrings
{
    //    const id replaced = nil;
    if ([self isKindOfClass:[NSArray class]] || [self isKindOfClass:[NSMutableArray class]])
    {
        const NSMutableArray *replaced = [NSMutableArray arrayWithArray:(NSArray*)self];
        //        replaced = [NSMutableArray arrayWithArray:self];
        for (int i = 0; i< [replaced count]; i++)
        {
            NSDictionary* dict = [replaced objectAtIndex:i];
            dict = [(NSDictionary *) dict dictionaryByReplacingNullsWithStrings];
            [replaced replaceObjectAtIndex:i withObject:dict];
        }
        return [replaced copy];
    }
    return nil;
}
@end
