
#import <Foundation/Foundation.h>

@interface NSDictionary (HAAddition)
- (NSDictionary *) dictionaryByReplacingNullsWithStrings;
- (id) dictionaryAndArrayByReplacingNullsWithStrings;
@end
