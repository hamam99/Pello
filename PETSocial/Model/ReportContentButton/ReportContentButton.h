//
//
//  ReportContentButton.h
//  PETSocial
//
//  Created by Administrator on 04/02/15.
//  Copyright (c) 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CompletionBlock)(BOOL success);

@interface ReportContentButton : UIButton

@property (assign, nonatomic) NSInteger intUserId;
@property (assign, nonatomic) NSInteger intItemId;

@end
