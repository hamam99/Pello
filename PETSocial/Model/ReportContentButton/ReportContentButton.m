//
//  ReportContentButton.m
//  PETSocial
//
//  Created by Administrator on 04/02/15.
//  Copyright (c) 2015 Ravi B. All rights reserved.
//

#import "ReportContentButton.h"
#import "Alerts.h"
#import "AFNetworking.h"

@implementation ReportContentButton

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self initializeOnce];
    return self;
}

- (void)awakeFromNib
{
    [self initializeOnce];
}

- (void)initializeOnce
{
    [self addTarget:self action:@selector(showReportingOptions:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)showReportingOptions:(id)sender
{
    UIActionSheet *actionSheet =  [Alerts actionSheetWithBlock:^(NSInteger buttonIndex) {
        
        switch (buttonIndex) {
            case 0:
                // report abuse 1
                [self reportAbuse:nil];
                break;
            case 1:
                // report abuse 2
                [self reportAbuse:nil];
                break;
            case 2:
                // report abuse 3
                [self reportAbuse:nil];
                break;
            default:
                break;
        }
        
    } andButtons:@"Racism or sexual content",@"Spam or fraud",@"Violence or abuse", nil];
    actionSheet.title = @"Report Content";
    [actionSheet showInView:self.superview];
}

- (void)reportAbuse:(CompletionBlock)pCompletionBlock
{
    NSMutableDictionary *dictParams = [NSMutableDictionary dictionary];
    [dictParams setObject:[NSString stringWithFormat:@"%ld",(long)_intUserId] forKey:@"user_id"];
    [dictParams setObject:[NSString stringWithFormat:@"%ld",(long)_intItemId] forKey:@"item_id"];
    
    [[Connection sharedConnectionWithDelegate:self] postReportAbuse:dictParams];
    
    /*   http://inheritxdev.net/petsocialnew/index.php?do=/webservice/abuse&type=post
    NSString *strRequestUrl = @"http://inheritxdev.net/petsocialinx/index.php?do=/webservice/abuse&type=post";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:strRequestUrl parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject){
        
     
        
        if (pCompletionBlock)
            pCompletionBlock(YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
     
        if (pCompletionBlock)
            pCompletionBlock(NO);
    }];*/
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEREPORTABUSE]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [Alerts showAlertWithMessage:@"Post reported successfully" withBlock:nil andButtons:@"Ok", nil];
                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:DeletePost object:nil];
                }
                else{
                    //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
       
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
      
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
//    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:nData];
}


@end