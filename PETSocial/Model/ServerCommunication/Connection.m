//
//  Connection.m
//  scatterwall
//
//  Created by Himanshu Jadav on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Connection.h"
#import "JSON.h"



@interface Connection ()
-(void)requestWithURL:(NSURL*)URL method:(NSString*)method data:(NSDictionary*)dataDict state:(NSString*)state;
@end

@implementation Connection

@synthesize connectionDelegate;
@synthesize state;
//@synthesize mutableData;
static Connection *sharedConnection = nil;

+ (Connection*)sharedConnection
{
    //    sharedConnection = [[Connection alloc] init];
    if (!sharedConnection)
    {
        sharedConnection = [[Connection alloc] init];
    }
	return sharedConnection;
}
+ (Connection*)sharedConnectionWithDelegate:(id)delegate
{
    if (!sharedConnection)
    {
        sharedConnection = [[Connection alloc] init];
    }
    sharedConnection.connectionDelegate = delegate;
	return sharedConnection;
}

#pragma mark Webservice Methods
-(void)deletePost:(NSDictionary *)dict{
    [self setState:STATEDELETEPOST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIDELETEPOST]] method:@"POST" data:dict state:STATEDELETEPOST];
}

-(void)getFBFriendList:(NSDictionary *)dict{
//http://inheritx.dnsdynamic.com:8590/petsocialnew/index.php?do=/webservice/userinfo&type=fb_friends_details

    [self setState:STATEFBFriends];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIFBFriends]] method:@"POST" data:dict state:STATEFBFriends];
}

-(void)getReverseGeocodingFromGoogle:(NSString *)string{
    [self setState:STATEREVERSEPLACESEARCH];
    [self requestWithURL:[NSURL URLWithString:string] method:@"POST" data:nil state:STATEREVERSEPLACESEARCH];
}
-(void)loginWithData:(NSDictionary*)dataDict
{
    [self setState:STATELOGIN];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APILOGIN]] method:@"POST" data:dataDict state:STATELOGIN];
}
-(void)registerWithData:(NSDictionary*)dataDict
{
    [self setState:STATEREGISTRATION];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIREGISTRATION]] method:@"POST" data:dataDict state:STATEREGISTRATION];
}

-(void)registerWithFacebookData:(NSDictionary*)dataDict
{
    [self setState:STATEREGISTRATION];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIREGISTRATION]] method:@"POST" data:dataDict state:STATEREGISTRATION];
}
-(void)forgotPasswordWithData:(NSDictionary*)dataDict{
    [self setState:STATEFORGOTPASSWORD];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIFORGOTPASSWORD]] method:@"POST" data:dataDict state:STATEFORGOTPASSWORD];
}
-(void)postStatusWithImage:(NSDictionary *)dataDict{
    [self setState:STATEPOSTSTATUS];
    [self postStatusWithImage:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIPOSTSTATUS]] method:@"POST" data:dataDict state:STATEPOSTSTATUS withImagekey:@"post_image"];
    //    [self requestWithURLandImage:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIPOSTSTATUS]] method:@"POST" data:dataDict state:STATEPOSTSTATUS];
}
-(void)getFeedList:(NSDictionary*)dataDict
{
    [self setState:STATEFEEDLIST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIPOSTLIST]] method:@"POST" data:dataDict state:STATEFEEDLIST];
}

-(void)likeDislike:(NSDictionary*)dataDict
{
    [self setState:STATELIKEDISLIKE];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APILIKEDISLIKE]] method:@"POST" data:dataDict state:STATELIKEDISLIKE];
}
-(void)locationData:(NSDictionary*)dataDict
{
    [self setState:STATELOCATION];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APILOCATION]] method:@"POST" data:dataDict state:STATELOCATION];
}
-(void)GetComment:(NSDictionary*)dataDict
{
    [self setState:STATEGETCOMMENT];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIGETCOMMENT]] method:@"POST" data:dataDict state:STATEGETCOMMENT];
}
-(void)GetComment:(NSDictionary*)dataDict forPhotoID:(NSString*)photoID
{
    NSString *customState = [NSString stringWithFormat:@"%@-%@", STATEGETCOMMENTWITHPHOTOID, photoID];
    [self setState:customState];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIGETCOMMENT]] method:@"POST" data:dataDict state:customState];
}
-(void)AddComment:(NSDictionary*)dataDict
{
    [self setState:STATEADDCOMMENT];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIADDCOMMENT]] method:@"POST" data:dataDict state:STATEADDCOMMENT];
}
-(void)DeleteComment:(NSDictionary*)dataDict
{
    [self setState:STATEDELETECOMMENT];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIDELETECOMMENT]] method:@"POST" data:dataDict state:STATEDELETECOMMENT];
}
-(void)getNotificationList:(NSDictionary*)dataDict
{
    [self setState:STATENOTIFICATON];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APINOTIFICATIONLIST]] method:@"POST" data:dataDict state:STATENOTIFICATON];
}

-(void)getExplorerList:(NSDictionary*)dataDict
{
    [self setState:STATEEXPLORER];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIEXPLORERLIST]] method:@"POST" data:dataDict state:STATEEXPLORER];
}
-(void)getSinglePostData:(NSDictionary*)dataDict
{
    [self setState:STATESINGLEPOST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APISINGLEPOST]] method:@"POST" data:dataDict state:STATESINGLEPOST];
}
-(void)findPeopleUsingName:(NSDictionary*)dataDict
{
    [self setState:STATESEARCHBYNAME];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APISEARCHPROFILE]] method:@"POST" data:dataDict state:STATESEARCHBYNAME];
}
-(void)searchUsingHashtag:(NSDictionary*)dataDict
{
    [self setState:STATESEARCHNYHASHTAG];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APISEATCHBYHASHTAG]] method:@"POST" data:dataDict state:STATESEARCHNYHASHTAG];
}

-(void)getPostOfUser:(NSDictionary*)dataDict
{
    [self setState:STATEALLPOST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIUSERSPOSTLIST]] method:@"POST" data:dataDict state:STATEALLPOST];
}
-(void)getPostHashtagOfUser:(NSDictionary*)dataDict
{
    [self setState:STATEALLPOSTHASHTAG];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIPOSTLISTHASHTAG]] method:@"POST" data:dataDict state:STATEALLPOSTHASHTAG];
}

-(void)getLikerOfPost:(NSDictionary*)dataDict
{
    [self setState:STATELIKER];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APILIKER]] method:@"POST" data:dataDict state:STATELIKER];
}

-(void)postReportAbuse:(NSDictionary*)dataDict
{
    [self setState:STATEREPORTABUSE];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIREPORTABUSE]] method:@"POST" data:dataDict state:STATEREPORTABUSE];
}

-(void)getReportCategory:(NSDictionary*)dataDict
{
    [self setState:STATEGETREPORTCATEGORY];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIREPORTCATEGORY]] method:@"POST" data:dataDict state:STATEGETREPORTCATEGORY];
}

-(void)postReportPost:(NSDictionary*)dataDict
{
    [self setState:STATEREPORTPOST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIREPORTPOST]] method:@"POST" data:dataDict state:STATEREPORTPOST];
}

-(void)getTaggedPostOfUser:(NSDictionary*)dataDict
{
    [self setState:STATETAGGEDPOST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APITAGGESPOSTLIST]] method:@"POST" data:dataDict state:STATETAGGEDPOST];
}
-(void)followingList:(NSDictionary*)dataDict
{
    [self setState:STATEFOLLOWING];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIFOLLOWING]] method:@"POST" data:dataDict state:STATEFOLLOWING];
}
-(void)followerList:(NSDictionary*)dataDict
{
    [self setState:STATEFOLLOWER];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIFOLLOWER]] method:@"POST" data:dataDict state:STATEFOLLOWER];
}
-(void)getUserDetail:(NSDictionary*)dataDict
{
    [self setState:STATEUSERDETAIL];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIUSERSDETAIL]] method:@"POST" data:dataDict state:STATEUSERDETAIL];
}
-(void)blockUser:(NSDictionary*)dataDict
{
    [self setState:STATEBLOCKUSER];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIBLOCKUSER]] method:@"POST" data:dataDict state:STATEBLOCKUSER];
}
//milap
-(void)FollowUnFollowUser:(NSDictionary*)dataDict
{
    [self setState:STATEFOllOWUNFOLLOW];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIFOllOWUNFOLLOW]] method:@"POST" data:dataDict state:STATEFOllOWUNFOLLOW];
}
-(void)editProfileDetail:(NSDictionary*)dataDict
{
    [self setState:STATEEDITPROFILE];
    [self postStatusWithImage:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIEDITPROFILE]] method:@"POST" data:dataDict state:STATEPOSTSTATUS withImagekey:@"profile_image"];
//    [self editProfileWithImage:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIEDITPROFILE]] method:@"POST" data:dataDict state:STATEEDITPROFILE];
}
//=/webservice/changepassword
-(void)changePassword:(NSDictionary *)dataDict{
    [self setState:STATECHANGEPASSWORD];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APICHANGEPASSWORD]] method:@"POST" data:dataDict state:STATECHANGEPASSWORD];

}
-(void)blockUserList:(NSDictionary*)dataDict
{
    [self setState:STATEBLOCKUSERLIST];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIBLOCKUSERLIST]] method:@"POST" data:dataDict state:STATEBLOCKUSERLIST];
}
-(void)notificationCount:(NSDictionary*)dataDict
{
    [self setState:STATENOTIFICATIONCOUNT];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APINotificationCount]] method:@"POST" data:dataDict state:STATENOTIFICATIONCOUNT];
}
-(void)seenNotificationCount:(NSDictionary*)dataDict
{
    [self setState:STATESEENNOTIFICATIONCOUNT];
    [self requestWithURL:[NSURL URLWithString:[BASE_URL stringByAppendingString:APISeenNotificationCount]] method:@"POST" data:dataDict state:STATESEENNOTIFICATIONCOUNT];
}
#pragma mark RequestURL methods


-(void)requestWithURL:(NSURL*)URL method:(NSString*)method data:(NSDictionary*)dataDict state:(NSString*)state1
{
    
    NSLog(@"Request %@ with URL: %@", method, [URL absoluteString]);
    AppDelegate *objAppDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([[self class] isConnected])
    {
#if TARGET_OS_IPHONE
        if ([state1 isEqualToString:STATENOTIFICATIONCOUNT]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }else {
          //  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        }
#endif
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:DEFAULTTIMEOUT];
        [request setHTTPMethod:method];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        //        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSString *strRequest = [dataDict JSONRepresentation];
         NSData *data = [strRequest dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:data];
        
        objAppDelegate.objCurrentNotificationReq = request;
        
        mutableData = [[NSMutableData alloc] init];
        if ([state1 isEqualToString:STATENOTIFICATIONCOUNT]) {
            conn = [[NSURLConnection alloc] initWithRequest:request delegate:APPDELEGATE];
        }else {
            conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }   
    }
    else
    {
        if (![state1 isEqualToString:STATENOTIFICATIONCOUNT]) {
          
            if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)])
            {
                [connectionDelegate ConnectionDidFail:[self state] Data:@"Internet connection not available. Please try again later."];
            }
        }
    }
}
-(NSString*)getCurrentRequest
{
    NSString*strRequest=@"";
    if (conn!=nil) {
        strRequest=[self state];
    }
    return strRequest;
}
-(NSMutableURLRequest *)postWithImageData:(NSDictionary*)_params url:(NSURL*)requestURL withImageKey:(NSString *)imageKey
{
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"0xKhTmLbOuNdArY";
    // set Content-Type in HTTP header0..........00000
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in [_params allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSArray * dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * docsDir = [dirPaths objectAtIndex:0];
    NSString * dataPath = [docsDir stringByAppendingPathComponent:[_params objectForKey:imageKey]];
    if ([FILEMANAGER fileExistsAtPath:dataPath]) {
        
        UIImage*imageToPost = [UIImage imageWithContentsOfFile:dataPath];
        // add image data
        NSData *imageData = UIImagePNGRepresentation(imageToPost);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            NSString *str=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.png\"\r\n",imageKey];
            
            [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type:image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        NSString *strRequest = [_params JSONRepresentation];
        
        
        // set URL
        [request setURL:requestURL];
        
    }
    return request;
}

-(void)postStatusWithImage:(NSURL*)URL method:(NSString*)method data:(NSDictionary*)dataDict state:(NSString*)state withImagekey:(NSString *)key
{
    if ([[self class] isConnected])
    {
#if TARGET_OS_IPHONE
      //  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
#endif
        
        mutableData = [[NSMutableData alloc] init];
        conn = [[NSURLConnection alloc] initWithRequest:[self postWithImageData:dataDict url:URL withImageKey:key] delegate:self];
        //  [conn set]
        
    }
    else
    {
        if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)])
        {
            [connectionDelegate ConnectionDidFail:[self state] Data:@"Internet connection not available. Please try again later."];
        }
    }
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    int statusCode = [request responseStatusCode];
    
    if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFinish:Data:statuscode:)])
    {
        NSString *state1 = [[request userInfo] objectForKey:@"state"];
        [connectionDelegate ConnectionDidFinish:state1 Data:[request responseString] statuscode:statusCode];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
	NSError *error = [request error];
    NSInteger statusCode = [error code];
    if (statusCode == 1) {
        if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)])
        {
            NSString *state1 = [[request userInfo] objectForKey:@"state"];
            [connectionDelegate ConnectionDidFail:state1 Data:@"Internet connection not available. Please try again later."];
        }
    }
    else{
        if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)])
        {
            NSString *strError = request.error.localizedDescription;
            
            if (strError.length == 0 || strError == nil)
                strError = kDefaultConnectionError;
            
            
            [connectionDelegate ConnectionDidFail:[error description] Data:strError];
        }
    }
}


//------------------------Webservice response-----------------------------------------------------------
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
	[mutableData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
#if TARGET_OS_IPHONE
	//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
#endif
    [mutableData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
#if TARGET_OS_IPHONE
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
#endif
    
    NSString*string = [[NSString alloc] initWithData:mutableData encoding:NSUTF8StringEncoding];
    NSDictionary*dataDict = [string JSONValue];
    BOOL hasError = NO;
    
    conn = nil;
    mutableData = nil;
    
    if (dataDict) {
        if ([dataDict isKindOfClass:[NSDictionary class]]) {
            if ([dataDict objectForKey:KEYERROR]) {
                hasError = YES;
                string = [NSString stringWithFormat:@"%@",[dataDict objectForKey:KEYERROR]];
            }
        }
    } else {
        hasError = YES;
    }
    
    if (hasError) {
        if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)]) {
            if (string == nil || string.length == 0)
                string = kDefaultConnectionError;
            
            [connectionDelegate ConnectionDidFail:[self state] Data:string];
        }
    } else {
        if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFinish:Data:statuscode:)]) {
            [connectionDelegate ConnectionDidFinish:[self state] Data:string statuscode:200];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    conn = nil;
    mutableData = nil;
#if TARGET_OS_IPHONE
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
#endif
    
    if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)])
    {
        NSString *strError = error.localizedDescription;
        
        if(strError.length == 0 || !strError)
            strError = kDefaultConnectionError;
        
        [connectionDelegate ConnectionDidFail:[self state] Data:strError];
    }
}

+ (BOOL)isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

#pragma mark - Video Upload Methods
- (void)postStatusWithVideo:(NSDictionary *)dataDict {
    [self setState:STATEPOSTVIDEO];
    [self postStatusWithVideo:[NSURL URLWithString:[BASE_URL stringByAppendingString:APIPOSTVIDEO]] method:@"POST" data:dataDict state:STATEPOSTVIDEO withVideokey:@"post_video"];
}

- (void)postStatusWithVideo:(NSURL*)URL method:(NSString*)method data:(NSDictionary*)dataDict state:(NSString*)state withVideokey:(NSString *)key
{
    if ([[self class] isConnected]) {
        mutableData = [[NSMutableData alloc] init];
        conn = [[NSURLConnection alloc] initWithRequest:[self postWithVideoData:dataDict url:URL withVideokey:key] delegate:self];
    } else {
        if ([connectionDelegate respondsToSelector:@selector(ConnectionDidFail:Data:)]) {
            [connectionDelegate ConnectionDidFail:[self state] Data:@"Internet connection not available. Please try again later."];
        }
    }
}

- (NSMutableURLRequest *)postWithVideoData:(NSDictionary*)_params url:(NSURL*)requestURL withVideokey:(NSString *)videoKey
{
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"0xKhTmLbOuNdArY";
    // set Content-Type in HTTP header0..........00000
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in [_params allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }

    /*NSArray * dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * docsDir = [dirPaths objectAtIndex:0];
    NSString * dataPath = [docsDir stringByAppendingPathComponent:[_params objectForKey:videoKey]];*/
    NSString *dataPath = [_params objectForKey:@"post_video"];
    NSError* error = nil;
    NSData* videoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:dataPath] options:NSDataReadingUncached error:&error];
    if (videoData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *str=[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"pello_video.mov\"\r\n",videoKey];
        
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type:video/quicktime\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:videoData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    NSString *strRequest = [_params JSONRepresentation];
    
    
    // set URL
    [request setURL:requestURL];
    return request;
}
@end