

#import <UIKit/UIKit.h>

@interface UIImage (HAUtils)
+(UIImage*)imageWithContentsOfBundleFileName:(NSString*)fileName;
@end
