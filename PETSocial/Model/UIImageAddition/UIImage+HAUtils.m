

#import "UIImage+HAUtils.h"

@implementation UIImage (HAUtils)

+(UIImage*)imageWithContentsOfBundleFileName:(NSString*)fileName
{
    UIImage*image = nil;
    if ([[NSBundle mainBundle] pathForResource:fileName ofType:@"png"])
    {
        image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"png"]];
    }
    return image;
}

@end
