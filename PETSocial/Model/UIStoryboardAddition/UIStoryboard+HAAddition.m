

#import "UIStoryboard+HAAddition.h"

@implementation UIStoryboard (HAAddition)
+(UIStoryboard *)defaultStoryboard
{
    return [UIStoryboard storyboardWithName:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
}
@end
