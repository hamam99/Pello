//
//  DashboardVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/6/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *mainImageBG;
- (IBAction)registerFBBtnAction:(UIButton *)sender;
- (IBAction)loginBtnAction:(UIButton *)sender;

@end
