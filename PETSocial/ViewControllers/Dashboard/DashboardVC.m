//
//  DashboardVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/6/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "DashboardVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"


@interface DashboardVC ()

@end

@implementation DashboardVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    
    if ([[USERDEFAULTS objectForKey:KEYISLOGEDIN] isEqualToString:KEYLOGEDINYES]) {
        [APPDELEGATE pushTabBar:self.navigationController];
    }
    if (iPhone4) {
        [self.scrollViewOutlet setContentSize:CGSizeMake(320, 630)];
        [self.mainImageBG setFrame:CGRectMake(0, 0, 320, 480)];
        [self.mainImageBG setImage:nil];
        [self.mainImageBG setImage:[UIImage imageNamed:@"bg"]];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionStateChanged:) name:FBSessionStateChangedNotification object:nil];

}

- (void)sessionStateChanged:(NSNotification*)notification {
    
    if (FBSession.activeSession.isOpen) {
        
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection,NSDictionary<FBGraphUser> *user,NSError *error)
         {
             if (!error) {
                 NSDictionary *dictUserInfo = user;
                 NSString *str=[NSString stringWithFormat:@"%@",[USERDEFAULTS objectForKey:@"Devicetoken"]];
                 NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                                     [dictUserInfo objectForKey:@"first_name"],@"first_name",
                                     [dictUserInfo objectForKey:@"last_name"],@"last_name",
                                     [dictUserInfo objectForKey:@"name"],@"user_name",
                                     [dictUserInfo objectForKey:@"id"],@"facebook_id",
                                     [dictUserInfo objectForKey:@"email"],@"email",
                                     str,@"cf_device_token",
                                     nil];
                 [[Connection sharedConnectionWithDelegate:self] registerWithFacebookData:dict];
             }
         }];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:FBSessionStateChangedNotification];
}

- (IBAction)registerFBBtnAction:(UIButton *)sender {
    [[FBSession activeSession] closeAndClearTokenInformation];
    [[FBSession activeSession] close];
    [FBSession setActiveSession:nil];
    
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    [APPDELEGATE openSessionWithAllowLoginUI:YES];
    
}

- (IBAction)loginBtnAction:(UIButton *)sender
{
    
}

#pragma mark Connection Delegate methods -

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEREGISTRATION]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [USERDEFAULTS removeObjectForKey:KEYUSERINFO];
                    [USERDEFAULTS setObject:[dataDict objectForKey:@"data"] forKey:KEYUSERINFO];
                     [USERDEFAULTS setObject:KEYLOGEDINYES forKey:KEYISLOGEDIN];
                    [APPDELEGATE pushTabBar:self.navigationController];
                }
                else{
                     [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"User name already exist. Please try with other username."];
                 }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
    
    
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
}

@end
