//
//  ExploreVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreVC : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionOutview;
- (IBAction)searchBtnAction:(UIButton *)sender;

@property (nonatomic, readwrite) int newPage;
@end
