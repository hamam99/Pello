//
//  SearchVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/21/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVC : UIViewController <UITextFieldDelegate>
{
    UILabel *messageLabel;

}
- (IBAction)segmentBtnAction:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UITextField *searchTextFld;
- (IBAction)backBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentBtnOutlet;

@property (nonatomic,readwrite)   int newPage;

@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;

@end
