//
//  SearchVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/21/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "SearchVC.h"
#import "FollowFollowerCell.h"
#import "UserProfileVC.h"
#import "ExploreCell.h"
#import "SinglePostVC.h"

@interface SearchVC ()
{
    NSArray *userDataArray;
    NSArray *hashTagPostArray;
}
@end

@implementation SearchVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    [self setTextFieldPadding:self.searchTextFld];
    
    self.segmentBtnOutlet.tintColor = [UIColor colorWithRed:51.0/256.0 green:176.0/256.0 blue:222.0/256.0 alpha:1.0];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:14.0 ] forKey:NSFontAttributeName];
    [self.segmentBtnOutlet setTitleTextAttributes:attributes
                                         forState:UIControlStateNormal];
    [self.segmentBtnOutlet setSelectedSegmentIndex:0];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated{
    
    if (messageLabel==nil) {
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.hidden=YES;
    }
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath=(NSIndexPath *)sender;
    
    if ([segue.identifier isEqualToString:@"SingleProfileVC"]) {
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        userProfile.userDetails=[userDataArray objectAtIndex:indexPath.row];
    }
    else  if ([segue.identifier isEqualToString:@"SinglePostVC"]) {
        SinglePostVC *singlePost=[segue destinationViewController];
        singlePost.postDataDetail=[hashTagPostArray objectAtIndex:indexPath.item];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)searchTextData:(BOOL)isUSer withPage:(int )pageNumber{
    
    
    if (self.searchTextFld.text.length>0) {
        
        hashTagPostArray = nil;
        userDataArray = nil;
        [self.collectionViewOutlet reloadData];
        [self.tableViewOutlet reloadData];
        
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        if (isUSer) {
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:self.searchTextFld.text,@"search",[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id", nil];
            [[Connection sharedConnectionWithDelegate:self] findPeopleUsingName:dict];
        }
        else{
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",self.searchTextFld.text,@"search_tag",[NSString stringWithFormat:@"%d",pageNumber],@"page", nil];
            [[Connection sharedConnectionWithDelegate:self] searchUsingHashtag:dict];
        }
    }else
    {
        [[[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter search text" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
}
- (IBAction)segmentBtnAction:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex==0) {
        self.searchTextFld.text=nil;//Prashant
        userDataArray =nil;
        [self.tableViewOutlet reloadData];
        self.searchTextFld.placeholder=@"Search for a user";
        [self.tableViewOutlet setHidden:NO];
        [self.collectionViewOutlet setHidden:YES];
    }
    else if (sender.selectedSegmentIndex==1) {
        self.searchTextFld.text=nil;//Prashant
        hashTagPostArray =nil;
        [self.collectionViewOutlet reloadData];
        [self.collectionViewOutlet setHidden:NO];
        [self.tableViewOutlet setHidden:YES];
        self.searchTextFld.placeholder=@"Search for a hashtag";
    }
}
- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UITableViewDatasource and delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    // Return the number of sections.
    if ([userDataArray count]!=0) {
        messageLabel.hidden=YES;
        return 1;
    }
    else
    {
        // Display a message when the table is empty
        messageLabel.hidden=NO;
        messageLabel.text = @"No records found";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        _tableViewOutlet.backgroundView = messageLabel;
        _tableViewOutlet.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return userDataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowFollowerCell *cell = (FollowFollowerCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowFollowerCell" forIndexPath:indexPath];
    cell.lblUserName.text=[[userDataArray objectAtIndex:indexPath.row] objectForKey:@"user_name"];
    
    cell.imgUsername.layer.borderWidth = 1.0f;
    cell.imgUsername.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    cell.imgUsername.layer.masksToBounds = NO;
    cell.imgUsername.clipsToBounds = YES;
    cell.imgUsername.layer.cornerRadius = cell.imgUsername.frame.size.width/2;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[userDataArray objectAtIndex:indexPath.row] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]])
    {
        [self.tabBarController setSelectedIndex:4];
        
        
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
    }
}

#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        [self searchTextData:YES withPage:1];
    }
    else if (self.segmentBtnOutlet.selectedSegmentIndex==1) {
        [self searchTextData:NO withPage:1];
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textfield1
{
    [self setTextFieldPadding:textfield1];
}

-(void)setTextFieldPadding:(UITextField *)textField{
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, textField.frame.size.height)];
    leftView.backgroundColor = textField.backgroundColor;
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
#pragma mark UICollectionView Datasource and Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    
    return CGSizeMake(100.f * fltMultiplyingFactor, 100.f * fltMultiplyingFactor);
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return hashTagPostArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ExploreCell *myCell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                                   forIndexPath:indexPath];
    
    NSDictionary *dictData=[hashTagPostArray objectAtIndex:indexPath.row];
    //for caching image is necessory then use this.
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[dictData objectForKey:@"post_image"]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [myCell.petImageOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_200"] success:nil failure:nil];
    
    
    return myCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"SinglePostVC" sender:indexPath];
}
#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATESEARCHNYHASHTAG])
            {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    hashTagPostArray=[dataDict objectForKey:@"data"];
                    [self.collectionViewOutlet setHidden:NO];
                    [self.tableViewOutlet setHidden:YES];
                    
                }
                else{
                    [self.collectionViewOutlet setHidden:NO];
                }
                [self refreshOutlets];
                
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATESEARCHBYNAME])
            {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    userDataArray=[dataDict objectForKey:@"data"];
                    [self.collectionViewOutlet setHidden:YES];
                    [self.tableViewOutlet setHidden:NO];
                }
                else{
                    [self.tableViewOutlet setHidden:NO];
                }
                [self refreshOutlets];
                
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
            [self refreshOutlets];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
            [self refreshOutlets];
            
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
-(void)refreshOutlets
{
    [self.collectionViewOutlet reloadData];
    [self.tableViewOutlet reloadData];
}

- (void)ConnectionDidFail:(NSString *)nState Data: (NSString *)nData{
    [SVProgressHUD dismiss];
}

@end
