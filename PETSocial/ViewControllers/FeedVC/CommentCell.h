//
//  CommentCell.h
//  PETSocial
//
//  Created by milap kundalia on 7/11/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface CommentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentTime;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblCommentText;
@property (strong, nonatomic) IBOutlet UIButton *btnUserNameBtn;

@end
