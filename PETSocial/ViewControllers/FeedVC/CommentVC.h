//
//  CommentVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/27/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentVC : UIViewController
{
    UILabel *messageLabel;

}
@property (strong, nonatomic) NSString *strPhotoID;
@property (strong, nonatomic) NSString *strTotalComment;

- (IBAction)backBtnAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITableView *CommentTableViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *viewAddComment;
@property (strong, nonatomic) IBOutlet UITextField *txtAddComment;
- (IBAction)btnSendAction:(id)sender;

@end
