//
//  FeedVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharingActionSheet.h"
#import <MediaPlayer/MediaPlayer.h>
#import "KLCPopup.h"
#import "CMPopTipView.h"

#import "PostTableViewCell.h"

@interface FeedVC : UIViewController<TTTAttributedLabelDelegate, CMPopTipViewDelegate>
{
    AppDelegate    * appdelegate;
}
@property (nonatomic ,strong) SharingActionSheet *sharingSheet;
@property (weak, nonatomic) IBOutlet UITableView *tblFeedList;
@property (nonatomic, assign) BOOL allPostForUser;
@property (nonatomic, strong) NSString *userProfileId;
@property (nonatomic,readwrite)   int newPage;
@property (weak, nonatomic) IBOutlet UIButton *backBtnOutlet;
@property (weak, nonatomic) IBOutlet UILabel *topHeaderLbl;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (nonatomic, readwrite) NSIndexPath *currentPlayedVideoIndexpath;
@property (nonatomic, readwrite) BOOL isScrolling;

@property (nonatomic, readwrite) NSInteger selectedViewMoreItemID;
@property (nonatomic, readwrite) NSIndexPath *currentPlayedVideoIndexPath;
@property (nonatomic, readwrite) BOOL fullVisible;

@property (nonatomic, readwrite) KLCPopup *likerPopup;
@property (nonatomic, readwrite) NSString *selectedPopupItemId;
@property (nonatomic, readwrite) NSString *selectedPopupTypeId;
@property (nonatomic, readwrite) NSString *currentSelectedHashtag;

@property (nonatomic, strong) CMPopTipView *tagNamePoptip;
@property (nonatomic, readwrite) NSMutableArray *poptipArray;

@property (nonatomic, readwrite) PostTableViewCell *currentPlayedVideoCell;
@property (nonatomic, readwrite) NSString *currentSelectedItemId;

- (IBAction)backBtnAction:(UIButton *)sender;
-(IBAction)commentToFeed:(UIStoryboardSegue *)sender;

@end
