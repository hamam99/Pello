//
//  FeedVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "FeedVC.h"
//#import "FeedViewCell.h"
#import "CommentVC.h"
#import "SharingActionSheet.h"
#import "LocationVC.h"
#import "UserProfileVC.h"
#import "TaggedUsersViewController.h"
#import "HashTagViewController.h"
#import "FeedHeaderView.h"
//#import "FeedCommentCell.h"
#import "FeedCommentEmptyCell.h"
#import "CommentCell.h"
#import "FeedCommetViewMoreTableViewCell.h"
#import "LikedViewController.h"
#import "KLCPopup.h"
#import "LikerListViewController.h"
#import "TTTAttributedLabel.h"
#import "Constant.h"


#define kMinFoldCommentCount 5
#define kVIDEO_CELL_SIZE 373
#define kCOMMENT_FONT_SIZE 14

@interface FeedVC ()
{
    NSMutableArray * arrayFeedData;
    UIView *viewFooter;
    UIActivityIndicatorView *pageLoader;
    BOOL isMoreData;
    BOOL isValidateData;
    int mainPageId;
    UITableViewController *tableViewController;
    NSIndexPath *commentIndexPath;
    NSIndexPath *indexPathRef;
    BOOL isLoadedCall;
}
@end


@implementation FeedVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self preferredStatusBarStyle];
    self.newPage=1;
    arrayFeedData=[[NSMutableArray alloc]init];
    //Check if screen for all post or not.
    if ([APPDELEGATE remoteNotif]) {
        
        [self.tabBarController setSelectedIndex:3];
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab4"]];
    }
    else{
        if (self.allPostForUser) {
            self.topHeaderLbl.text=@"Posts";
            //[self.backBtnOutlet setHidden:NO];
            [APPDELEGATE ShowHUDWith:@"Loading..."];
            [self callForAllPost:1];
        }
        else{
            //[self.backBtnOutlet setHidden:YES];
            self.topHeaderLbl.text=@"FEED";
            
            isLoadedCall=YES;
            [self callfeedListData:1];
            [APPDELEGATE ShowHUDWith:@"Loading..."];
        }
    }
    [self setUpFooterView];
    tableViewController= [[UITableViewController alloc] init];
    tableViewController.tableView = self.tblFeedList;
    
    UIRefreshControl *refreshControl = [UIRefreshControl.alloc init];
    [refreshControl setTintColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],
                                        };
    NSString *s = @"Refreshing...";
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:s];
    
    [attributeString setAttributes:refreshAttributes range:NSMakeRange(0, attributeString.length)];
    refreshControl.attributedTitle=attributeString;
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl=refreshControl;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostReloadTable) name:DeletePost object:nil];
    
    UINib * feedCommentCell = [UINib nibWithNibName:@"FeedCommentCell" bundle:nil];
    [self.tblFeedList registerNib:feedCommentCell forCellReuseIdentifier:@"FeedCommentCell"];
    
    UINib * feedCommentEmptyCell = [UINib nibWithNibName:@"FeedCommentEmptyCell" bundle:nil];
    [self.tblFeedList registerNib:feedCommentEmptyCell forCellReuseIdentifier:@"FeedCommentEmptyCell"];
    
    UINib * feedCommentViewMoreCell = [UINib nibWithNibName:@"FeedCommetViewMoreTableViewCell" bundle:nil];
    [self.tblFeedList registerNib:feedCommentViewMoreCell forCellReuseIdentifier:@"feedCommentViewMoreCell"];
    
    //External Feed Cell
    UINib * externalFeedCell = [UINib nibWithNibName:@"PostTableViewCell" bundle:nil];
    [self.tblFeedList registerNib:externalFeedCell forCellReuseIdentifier:@"PostTableViewCell"];

}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.currentSelectedHashtag != nil) {
        [self.backBtnOutlet setHidden:NO];
        self.topHeaderLbl.text = [NSString stringWithFormat:@"#%@", self.currentSelectedHashtag];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    if (self.currentPlayedVideoCell) {
        [self makeItStahp:self.currentPlayedVideoCell];
    }
}

-(void)deletePostReloadTable{
    
    [arrayFeedData removeObjectAtIndex:indexPathRef.section];
    [self.tblFeedList beginUpdates];
    [self.tblFeedList deleteSections:[NSIndexSet indexSetWithIndex:indexPathRef.section]  withRowAnimation:UITableViewRowAnimationFade];
    [self.tblFeedList endUpdates];
    [[NSNotificationCenter defaultCenter] removeObserver:DeletePost];
    
}

-(void)pullToRefresh{
    if (self.allPostForUser) {
        [self callForAllPost:1];
    } else if (self.currentSelectedHashtag) {
        [self callForPostWithHashtag:1];
    }
    else{
        [self callfeedListData:1];
    }
    isValidateData=NO;
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
        [tableViewController.refreshControl endRefreshing];
    });
}

-(void)callForAllPost:(int )pageId{
    NSString *strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",self.userProfileId,@"user_id",[NSString stringWithFormat:@"%d",pageId],@"page", nil];
    [[Connection sharedConnectionWithDelegate:self] getPostOfUser:dict];
}
-(void)callForPostWithHashtag:(int )pageId {
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    mainPageId=pageId;
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                        [[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",
                        [NSString stringWithFormat:@"%d",pageId],@"page",
                        self.currentSelectedHashtag, @"hashtag",
                        nil];
    [[Connection sharedConnectionWithDelegate:self]getPostHashtagOfUser:dict];
}
-(void)callfeedListData:(int )pageId{

    mainPageId=pageId;
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",[NSString stringWithFormat:@"%d",pageId],@"page", nil];
    [[Connection sharedConnectionWithDelegate:self]getFeedList:dict];
    
}
-(void)callCommentListDataForPhotoID:(NSString*)strPhotoID{
    //{"photo_id":"1"}
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strPhotoID,@"photo_id", nil];
    [[Connection sharedConnectionWithDelegate:self]GetComment:dict forPhotoID:strPhotoID];
}
-(void)locationBtnAction:(UIButton *)sender{
    [self performSegueWithIdentifier:@"LocationVC" sender:sender];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.backBtnOutlet setHidden:YES];
    appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab1"]];
    
    if (appdelegate.isNewFeed==YES)
    {
        if (self.allPostForUser) {
            self.topHeaderLbl.text=@"Posts";
            //[self.backBtnOutlet setHidden:NO];
            [APPDELEGATE ShowHUDWith:@"Loading..."];
            [self callForAllPost:1];
        }
        else{
            //[self.backBtnOutlet setHidden:YES];
            self.topHeaderLbl.text=@"FEED";
            [APPDELEGATE ShowHUDWith:@"Loading..."];
            [self callfeedListData:1];
        }
        appdelegate.isNewFeed=NO;
    }
    
    if (arrayFeedData.count!=0 && isLoadedCall==NO) {
        [self callfeedListData:1];
    }
    
}

-(IBAction)commentToFeed:(UIStoryboardSegue *)segue{
    if ([segue.sourceViewController isKindOfClass:[CommentVC class]]) {
        CommentVC *commentVC = segue.sourceViewController;
        NSMutableDictionary *dict =[[arrayFeedData objectAtIndex:commentIndexPath.section] mutableCopy];
        [dict setValue:commentVC.strTotalComment forKey:@"total_comment"];
        [arrayFeedData replaceObjectAtIndex:commentIndexPath.section withObject:dict];
        [self.tblFeedList reloadData];
    }
}
- (IBAction)backBtnAction:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    self.currentSelectedHashtag = nil;
    self.topHeaderLbl.text = @"FEED";
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    [self.backBtnOutlet setHidden:YES];
    [self callfeedListData:1];
}

- (void)boneButtonDidPushed:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    PostTableViewCell *cell = [self.tblFeedList cellForRowAtIndexPath:indexPath];
    NSDictionary *itemData = [arrayFeedData objectAtIndex:indexPath.section];
    NSArray *tags = [itemData objectForKey:@"tag"];
    
    NSInteger maxTagUsername = 5;
    
    if (self.poptipArray == nil) {
        self.poptipArray = [[NSMutableArray alloc] init];
    }
    
    for (int i=0;i<tags.count;i++) {
        if (i==maxTagUsername) break;
        NSString *username = [[tags objectAtIndex:i] objectForKey:@"user_name"];
        CMPopTipView *tipView = [[CMPopTipView alloc] initWithMessage:username];
        tipView.backgroundColor = [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        tipView.has3DStyle = NO;
        tipView.delegate = self;
        tipView.textColor = [UIColor whiteColor];
        //tipView.dismissTapAnywhere = YES;
        tipView.triggerTapAnywhere = YES;
        tipView.hasGradientBackground = NO;
        tipView.textFont = [UIFont systemFontOfSize:12.0f];
        tipView.borderColor = [UIColor clearColor];
        
        [tipView presentPointingAtView:sender inView:cell.contentView animated:YES index:i];
        
        [self.poptipArray addObject:tipView];
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LocationVC"]) {
        UIButton *btn=(UIButton *)sender;
        LocationVC *locationVC=(LocationVC *)[segue destinationViewController];
        locationVC.locationName=[[arrayFeedData objectAtIndex:btn.tag] objectForKey:@"location"];
    }
    else if ([segue.identifier isEqualToString:@"CommentSegue"]) {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
        commentIndexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
        CommentVC *commentVC=(CommentVC *)[segue destinationViewController];
        //commentVC.strPhotoID=[[arrayFeedData objectAtIndex:commentIndexPath.section] objectForKey:@"photo_id"];
        //commentVC.strPhotoID = [NSString stringWithFormat:@"%ld", (long)self.selectedViewMoreItemID];
        commentVC.strPhotoID = self.currentSelectedItemId;
    }
    else if([segue.identifier isEqualToString:@"SingleProfileVC"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        NSIndexPath *indexPath =( NSIndexPath *)sender;
        userProfile.userDetails=[arrayFeedData objectAtIndex:indexPath.section];
    }
    else if([segue.identifier isEqualToString:@"TaggedUser"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        NSIndexPath *indexPath =( NSIndexPath *)sender;
        userProfile.userDetails=[[[arrayFeedData objectAtIndex:indexPath.section] objectForKey:@"tag"] objectAtIndex:0];
    }
    else if([segue.identifier isEqualToString:@"TaggedUserSegue"]){
        TaggedUsersViewController *tagUsers=(TaggedUsersViewController *)[segue destinationViewController];
        NSIndexPath *indexPath =( NSIndexPath *)sender;
        tagUsers.arrayData=[[arrayFeedData objectAtIndex:indexPath.section] objectForKey:@"tag"];
    } else if ([segue.identifier isEqualToString:@"LikerListSegue"]) {
        LikerListViewController *lkvc = (LikerListViewController*)[segue destinationViewController];
        lkvc.itemId = self.selectedPopupItemId;
        lkvc.typeId = self.selectedPopupTypeId;
    }
}

-(void)moreBtnAction:(UIButton *)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    
    indexPathRef=indexPath;
    int postUserId=[[[arrayFeedData objectAtIndex:indexPath.section] objectForKey:@"user_id"] intValue];
    int userId=[[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"] intValue];
    
    NSDictionary *curD = [arrayFeedData objectAtIndex:indexPath.section];
    
    if (postUserId==userId) {
        self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
        self.sharingSheet.postData=[arrayFeedData objectAtIndex:indexPath.section];
        
        PostTableViewCell *tmp = [self.tblFeedList cellForRowAtIndexPath:indexPath];
        UIImage *tmpImg = tmp.imgPet.image;
        if (tmpImg) {
            self.sharingSheet.sharedImage = tmpImg;
        }
        self.sharingSheet.deleteBtnNeeded=YES;
        
        [[APPDELEGATE window] addSubview:self.sharingSheet.view];
        [self.sharingSheet presentActionSheet];
    } else {
        self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
        self.sharingSheet.postData=[arrayFeedData objectAtIndex:indexPath.section];
        self.sharingSheet.deleteBtnNeeded=NO;
        [[APPDELEGATE window] addSubview:self.sharingSheet.view];
        self.sharingSheet.btnReportContent.intItemId = [[self.sharingSheet.postData valueForKey:@"photo_id"] integerValue];
        self.sharingSheet.btnReportContent.intUserId = postUserId;
        [self.sharingSheet presentActionSheet];
    }
}

-(void)commentBtnAction:(UIButton *)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    NSDictionary *curD = [arrayFeedData objectAtIndex:indexPath.section];
    NSString *itemId = ([curD objectForKey:@"photo_id"]) ? [curD objectForKey:@"photo_id"] : [curD objectForKey:@"video_id"];
    self.currentSelectedItemId=itemId;
    
    [self performSegueWithIdentifier:@"CommentSegue" sender:sender];
}

- (void)audioBtnAction:(UIButton*)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    PostTableViewCell *cell = [self.tblFeedList cellForRowAtIndexPath:indexPath];
    
    cell.avPlayer.muted = ![cell.avPlayer isMuted];
    
    if (cell.avPlayer.muted) {
        [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-muted-icon.png"] forState:UIControlStateNormal];
    } else {
        [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-icon.png"] forState:UIControlStateNormal];
    }
    
}

- (void)playVideoButtonDidPushed:(UIButton*)sender
{
    
}

-(void)likeBtnAction:(UIButton *)sender{
    NSString *likeFlag;
    if (sender.tag==likeBtnTag) {
        likeFlag= @"1";
    }
    else if (sender.tag==disLikeBtnTag) {
        likeFlag= @"0";
    }
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    
    NSDictionary *itemData = [arrayFeedData objectAtIndex:indexPath.section];
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                        [[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",
                        likeFlag,@"like",
                        nil];
    
    if ([itemData objectForKey:@"photo_id"]) {
        [dict setObject:[itemData objectForKey:@"photo_id"] forKey:@"photo_id"];
        [dict setObject:@"photo" forKey:@"type_id"];
    }
    
    if ([itemData objectForKey:@"video_id"]) {
        [dict setObject:[itemData objectForKey:@"video_id"] forKey:@"video_id"];
        [dict setObject:@"video" forKey:@"type_id"];
    }

    
    [[Connection sharedConnectionWithDelegate:self] likeDislike:dict];
}

- (void)showLikePopup:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    
    LikedViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LikedViewController"];
    NSDictionary *itemData = [arrayFeedData objectAtIndex:indexPath.section];
    
    if ([[itemData objectForKey:@"total_like"] integerValue] == 0) {
        return;
    }
    
    if ([itemData objectForKey:@"photo_id"]) {
        vc.itemId = [itemData objectForKey:@"photo_id"];
        vc.typeId = @"photo";
    }
    
    if ([itemData objectForKey:@"video_id"]) {
        vc.itemId = [itemData objectForKey:@"video_id"];
        vc.typeId = @"video";
    }
    
    self.selectedPopupItemId = vc.itemId;
    self.selectedPopupTypeId = vc.typeId;
    
    UIView* contentView = vc.view;
    contentView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width*0.90, self.view.frame.size.height*0.25);
    [vc.collectionView setBackgroundColor:[UIColor clearColor]];
    [contentView setBackgroundColor:[UIColor clearColor]];
    
    UIView* popupView = [[UIView alloc] init];
    popupView.translatesAutoresizingMaskIntoConstraints = NO;
    popupView.layer.cornerRadius = 6.0;
    popupView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width*0.90, self.view.frame.size.height*0.30);
    [popupView setBackgroundColor:[UIColor whiteColor]];
    [popupView addSubview:contentView];
    
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, popupView.frame.size.height-30, popupView.frame.size.width, 30)];
    int pages = floor(vc.collectionView.contentSize.width /
                      vc.collectionView.frame.size.width) + 1;
    [pageControl setNumberOfPages:pages];
    [pageControl setPageIndicatorTintColor:[UIColor grayColor]];
    [pageControl setCurrentPageIndicatorTintColor:[UIColor darkGrayColor]];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(pageControlClicked:)];
    pageControl.userInteractionEnabled=YES;
    [pageControl addGestureRecognizer:tapGesture];
    
    pageControl.tag = 321;
    [popupView addSubview:pageControl];
    vc.pageControl = (UIPageControl*)[popupView viewWithTag:321];
    
    
    /*UIButton *seeMoreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [seeMoreBtn setFrame:CGRectMake(0, popupView.frame.size.height-30, popupView.frame.size.width, 30)];
    [seeMoreBtn setImage:[UIImage imageNamed:@"social-dots"] forState:UIControlStateNormal];
    [seeMoreBtn addTarget:self action:@selector(likerPopupSeeMoreButtonDidPushed:) forControlEvents:UIControlEventTouchUpInside];
    [popupView addSubview:seeMoreBtn];*/
    
    [vc reinit];
    
    self.likerPopup = [KLCPopup popupWithContentView:popupView
                                           showType:KLCPopupShowTypeFadeIn
                                        dismissType:KLCPopupDismissTypeFadeOut
                                           maskType:KLCPopupMaskTypeDimmed
                           dismissOnBackgroundTouch:YES
                              dismissOnContentTouch:NO];
    [self.likerPopup show];
}

-(void)pageControlClicked:(UITapGestureRecognizer *)gesture
{
    if (self.likerPopup) {
        [self.likerPopup dismiss:YES];
        [self performSegueWithIdentifier:@"LikerListSegue" sender:self];
    }
}

- (void)likerPopupSeeMoreButtonDidPushed:(UIButton*)sender
{
    if (self.likerPopup) {
        [self.likerPopup dismiss:YES];
        [self performSegueWithIdentifier:@"LikerListSegue" sender:self];
    }
}

-(void)userPrfoileNavigation:(UITapGestureRecognizer *)sender
{
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tblFeedList];
    NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    
    if ([[[arrayFeedData objectAtIndex:indexPath.section] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
        [self.tabBarController setSelectedIndex:4];
        
        
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
    }
}
#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEFEEDLIST] || [nState isEqualToString:STATEALLPOSTHASHTAG]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    if (!isValidateData) {
                        [tableViewController.refreshControl endRefreshing];
                        arrayFeedData=[[NSMutableArray alloc]init];
                        arrayFeedData=[dataDict objectForKey:@"data"];
                        NSInteger counter = 0;
                    } else {
                        arrayFeedData=[[arrayFeedData arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    
                    for (int i=0;i<arrayFeedData.count;i++) {
                        
                        NSInteger comments = [[[arrayFeedData objectAtIndex:i] objectForKey:@"comments"] count];
                        if (comments>kMinFoldCommentCount) {
                            
                            
                            NSMutableArray *unfoldedComments = [[NSMutableArray alloc] initWithCapacity:4];
                            NSArray *curComm = [[arrayFeedData objectAtIndex:i] objectForKey:@"comments"];
                            for (int i=0;i<kMinFoldCommentCount;i++) {
                                [unfoldedComments addObject:[curComm objectAtIndex:i]];
                            }
                            
                            NSString *viewmore = [NSString stringWithFormat:@"View all %lu comments", (unsigned long)comments];
                            [unfoldedComments insertObject:viewmore atIndex:1];
                            [[arrayFeedData objectAtIndex:i] setObject:unfoldedComments forKey:@"comments"];
                            
                        }
                    }
                    
                    
                    [self.tblFeedList reloadData];
                    self.isScrolling=NO;
                    [self checkWhichVideo];
                }
                else{
                    [tableViewController.refreshControl endRefreshing];
                    [arrayFeedData removeAllObjects];
                    [self.tblFeedList reloadData];
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"No records found"];
                }
                self.tblFeedList.tableFooterView=nil;
                [pageLoader stopAnimating];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATELIKEDISLIKE]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    NSString *type_id = [dataDict objectForKey:@"type_id"];
                    NSString *itemID = nil;
                    NSString *itemName = @"";
                    if ([type_id isEqualToString:@"photo"]) {
                        itemID=[[[dataDict valueForKey:@"data"] objectAtIndex:0] objectForKey:@"photo_id"];
                        itemName = @"photo_id";
                    } else {
                        itemID=[[dataDict valueForKey:@"data"] objectForKey:@"video_id"];
                        itemName = @"video_id";
                    }
                    
                    [arrayFeedData enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop){
                        if([[obj valueForKey:itemName] isEqualToString:itemID])
                        {
                            NSString *cur = [obj objectForKey:@"like"];
                            if ([cur isEqualToString:@"1"]) {
                                [obj setValue:@"0" forKey:@"like"];
                            } else {
                                [obj setValue:@"1" forKey:@"like"];
                            }
                            
                            NSString *curLike = [obj objectForKey:@"total_like"];
                            NSInteger curLikeInt = [curLike integerValue];
                            if ([[dataDict objectForKey:@"action"] isEqualToString:@"like"]) {
                                curLikeInt += 1;
                            } else {
                                curLikeInt = MAX(0, curLikeInt-1);
                            }
                            [obj setValue:[NSString stringWithFormat:@"%ld", (long)curLikeInt] forKey:@"total_like"];
                            
                            *stop  = YES;
                        }
                    }];
                    [self.tblFeedList reloadData];
                    [SVProgressHUD dismiss];
                }
                else{
                    [SVProgressHUD dismiss];
                }
            }
            else if ([nState isEqualToString:STATEALLPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    if (!isValidateData) {
                        [tableViewController.refreshControl endRefreshing];
                        
                        arrayFeedData=[[NSMutableArray alloc]init];
                        arrayFeedData=[dataDict objectForKey:@"data"];
                    }
                    else{
                        arrayFeedData=[[arrayFeedData arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    [self.tblFeedList reloadData];
                    
                    
                }
                else{
                }
                [self.tblFeedList reloadData];
                self.tblFeedList.tableFooterView=nil;
                [pageLoader stopAnimating];
                [SVProgressHUD dismiss];
                
            }
            
            
            // Out of the usual condition checker
            if ([nState containsString:STATEGETCOMMENTWITHPHOTOID]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    NSArray *tmp = [nState componentsSeparatedByString: @"-"];
                    NSString *photoid = tmp[1];
                    if (arrayFeedData && arrayFeedData.count > 0) {
                        for (int i=0;i<arrayFeedData.count;i++) {
                            if ([[[arrayFeedData objectAtIndex:i] objectForKey:@"photo_id"] isEqualToString:photoid]) {
                                NSMutableArray *comments = [[arrayFeedData objectAtIndex:i] objectForKey:@"comments"];
                                [comments addObjectsFromArray:[dataDict objectForKey:@"data"]];
                                [[arrayFeedData objectAtIndex:i] setValue:comments forKey:@"comments"];
                            }
                        }
                    }
                } else{
                    [SVProgressHUD dismiss];
                }
                [self.tblFeedList reloadData];
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Request Timed Out"];
}


#pragma mark - Table View

- (BOOL)isCellVisible:(UITableViewCell*)cell
{
    //return [self.tblFeedList.visibleCells containsObject:cell];
    
    NSIndexPath *indexPath = [self.tblFeedList indexPathForCell:cell];
    BOOL exists = [self.tblFeedList.indexPathsForVisibleRows containsObject:indexPath];
    return exists;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return arrayFeedData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger retVal = 2;
    NSDictionary *curD = [arrayFeedData objectAtIndex:section];
    if ([curD objectForKey:@"comments"] && [[curD objectForKey:@"comments"] count] > 0) {
        retVal = [[curD objectForKey:@"comments"] count] + 1;
    }
    return retVal;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row>0) {
        
        /*NSArray *comments = [dictData objectForKey:@"comments"];
        NSString *curComments = [[comments objectAtIndex:indexPath.row-1] objectForKey:@"text"];
        NSData *strval = [Base64 decode:curComments];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        if (strTextMsg.length==0) {
            strTextMsg = curComments;
        }
        
        CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
        CGFloat hight=[strTextMsg getHeightOfTextForFontSize:12.0 withLabelWidth:320*fltMultiplyingFactor];
        NSLog(@"Comment: %@ --- height: %f", strTextMsg, MAX(18, 14+hight));
        if (indexPath.row == comments.count) {
            return 21+hight;
        } else {
            return MAX(18, 14+hight);
        }*/
        
        NSDictionary *dictData=[arrayFeedData objectAtIndex:indexPath.section];
        NSDictionary *commentData = [[dictData objectForKey:@"comments"] objectAtIndex:indexPath.row-1];
        
        if ([commentData isKindOfClass:[NSString class]]) {
            return 44.0f;
        }
        
        NSData *strval = [Base64 decode:[commentData objectForKey:@"text"]];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        NSString *username = [commentData objectForKey:@"user_name"];
        NSString *wholeStr = [NSString stringWithFormat:@"%@ %@", username, strTextMsg];
        
        TTTAttributedLabel *tempLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, 0, 320, 72)];
        [self processLabelCommentWithCommentStr:wholeStr andUsername:username forLabel:tempLabel];
        
        return tempLabel.frame.size.height;
    } else {
        if (IS_IPHONE_5) {
            return 383.0f;
        } else if (IS_IPHONE_6){
            return 410.0f;
        } else {
            return 410.0f;
        }
        
    }

    /*if (indexPath.row==0) {
        CGFloat totalHeight=366;    // was 366
        
        totalHeight = totalHeight * appdelegate.window.frame.size.width/320;
        
        CGFloat height=[[dictData objectForKey:@"post_status"] getHeightOfTextForFontSize:14.0 withLabelWidth:313.0*appdelegate.window.frame.size.width/320];
        if ([[dictData objectForKey:@"tag"] count]==0)
        {
            return totalHeight+height;
        }
        else{
            return totalHeight+height+20;
            
        }
    } else {
        return 44.0f;
    }*/
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 2) {
        FeedCommetViewMoreTableViewCell *tmpCell = [tableView cellForRowAtIndexPath:indexPath];
        if ([tmpCell isKindOfClass:[FeedCommetViewMoreTableViewCell class]]) {
            self.selectedViewMoreItemID = tmpCell.item_id;
            [self performSelector:@selector(commentBtnAction:) withObject:nil];
        }
        
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    FeedHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"FeedHeaderView" owner:self options:nil] objectAtIndex:0];
    headerView.avatarImage.layer.borderWidth = 1.0f;
    headerView.avatarImage.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    headerView.avatarImage.layer.masksToBounds = NO;
    headerView.avatarImage.clipsToBounds = YES;
    headerView.avatarImage.layer.cornerRadius = headerView.avatarImage.frame.size.width/2;
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[[arrayFeedData objectAtIndex:section] objectForKey:@"user_image"]];
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [headerView.avatarImage setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
    
    CGFloat usernameFontSize = 13.0f;
    CGFloat locationFontSize = 10.0f;
    
    NSDictionary *curD = [arrayFeedData objectAtIndex:section];
    NSString *itemId = ([curD objectForKey:@"photo_id"]) ? [curD objectForKey:@"photo_id"] : [curD objectForKey:@"video_id"];
    //NSString *username = [NSString stringWithFormat:@"%@ - %@",[curD objectForKey:@"user_name"], itemId];
    NSString *username = [curD objectForKey:@"user_name"];
    
    if ([[[arrayFeedData objectAtIndex:section] objectForKey:@"location"] length]>0) {
        
        headerView.usernameLabel.frame = CGRectMake(45, 5, 150, 30);
        headerView.usernameLabel.text=username;
        headerView.usernameLabel.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        [headerView.usernameLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:usernameFontSize]];
        headerView.usernameLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
        [headerView.usernameLabel addGestureRecognizer:tapGest];
        
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(40, 30, 280, 15)];
        [btn setTitle:[[arrayFeedData objectAtIndex:section] objectForKey:@"location"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageWithContentsOfBundleFileName:@"icn_location1"] forState:UIControlStateNormal];
        
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btn addTarget:self action:@selector(locationBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:locationFontSize]];
        btn.tag=section;
        [headerView addSubview:btn];
        
        NSString *dateString = [[arrayFeedData objectAtIndex:section] objectForKey:@"time_stamp"];
        //headerView.timeLabel.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]];
        [headerView.timeButton setTitle:[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]] forState:UIControlStateNormal];
        
        return headerView;
        
    } else {
        headerView.usernameLabel.frame = CGRectMake(45, 12, 150, 30);
        headerView.usernameLabel.text=username;
        headerView.usernameLabel.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        [headerView.usernameLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:usernameFontSize]];
        headerView.usernameLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
        [headerView.usernameLabel addGestureRecognizer:tapGest];
        
        NSString *dateString = [[arrayFeedData objectAtIndex:section] objectForKey:@"time_stamp"];
        //headerView.timeLabel.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]];
        [headerView.timeButton setTitle:[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]] forState:UIControlStateNormal];
        
        return headerView;
    }
    
    /*UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 55.0)];
    [v setBackgroundColor:[UIColor whiteColor]];
    UIImageView *imgV;
    imgV=[[UIImageView alloc]init];
    
    [imgV setFrame:CGRectMake(5, 10, 35, 35)];
    imgV.layer.borderWidth = 1.0f;
    imgV.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    imgV.layer.masksToBounds = NO;
    imgV.clipsToBounds = YES;
    imgV.layer.cornerRadius = imgV.frame.size.width/2;
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[[arrayFeedData objectAtIndex:section] objectForKey:@"user_image"]];
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [imgV setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
    
    [v addSubview:imgV];
    
    
    if ([[[arrayFeedData objectAtIndex:section] objectForKey:@"location"] length]>0) {
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(45, 5, 280, 30)];
        lblName.text=[[arrayFeedData objectAtIndex:section] objectForKey:@"user_name"];
        
        
        lblName.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        [lblName setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]];
        lblName.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
        [lblName addGestureRecognizer:tapGest];
        
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(40, 30, 280, 15)];
        [btn setTitle:[[arrayFeedData objectAtIndex:section] objectForKey:@"location"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageWithContentsOfBundleFileName:@"icn_location1"] forState:UIControlStateNormal];
        
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btn addTarget:self action:@selector(locationBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0]];
        btn.tag=section;
        [v addSubview:btn];
        [v addSubview:lblName];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        timeLabel.textColor=[UIColor colorWithRed:234.0/255.0f green:234.0/255.0f blue:234.0/255.0f alpha:1.0];
        [timeLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:9.0]];
        timeLabel.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:
                                                      [[arrayFeedData objectAtIndex:section] objectForKey:@"date"]]];
        [v addSubview:timeLabel];
        
        return v;
    }
    else{
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(45, 12, 280, 30)];
        lblName.text=[[arrayFeedData objectAtIndex:section] objectForKey:@"user_name"];
        lblName.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        [lblName setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]];
        lblName.userInteractionEnabled=YES;
        [v addSubview:lblName];
        
        lblName.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
        [lblName addGestureRecognizer:tapGest];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        timeLabel.textColor=[UIColor colorWithRed:234.0/255.0f green:234.0/255.0f blue:234.0/255.0f alpha:1.0];
        [timeLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:9.0]];
        timeLabel.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:
                                                      [[arrayFeedData objectAtIndex:section] objectForKey:@"date"]]];
        [v addSubview:timeLabel];
        
        
        return v;
    }*/
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictData=[arrayFeedData objectAtIndex:indexPath.section];
    if (indexPath.row==0) {
        return [self processFeedViewCellForTableView:tableView andIndexPath:indexPath];
    } else {
        if ([dictData objectForKey:@"comments"] && [[dictData objectForKey:@"comments"] count]) {
            //return [self processFeedCommentCellForTableView:tableView andIndexPath:indexPath];
            return [self processCommentCellForTableView:tableView andIndexPath:indexPath];
        } else {
            return [self processFeedCommentEmptyCellForTableView:tableView andIndexPath:indexPath];
        }
    }
    return nil;
    
}

- (UITableViewCell*)processCommentCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    NSDictionary *dictData=[arrayFeedData objectAtIndex:indexPath.section];
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    NSDictionary *commentData = [[dictData objectForKey:@"comments"] objectAtIndex:indexPath.row-1];
    
    if ([commentData isKindOfClass:[NSString class]]) {
        FeedCommetViewMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedCommentViewMoreCell"];
        cell.viewAllLabel.text = (NSString*)commentData;
        cell.item_id = [[dictData objectForKey:@"photo_id"] integerValue];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        NSData *strval = [Base64 decode:[commentData objectForKey:@"text"]];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        NSString *username = [commentData objectForKey:@"user_name"];
        NSString *wholeStr = [NSString stringWithFormat:@"%@ %@", username, strTextMsg];
        
        [self processLabelCommentWithCommentStr:wholeStr andUsername:username forLabel:cell.lblCommentText];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

- (void)processLabelCommentWithCommentStr:(NSString*)wholeStr andUsername:(NSString*)username forLabel:(TTTAttributedLabel*)label
{
    NSMutableAttributedString *attri_str=[[NSMutableAttributedString alloc]initWithString:wholeStr];
    NSInteger begin= 0;
    NSInteger end=[username length];
    [attri_str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:kCOMMENT_FONT_SIZE] range:NSMakeRange(begin, end)];
    [attri_str addAttribute:NSForegroundColorAttributeName value:kDEFAULT_BLUE_COLOR range:NSMakeRange(begin, end)];
    [label setNumberOfLines:0];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text=attri_str;
    
    [label setTextColor:[UIColor grayColor]];
    
    //Search hashtag
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:[attri_str string] options:0 range:NSMakeRange(0, [attri_str string].length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString* word = [[attri_str string] substringWithRange:wordRange];
        NSRange editedRange = NSMakeRange(wordRange.location-1, wordRange.length+1);
        
        label.delegate=self;
        label.tag=99;
        
        NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName, nil];
        NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
        NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        label.linkAttributes = linkAttributes;
        
        [label addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"hashtag:%@", word]] withRange:editedRange];
    }
    
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    
    CGRect frame=label.frame;
    //CGFloat hight=[[attri_str string] getHeightOfTextForFontSize:12.0 withLabelWidth:frame.size.width*fltMultiplyingFactor];
    CGFloat height = [self calculateLabelFrameForText:[attri_str string] andLabelWidth:frame.size.width*fltMultiplyingFactor];
    [label setFrame:CGRectMake(label.frame.origin.x,
                                             0,
                                             frame.size.width,
                                             MAX(21.0f, height))];
    
    //NSLog(@"Comment: %@ --- frame: %@", [attri_str string], NSStringFromCGRect(label.frame));
}

- (CGFloat)calculateLabelFrameForText:(NSString*)str andLabelWidth:(CGFloat)labelWidth
{
    CGFloat height=[str getHeightOfTextForFontSize:kCOMMENT_FONT_SIZE withLabelWidth:labelWidth];
    return MAX(height, 21.0f);
}

/*- (FeedCommentCell*)processFeedCommentCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    FeedCommentCell *cell = (FeedCommentCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedCommentCell" forIndexPath:indexPath];
    
    NSDictionary *dictData=[arrayFeedData objectAtIndex:indexPath.section];
    NSDictionary *commentData = [[dictData objectForKey:@"comments"] objectAtIndex:indexPath.row-1];
    
    NSData *strval = [Base64 decode:[commentData objectForKey:@"text"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    NSString *username = [commentData objectForKey:@"user_name"];
    NSString *wholeStr = [NSString stringWithFormat:@"%@ %@", username, strTextMsg];
    
    NSMutableAttributedString *attri_str=[[NSMutableAttributedString alloc]initWithString:wholeStr];
    NSInteger begin= 0; //[wholeStr length]-[username length];
    NSInteger end=[username length];
    [attri_str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:kCOMMENT_FONT_SIZE] range:NSMakeRange(begin, end)];
    [attri_str addAttribute:NSForegroundColorAttributeName value:kDEFAULT_BLUE_COLOR range:NSMakeRange(begin, end)];
    
    cell.lblCommentText.attributedText=attri_str;
    [cell.lblCommentText sizeToFit];
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    CGRect frame=cell.lblCommentText.frame;
    [cell.lblCommentText setNumberOfLines:0];
    //cell.lblCommentText.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.lblCommentText.lineBreakMode = NSLineBreakByWordWrapping;
    CGFloat hight=[[attri_str string] getHeightOfTextForFontSize:12.0 withLabelWidth:frame.size.width*fltMultiplyingFactor];
    
    CGRect realRect = CGRectMake(cell.lblCommentText.frame.origin.x,
                                 18,
                                 frame.size.width,
                                 hight+3);
    [cell.lblCommentText setFrame:realRect];
    NSLog(@"comment: %@ --- has frame: %@", [cell.lblCommentText.attributedText string], NSStringFromCGRect(cell.lblCommentText.frame));
    
    return cell;
}*/

- (FeedCommentEmptyCell*)processFeedCommentEmptyCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    FeedCommentEmptyCell *cell = (FeedCommentEmptyCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedCommentEmptyCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (PostTableViewCell*)processFeedViewCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    PostTableViewCell *cell = (PostTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PostTableViewCell" forIndexPath:indexPath];
    //FeedViewCell *cell = (FeedViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedViewCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dictData=[arrayFeedData objectAtIndex:indexPath.section];
    
    [cell.activityIndicator setHidden:YES];
    
    cell.lbluserName.text=[dictData objectForKey:@"user_name"];
    cell.btnComment.tag=indexPath.row;
    [cell.btnComment addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment2 addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.audioButton addTarget:self action:@selector(audioBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnBoneTag addTarget:self action:@selector(boneButtonDidPushed:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnLike.tag=indexPath.row;
    [cell.btnLike setTitle:[NSString stringWithFormat:@"%@ Likes",[dictData objectForKey:@"total_like"]] forState:UIControlStateNormal];
    [cell.btnLike addTarget:self action:@selector(showLikePopup:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnLikeCenter addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.playVideoButton addTarget:self action:@selector(playVideoButtonDidPushed:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnMore.tag=indexPath.row;
    [cell.btnMore addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_image"]];
    
    //for caching image is necessory then use this.
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL]
                                                                     cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgPet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"image_626"] success:nil failure:nil];
    
    [cell.playVideoButton setHidden:YES];
    [cell.moviePlayer.view setHidden:YES];
    
    [cell.imgPet setHidden:NO];
    [cell.moviePlayer.view setHidden:YES];
    [cell.playVideoButton setHidden:YES];
    [cell.audioButton setHidden:YES];
    
    if (self.fullVisible && !self.isScrolling) {
        if ([dictData objectForKey:@"video_url"]) {
            //[cell setBackgroundColor:[UIColor redColor]];
            NSData *strval = [Base64 decode:[dictData objectForKey:@"post_status"]];
            NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
            
            NSURL *videoURL = [NSURL URLWithString:[dictData objectForKey:@"video_url"]];
            //NSURL *videoURL = [[NSBundle mainBundle] URLForResource:@"Kunto Aji - Terlalu Lama Sendiri" withExtension:@"mp4"];
            //NSURL *videoURL = [NSURL URLWithString:@"http://nordenmovil.com/urrea/InstalaciondelavaboURREAbaja.mp4"];
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            
            dispatch_async(queue, ^{
                cell.avPlayerItem = [AVPlayerItem playerItemWithURL:videoURL];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    cell.avPlayer = [AVPlayer playerWithPlayerItem:cell.avPlayerItem];
                    cell.avLayer = [AVPlayerLayer playerLayerWithPlayer:cell.avPlayer];
                    cell.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                    
                    [self makeItPlay:cell];
                });
            });
            
        }
    } else {
        NSLog(@"cell fullVisible: %@", (self.fullVisible)?@"YES":@"NO");
        NSLog(@"cell isScrolling: %@", (self.isScrolling)?@"YES":@"NO");
        //[cell setBackgroundColor:[UIColor grayColor]];
        [self makeItStahp:cell];
    }
        
    /*[cell.avPlayer addObserver:self
                    forKeyPath:@"currentItem.loadedTimeRanges"
                       options:NSKeyValueObservingOptionNew
                       context:nil];*/
        

    /*cell.lblStatus.text=[NSString stringWithFormat:@"%@ %@",[dictData objectForKey:@"user_name"],[dictData objectForKey:@"post_status"]];
    NSData *strval = [Base64 decode:[dictData objectForKey:@"post_status"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    cell.lblStatus.text=[NSString stringWithFormat:@"%@ %@",[dictData objectForKey:@"user_name"],
                         [strTextMsg isEqualToString:@""]?[dictData objectForKey:@"post_status"]:strTextMsg];
    
    NSString *MainCommentString= cell.lblStatus.text;
    NSString *strUsername=[dictData objectForKey:@"user_name"];
    NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainCommentString];
    NSRange rangeStatus=  [MainCommentString rangeOfString:MainCommentString];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeStatus];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeStatus];
    NSRange rangeUsername=  [MainCommentString rangeOfString:[NSString stringWithFormat:@"%@",strUsername]];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0 green:176.0/255.0 blue:222.0/255.0 alpha:1.0] range:rangeUsername];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
    
    for (int i=0; i<[[dictData objectForKey:@"tag_hash"] count]; i++)
    {
        NSString *strHashTag;
        NSString *strHash;
        
        if ([[dictData objectForKey:@"tag_hash"]count]>0)
        {
            strHashTag=[NSString stringWithFormat:@"#%@",[[dictData objectForKey:@"tag_hash"] objectAtIndex:i] ];
            strHash=[[dictData objectForKey:@"tag_hash"] objectAtIndex:i];
        }
        
        NSRange rangeHashTag=  [MainCommentString rangeOfString:[NSString stringWithFormat:@"%@",strHashTag]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeHashTag];
        cell.lblStatus.delegate=self;
        cell.lblStatus.tag=1;
        NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName, nil];
        NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
        NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        cell.lblStatus.linkAttributes = linkAttributes;
        [cell.lblStatus addLinkToPhoneNumber:strHash withRange:rangeHashTag];
        
    }
    cell.lblStatus.delegate=self;
    cell.lblStatus.tag=1;
    [cell.lblStatus addLinkToAddress:dictData withRange:rangeUsername];
    cell.lblStatus.attributedText=string;*/
    
    cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"total_like"]];
    cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"total_comment"]];
    
    if ([[dictData objectForKey:@"like"] boolValue]==YES) {
        [cell.btnLike setTag:likeBtnTag];
        [cell.btnLikeCenter setTag:likeBtnTag];
        
        [cell.btnLikeCenter setImage:[UIImage imageNamed:@"center-unlike-button2.png"] forState:UIControlStateNormal];
        
        //[cell.btnLike setImage:nil forState:UIControlStateNormal];
        //[cell.btnLike setImage:[UIImage imageNamed:@"dark_likeBox"] forState:UIControlStateNormal];
    } else {
        [cell.btnLike setTag:disLikeBtnTag];
        [cell.btnLikeCenter setTag:disLikeBtnTag];
        
        [cell.btnLikeCenter setImage:[UIImage imageNamed:@"center-like-button.png"] forState:UIControlStateNormal];
        
        //[cell.btnLike setImage:nil forState:UIControlStateNormal];
        //[cell.btnLike setImage:[UIImage imageNamed:@"light_likeBox"] forState:UIControlStateNormal];
    }
    
    //cell.lblTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[dictData objectForKey:@"date"]]];

    CGRect frame = cell.lblStatus.frame;
    [cell.lblStatus setNumberOfLines:0];
    [cell.lblStatus setLineBreakMode:NSLineBreakByWordWrapping];
    
    
    CGFloat height=[[dictData objectForKey:@"post_status"] getHeightOfTextForFontSize:cell.lblStatus.font.pointSize withLabelWidth:frame.size.width];
    [cell.lblStatus setFrame:CGRectMake(cell.lblStatus.frame.origin.x,
                                        cell.lblStatus.frame.origin.y,
                                        frame.size.width,
                                        height+7)];
    
    if ([[dictData objectForKey:@"tag"] count]==0)
    {
        cell.lblTaggedUsers.hidden=YES;
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.operationView.frame.origin.y+cell.operationView.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
    }
    else
    {
        cell.lblTaggedUsers.hidden=NO;//cell.btnLike.frame.origin.y+cell.btnLike.frame.size.height
        cell.lblTaggedUsers.hidden=YES;
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.lblTaggedUsers.frame.origin.y+cell.lblTaggedUsers.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
        
        NSString *strUserName=[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"];
        
        
        if ([[dictData objectForKey:@"tag"] count]==1) {
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ is tagged",strUserName];
            
            
            NSString *MainPostString=[NSString stringWithFormat:@"%@ is tagged",strUserName];
            
            
            NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
            
            
            NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
            
            
            NSRange rangeIsTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" is tagged"]];
            
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeIsTagged];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeIsTagged];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
            
            cell.lblTaggedUsers.delegate=self;
            cell.lblTaggedUsers.tag=2;
            
            NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                             , nil];
            
            NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            cell.lblTaggedUsers.linkAttributes = linkAttributes;
            [cell.lblTaggedUsers addLinkToAddress:dictData withRange:rangeUsername];
            
            cell.lblTaggedUsers.attributedText=string;
        } else {
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ and %lu more are tagged",strUserName,[[dictData objectForKey:@"tag"]count]-1];
            
            NSString *MainPostString=[NSString stringWithFormat:@"%@ and %lu more are tagged",strUserName,[[dictData objectForKey:@"tag"]count]-1];
            
            
            NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
            
            NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
            NSRange rangeNumberofTaggedUsers=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%lu more",[[dictData objectForKey:@"tag"]count]-1]];
            
            NSRange rangeAnd=  [MainPostString rangeOfString:[NSString stringWithFormat:@" and "]];
            NSRange rangeAreTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" are tagged"]];
            
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAnd];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAnd];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAreTagged];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAreTagged];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeNumberofTaggedUsers];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeNumberofTaggedUsers];
            cell.lblTaggedUsers.delegate=self;
            cell.lblTaggedUsers.tag=2;
            NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                             , nil];
            NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            cell.lblTaggedUsers.linkAttributes = linkAttributes;
            [cell.lblTaggedUsers addLinkToPhoneNumber:nil withRange:rangeNumberofTaggedUsers];
            [cell.lblTaggedUsers addLinkToAddress:dictData withRange:rangeUsername];
            cell.lblTaggedUsers.attributedText=string;
        }
    }
    
    
    cell.imgPet.frame = CGRectMake(cell.imgPet.frame.origin.x, cell.imgPet.frame.origin.y, cell.imgPet.frame.size.width, cell.imgPet.frame.size.width);
    cell.operationView.frame = CGRectMake(cell.operationView.frame.origin.x, cell.imgPet.frame.origin.y + cell.imgPet.frame.size.height + 3, cell.operationView.frame.size.width, cell.operationView.frame.size.height);
    
    if (!cell.lblTaggedUsers.hidden)
    {
        cell.lblTaggedUsers.frame = CGRectMake(cell.lblTaggedUsers.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblTaggedUsers.frame.size.width, cell.lblTaggedUsers.frame.size.height);
        cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.lblTaggedUsers.frame.origin.y + cell.lblTaggedUsers.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
    }
    else
    {
        cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
    }
    return cell;
}

- (void)makeItPlay:(PostTableViewCell*)cell
{
    [cell.imgPet setHidden:YES];
    [cell.activityIndicator setHidden:NO];
    [cell.activityIndicator startAnimating];
    AVAsset *currentPlayerAsset = cell.avPlayer.currentItem.asset;
    [cell.audioButton setHidden:NO];
    
    NSLog(@"Prepare to play: %@", [(AVURLAsset *)currentPlayerAsset URL]);
    cell.avLayer.frame = cell.imgPet.frame;
    [cell.contentView.layer addSublayer:cell.avLayer];
    cell.avPlayer.muted = YES;
    [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-muted-icon.png"] forState:UIControlStateNormal];
    [cell.avPlayer play];
    
    self.currentPlayedVideoCell = cell;

}

- (void)makeItStahp:(PostTableViewCell*)cell
{
    [cell.moviePlayer.view setHidden:YES];
    [cell.activityIndicator setHidden:YES];
    [cell.audioButton setHidden:YES];
    [cell.imgPet setHidden:NO];
    [cell.avPlayer pause];
    [cell.avLayer removeFromSuperlayer];
    cell.avPlayer = nil;
    cell.avPlayerItem = nil;
    
    [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-muted-icon.png"] forState:UIControlStateNormal];
    
    self.currentPlayedVideoCell = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    NSLog(@"Observe: %@", keyPath);
    
    if([keyPath isEqualToString:@"currentItem.loadedTimeRanges"]){
        /*NSArray *timeRanges = (NSArray*)[change objectForKey:NSKeyValueChangeNewKey];
        if (timeRanges) {
            CMTimeRange timerange=[[timeRanges objectAtIndex:0]CMTimeRangeValue];
            NSLog(@" . . . %.5f -> %.5f", CMTimeGetSeconds(timerange.start), CMTimeGetSeconds(CMTimeAdd(timerange.start, timerange.duration)));
        }*/
    }
}



#pragma mark - Delegate Methods of TTTAttributedLabel -


- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithPhoneNumber:(NSString *)strHashTag{
    
    if (label.tag==1){
        HashTagViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewController"];
        vc.strHashTag=strHashTag;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if(label.tag==2){
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tblFeedList];
        NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
        [self performSegueWithIdentifier:@"TaggedUserSegue" sender:indexPath];
    }
    
    
}

- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithAddress:(NSDictionary *)addressComponents
{
    if (label.tag==1) {
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tblFeedList];
        NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
        if ([[addressComponents objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
            [self.tabBarController setSelectedIndex:4];
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
            
        }else{
            
            [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
        }
    }
    else if(label.tag==2) {
        
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tblFeedList];
        NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
        @try {
            if ([[[arrayFeedData objectAtIndex:indexPath.section] objectForKey:@"tag"] count]>0){
                if ([[[[[arrayFeedData objectAtIndex:indexPath.section] objectForKey:@"tag"] objectAtIndex:0] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
                    [self.tabBarController setSelectedIndex:4];
                    [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
                    
                }else{
                    
                    [self performSegueWithIdentifier:@"TaggedUser" sender:indexPath];
                }
                
            }
        }
        @catch (NSException *exception) {
        }
        @finally {
            
        }
    }
}

 
-(void)setUpFooterView{
    viewFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50)];
    pageLoader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [pageLoader setColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    pageLoader.center = viewFooter.center;
    pageLoader.hidesWhenStopped = YES;
    [viewFooter addSubview:pageLoader];
}

#pragma mark - Pagination & ScrollView Delegate Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    /*self.isScrolling = NO;
    [self.tblFeedList reloadData];
    [self checkWhichVideo];*/
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        self.isScrolling = NO;
        [self checkWhichVideo];
        [self.tblFeedList reloadData];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.isScrolling = YES;
    [self.tblFeedList reloadData];
    [self eraseAllPoptip];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance) {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            if (self.allPostForUser) {
                [self callForAllPost:self.newPage];
            }
            else{
                [self callfeedListData:self.newPage];
            }
            self.tblFeedList.tableFooterView=viewFooter;
            [pageLoader startAnimating];
            
        }
        
        //Put your load more data method here...
    }
    [self checkWhichVideo];
}

- (void)checkWhichVideo
{
    for(UITableViewCell *cell in [self.tblFeedList visibleCells])
    {
        NSIndexPath *indexPath = [self.tblFeedList indexPathForCell:cell];
        CGRect cellRect = [self.tblFeedList rectForRowAtIndexPath:indexPath];
        UIView *superview = self.tblFeedList.superview;
        
        CGRect convertedRect=[self.tblFeedList convertRect:cellRect toView:superview];
        CGRect intersect = CGRectIntersection(self.tblFeedList.frame, convertedRect);
        float visibleHeight = CGRectGetHeight(intersect);
        
        NSDictionary *dictData=[arrayFeedData objectAtIndex:indexPath.section];

        if(visibleHeight>kVIDEO_CELL_SIZE*0.6) { // only if 60% of the cell is visible
            // Play it
            if ([cell isKindOfClass:[PostTableViewCell class]]) {
                PostTableViewCell *moviecell = (PostTableViewCell*)cell;
                if ([dictData objectForKey:@"video_url"]) {
                    self.fullVisible = YES;
                }
            }
        } else {
            // Shut it down
            if ([cell isKindOfClass:[PostTableViewCell class]]) {
                if ([dictData objectForKey:@"video_url"]) {
                    self.fullVisible = NO;
                }
            }
        }
    }
}


#pragma mark - TTTAttributedLabel Delegate Methods
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"Did click label with tag: %ld and URL: %@", (long)label.tag, [url absoluteString]);
    NSArray *urlSplitted = [[url absoluteString] componentsSeparatedByString:@":"];
    if ([urlSplitted objectAtIndex:1]) {
        self.topHeaderLbl.text = [NSString stringWithFormat:@"#%@", [urlSplitted objectAtIndex:1]];
        [self.backBtnOutlet setHidden:NO];
        self.currentSelectedHashtag = [urlSplitted objectAtIndex:1];
        [self callForPostWithHashtag:1];
    }
}

#pragma mark - TipPopup Delegate Methods
- (void)triggerAnywhereFired:(CMPopTipView *)popTipView
{
    [self eraseAllPoptip];
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    
}

- (void)eraseAllPoptip
{
    if (self.poptipArray) {
        for (CMPopTipView *obj in self.poptipArray) {
            [obj dismissAnimated:YES];
        }
        self.poptipArray = nil;
    }
}

@end
