//
//  FeedViewCell.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/27/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MoviePlayerViewController.h"


@interface FeedViewCell : UITableViewCell <TTTAttributedLabel>

@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnComment2;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentCount;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgPet;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbluserName;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblTaggedUsers;
@property (weak, nonatomic) IBOutlet UIView *operationView;
@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;
@property (weak, nonatomic) IBOutlet UIButton *audioButton;

@property (weak, nonatomic) IBOutlet UIButton *btnLikeCenter;
@property (strong, nonatomic) NSIndexPath *indexPath;

@property (retain, nonatomic) MoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) UIView *playerView;
@property (retain, nonatomic) AVPlayer *avPlayer;
@property (nonatomic, strong) AVURLAsset *assetURL;
@property (nonatomic, strong) AVPlayerLayer *avLayer;
@property (nonatomic, strong) AVPlayerItem *avPlayerItem;
@end
