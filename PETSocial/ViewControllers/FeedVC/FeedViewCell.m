//
//  FeedViewCell.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/27/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "FeedViewCell.h"

@implementation FeedViewCell

@synthesize text;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        
        //NSURL *videoURL = [NSURL URLWithString:@"http://52.34.205.159/file/video/2016/01/678d306dd370c54db066a13ccc75d8b7.mov"];
        /*NSURL *videoURL = [NSURL URLWithString:@"http://nordenmovil.com/urrea/InstalaciondelavaboURREAbaja.mp4"];
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
        [self.moviePlayer prepareToPlay];
        self.moviePlayer.shouldAutoplay = YES;
        self.moviePlayer.movieSourceType= MPMovieSourceTypeFile;
        [self.moviePlayer setControlStyle:MPMovieControlStyleDefault];
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
        [self.moviePlayer.view setFrame:CGRectMake(10.0, 74.0, 300.0 , 300.0)];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view addSubview:self.moviePlayer.view];
        });
        [self.moviePlayer play];*/
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    /*self.moviePlayer = [[MoviePlayerViewController alloc] init];
    self.moviePlayer.controlStyle = MPMovieControlStyleDefault;
    self.moviePlayer.movieSourceType= MPMovieSourceTypeFile;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    [self.contentView insertSubview:self.moviePlayer.view belowSubview:self.audioButton];*/
    
    /*self.avPlayer = [[AVPlayer alloc] init];
    
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    layer.frame = self.imgPet.frame;
    [self.contentView.layer addSublayer:layer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];*/
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)layoutSubviews
{
    self.moviePlayer.view.frame = self.imgPet.frame;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
