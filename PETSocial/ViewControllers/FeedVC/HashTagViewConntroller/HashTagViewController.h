//
//  HashTagViewController.h
//  PETSocial
//
//  Created by milap kundalia on 8/5/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HashTagViewController : UIViewController
{

}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionOutview;
@property (strong, nonatomic) NSString *strHashTag;
@property (nonatomic, readwrite) int newPage;
- (IBAction)btnBackAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *topHeaderLbl;

@end
