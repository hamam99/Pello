//
//  LikedViewController.h
//  PETSocial
//
//  Created by Habibi on 1/25/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikedViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UIView *viewForTable;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, readwrite) NSString *itemId;
@property (nonatomic, readwrite) NSString *typeId;
@property (nonatomic, readwrite) NSMutableArray *mainArray;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, readwrite) UIPageControl *pageControl;

- (void)reinit;

@end
