//
//  LikerListTableViewCell.h
//  PETSocial
//
//  Created by Habibi on 1/26/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikerListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgUserOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *imgPetOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblUSername;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgLikeComment;
@end
