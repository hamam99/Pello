//
//  LikerListViewController.m
//  PETSocial
//
//  Created by Habibi on 1/26/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import "LikerListViewController.h"
#import "LikerListTableViewCell.h"

@interface LikerListViewController ()

@end

@implementation LikerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    self.indicatorView.center = self.view.center;
    //[self.view addSubview:self.indicatorView];
    
    [self callLikerList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)callLikerList
{
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSString *strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               strMyUserID,@"user_id",
                               self.typeId,@"type_id",
                               @"liker",@"action",
                               nil];
    
    if ([self.typeId isEqualToString:@"photo"]) {
        [dict setObject:self.itemId forKey:@"photo_id"];
    } else {
        [dict setObject:self.itemId forKey:@"video_id"];
    }
    
    
    [[Connection sharedConnectionWithDelegate:self] getLikerOfPost:dict];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)userProfileNavigation:(UITapGestureRecognizer *)sender
{
    
    /*NSDictionary *dict=[notificationData objectAtIndex:sender.view.tag];
    if ([[dict objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]])
    {
        [self.tabBarController setSelectedIndex:4];
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:sender];
    }*/
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Connection Delegate methods
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATELIKER]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES) {
                    self.mainArray = [dataDict objectForKey:@"data"];
                    
                    /*if (self.mainArray.count>0 && self.mainArray.count<4) {
                        NSDictionary *tmp = [self.mainArray objectAtIndex:0];
                        for (int i=1;i<500;i++) {
                            [self.mainArray addObject:tmp];
                        }
                    }*/
                    
                    [self.tableView reloadData];
                } else {
                    
                }
                
                [SVProgressHUD dismiss];
            }
            
        } else if (strstatuscode == 500) {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        } else {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
    //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Request Timed Out"];
}

#pragma mark - UITableView Delegate & Data Source Methods

- (NSInteger)tableView:(UITableView*)cell numberOfRowsInSection:(NSInteger)section
{
    return [self.mainArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    LikerListTableViewCell *cell = (LikerListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"LikerCell" forIndexPath:indexPath];
    NSDictionary *dict=[self.mainArray objectAtIndex:indexPath.row];
    
    cell.lblUSername.text=[dict objectForKey:@"user_name"];
    cell.lblTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[dict objectForKey:@"time_stamp"]]];
    
    cell.imgUserOutlet.layer.borderWidth = 1.0f;
    cell.imgUserOutlet.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    cell.imgUserOutlet.layer.masksToBounds = NO;
    cell.imgUserOutlet.clipsToBounds = YES;
    cell.imgUserOutlet.layer.cornerRadius = cell.imgUserOutlet.frame.size.width/2;
    
    cell.lblUSername.tag=indexPath.row;
    cell.lblUSername.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userProfileNavigation:)];
    [cell.lblUSername addGestureRecognizer:tapGest];
    tapGest.view.tag=indexPath.row;

    NSString *strimageURL=[NSString stringWithFormat:@"%@/%@",REAL_BASE_URL,[dict objectForKey:@"user_image"]];
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgUserOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    [cell.imgPetOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    
    if ([self.typeId isEqualToString:@"photo"]) {
        cell.imgLikeComment.image=[UIImage imageNamed:@"icn_like"];
        [cell.lblInfoOutlet setFrame:CGRectMake(cell.lblInfoOutlet.frame.origin.x, 45, cell.lblInfoOutlet.frame.size.width, cell.lblInfoOutlet.frame.size.height)];
        cell.lblInfoOutlet.text=@"Loved Your Photo";
        [cell.lblInfoOutlet setNumberOfLines:1];
    } else {
        cell.imgLikeComment.image=[UIImage imageNamed:@"icn_like"];
        [cell.lblInfoOutlet setFrame:CGRectMake(cell.lblInfoOutlet.frame.origin.x, 45, cell.lblInfoOutlet.frame.size.width, cell.lblInfoOutlet.frame.size.height)];
        cell.lblInfoOutlet.text=@"Loved Your Video";
        [cell.lblInfoOutlet setNumberOfLines:1];
    }
    
    
    return cell;
}

@end
