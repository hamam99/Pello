//
//  LocationVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/10/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *topHeaderView;
- (IBAction)backBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;
@property (nonatomic,readwrite)   int newPage;
@property (weak, nonatomic) NSString *locationName;
@end
