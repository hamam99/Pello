//
//  LocationVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/10/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "LocationVC.h"
#import "ExploreCell.h"
#import "SinglePostVC.h"

@interface LocationVC ()

{
    NSMutableArray *locationDataArray;
    BOOL isMoreData;
    UIRefreshControl *refreshControl;
    BOOL isValidateData;
    
}
@end

@implementation LocationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.topHeaderView setTitle:self.locationName forState:UIControlStateNormal];
    [self preferredStatusBarStyle];
    locationDataArray=[[NSMutableArray alloc]init];
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    [self locationBasedData:1];
    [self setUpPulltoRefreshView];
}
-(void)setUpPulltoRefreshView{
    refreshControl= [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl setTintColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],
                                        };
    NSString *s = @"Refreshing...";
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:s];
    
    [attributeString setAttributes:refreshAttributes range:NSMakeRange(0, attributeString.length)];
    refreshControl.attributedTitle=attributeString;
    
    
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    [self.collectionViewOutlet addSubview:refreshControl];
    self.collectionViewOutlet.alwaysBounceVertical = YES;
}
-(void)refershControlAction{
    isValidateData=NO;
    [self locationBasedData:1];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
        
    });
}

-(void)locationBasedData:(int)page{
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",self.locationName,@"location_name",[NSString stringWithFormat:@"%d",page],@"page", nil];
    [[Connection sharedConnectionWithDelegate:self] locationData:dict];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated{
  
}
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     NSIndexPath *indexPath=(NSIndexPath *)sender;
     if ([segue.identifier isEqualToString:@"SinglePostVC"]) {
         SinglePostVC *singlePost=[segue destinationViewController];
         singlePost.postDataDetail=[locationDataArray objectAtIndex:indexPath.item];
     }
     
 }


- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UICollectionView Datasource and Delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    
    
    if (indexPath.row==locationDataArray.count && locationDataArray.count>collectionItems) {
        return CGSizeMake(300.f * fltMultiplyingFactor, 50.f * fltMultiplyingFactor);
    }
    else{
        return CGSizeMake(100.f  * fltMultiplyingFactor, 100.f  * fltMultiplyingFactor);
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    if (locationDataArray.count) {
        if (isMoreData) {
            return locationDataArray.count+1;
        }
        else{
            return locationDataArray.count;
        }
    }
    else{
        return 0;
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.item<locationDataArray.count)
    {
        ExploreCell *myCell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell" forIndexPath:indexPath];
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[[locationDataArray objectAtIndex:indexPath.item] objectForKey:@"post_image"]];
        if(([strimageURL length] > 0)){
//            myCell.petImageOutlet.imageURL = [NSURL URLWithString:strimageURL];
            //for caching image is necessory then use this.
            NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
            [myCell.petImageOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_200"] success:nil failure:nil];
        }
        return myCell;
    }
    else{
        ExploreCell *cell = (ExploreCell *)[self.collectionViewOutlet dequeueReusableCellWithReuseIdentifier:@"SeeMoreCell" forIndexPath:indexPath];
         cell.grayIndicatorOutlet.frame=CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, cell.grayIndicatorOutlet.frame.size.width, cell.grayIndicatorOutlet.frame.size.height);
        [cell.grayIndicatorOutlet startAnimating];
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"SinglePostVC" sender:indexPath];
}


#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString* )nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATELOCATION]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    if (!isValidateData) {
                        [refreshControl endRefreshing];
                        locationDataArray=[[NSMutableArray alloc]init];
                        locationDataArray=[dataDict objectForKey:@"data"];
                    }
                    else{
                        locationDataArray=[[locationDataArray arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    [self.collectionViewOutlet reloadData];
                 }
                else{
                 }
 
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }
#pragma mark Pagination
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            [self locationBasedData:self.newPage];
            //            self.collectionOutview.tableFooterView=viewFooter;
        }
        //Put your load more data method here...
    }
}

@end
