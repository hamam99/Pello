//
//  MoviePlayerViewController.h
//  PETSocial
//
//  Created by Habibi on 1/11/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface MoviePlayerViewController : MPMoviePlayerController

@end
