//
//  MoviePlayerViewController.m
//  PETSocial
//
//  Created by Habibi on 1/11/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import "MoviePlayerViewController.h"

@implementation MoviePlayerViewController

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"MoviePlayerViewController viewDidAppear");
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"MoviePlayerViewController viewWillAppear");
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"MoviePlayerViewController viewWillDisappear");
}

@end
