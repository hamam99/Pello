//
//  SharingActionSheet.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/30/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
#import "ReportContentButton.h"
#import "STTwitter.h"

@protocol SharingActionSheetDelegate <NSObject>
- (void)reportBtnDidPushed;
@end

@interface SharingActionSheet : UIViewController<UIDocumentInteractionControllerDelegate>
@property (nonatomic, assign) id<SharingActionSheetDelegate> delegate;
- (IBAction)cancelBtnAction:(UIButton *)sender;
- (IBAction)fbSharingAction:(UIButton *)sender;
- (IBAction)twSharingAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *shareViewDelete;
- (void)presentActionSheet;
@property (nonatomic, strong) ACAccountStore *accountStore;

- (IBAction)ReportPostAction:(id)sender;
@property (nonatomic, strong) ACAccount *facebookAccount;
- (IBAction)deletePostAction:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *shareViewOutlet;
@property (strong, nonatomic) NSDictionary *postData;
@property (weak, nonatomic) IBOutlet ReportContentButton *btnReportContent;
@property (assign, nonatomic) BOOL deleteBtnNeeded;

@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *instagramButton;
@property (strong, nonatomic) STTwitterAPI *twitter;

@property (nonatomic, strong) UIDocumentInteractionController *documentController;
@property (nonatomic, readwrite) UIImage *sharedImage;

@property (nonatomic, readwrite) BOOL isFBVideoSharing;

@end
