//
//  SharingActionSheet.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/30/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "SharingActionSheet.h"
#import <Accounts/Accounts.h>
#import "Constant.h"
#import "SVProgressHUD.h"

@interface SharingActionSheet ()

@end

@implementation SharingActionSheet

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    [super viewDidLoad];
    UIImageView *backImage=(UIImageView *)[[self view] viewWithTag:245];
    [backImage setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(outSideViewAction:)];
    [backImage addGestureRecognizer:tapGesture];
    _accountStore = [[ACAccountStore alloc] init];
    // Do any additional setup after loading the view.
    [self.facebookButton.layer setBorderWidth:1.0f];
    [self.facebookButton.layer setBorderColor:[UIColor colorWithRed:233.0f/250.0f green:233.0f/250.0f blue:233.0f/250.0f alpha:1.0f].CGColor];
    [self.facebookButton.layer setCornerRadius:4.0f];
    
    [self.twitterButton.layer setBorderWidth:1.0f];
    [self.twitterButton.layer setBorderColor:[UIColor colorWithRed:233.0f/250.0f green:233.0f/250.0f blue:233.0f/250.0f alpha:1.0f].CGColor];
    [self.twitterButton.layer setCornerRadius:4.0f];
    
    [self.instagramButton.layer setBorderWidth:1.0f];
    [self.instagramButton.layer setBorderColor:[UIColor colorWithRed:233.0f/250.0f green:233.0f/250.0f blue:233.0f/250.0f alpha:1.0f].CGColor];
    [self.instagramButton.layer setCornerRadius:4.0f];
}
-(void)viewWillAppear:(BOOL)animated{
    //  [self presentActionSheet];
    
    self.shareViewOutlet.hidden = TRUE;
    CGRect frame = self.shareViewOutlet.frame;
    frame.origin = CGPointMake(0.0, self.view.bounds.size.height+frame.size.height);
    self.shareViewOutlet.frame = frame;
    
    self.shareViewDelete.hidden = TRUE;
    CGRect frame1 = self.shareViewDelete.frame;
    frame1.origin = CGPointMake(0.0, self.view.bounds.size.height+frame1.size.height);
    self.shareViewDelete.frame = frame1;
    
    [[APPDELEGATE lblNotificationCount] setHidden:YES];
    [[APPDELEGATE viewNewNotification] setHidden:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [[APPDELEGATE lblNotificationCount] setHidden:NO];
    [[APPDELEGATE viewNewNotification] setHidden:NO];
}
-(void)outSideViewAction:(UITapGestureRecognizer *)sender{
    [self slideOut];
}
- (void)presentActionSheet {
    
    
    if (self.deleteBtnNeeded) {
        
        
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.shareViewDelete.frame;
            frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
            self.shareViewDelete.frame = frame;
           // [self.view addSubview:self.shareViewDelete];
            frame.origin = CGPointMake(0.0, self.view.bounds.size.height - self.shareViewDelete.bounds.size.height);
            self.shareViewDelete.frame = frame;
            self.shareViewDelete.hidden = FALSE;
            [self.shareViewDelete setUserInteractionEnabled:YES];
        } completion:^(BOOL finished) {
            
        }];
        [[[self view] viewWithTag:245] setAlpha:0];
        [UIView animateWithDuration:0.1 animations:^{
            [[[self view] viewWithTag:245] setAlpha:1];
        }];
    }
    else {
        
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.shareViewOutlet.frame;
            frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
            self.shareViewOutlet.frame = frame;
       //     [self.view addSubview:self.shareViewOutlet];
            frame.origin = CGPointMake(0.0, self.view.bounds.size.height - self.shareViewOutlet.bounds.size.height);
            self.shareViewOutlet.frame = frame;
            [self.shareViewOutlet setUserInteractionEnabled:YES];
            self.shareViewOutlet.hidden = FALSE;
        } completion:^(BOOL finished) {
            
        }];
        [[[self view] viewWithTag:245] setAlpha:0];
        [UIView animateWithDuration:0.1 animations:^{
            [[[self view] viewWithTag:245] setAlpha:1];
        }];

    }
    
    //    [self.tabBarController removeFromParentViewController];
}
- (IBAction)reportButtonTapped:(id)sender
{
    [self slideOut];
 }

- (void) slideOut {
    if (self.deleteBtnNeeded) {
        [UIView animateWithDuration:0.2 animations:^{
            // Move this view to bottom of superview
            CGRect frame = self.shareViewDelete.frame;
            frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
            self.shareViewDelete.frame = frame;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                // Move this view to bottom of superview
                [self.view setAlpha:0];
            } completion:^(BOOL finished) {
                
                [self.view removeFromSuperview];
            }];
        }];
        
    }
    else {
        [UIView animateWithDuration:0.2 animations:^{
            // Move this view to bottom of superview
            CGRect frame = self.shareViewOutlet.frame;
            frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
            self.shareViewOutlet.frame = frame;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                // Move this view to bottom of superview
                [self.view setAlpha:0];
            } completion:^(BOOL finished) {
                
                [self.view removeFromSuperview];
            }];
        }];
        
    }
    //    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)cancelBtnAction:(UIButton *)sender {
    //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self slideOut];
}
-(IBAction)deletePostAction:(UIButton *)sender
{
    
    //Prashant
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Are you sure you want to delete the post?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=5000;
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==5000) {
        if (buttonIndex==1) {
            NSInteger postUserId=[[self.postData objectForKey:@"user_id"] integerValue];
            NSInteger userId=[[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"] integerValue];
            
            if (postUserId==userId) {
                [APPDELEGATE ShowHUDWith:@"Loading..."];
                NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[self.postData objectForKey:@"photo_id"],@"photo_id",[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id", nil];
                [[Connection sharedConnectionWithDelegate:self] deletePost:dict];
            }
            else{
                [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"You can't delete other user's post."];
                [self slideOut];
            }

        }
    }
}

- (IBAction)fbSharingAction:(UIButton *)sender
{
    //Prashant
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [APPDELEGATE ShowHUDWith:@"Loading..."];
    }];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [ self ShareNewPostOnFB];
    }];

    [self slideOut];
}

- (IBAction)twSharingAction:(UIButton *)sender
{
    NSString *strTwitterToken       = kTwitterAccessToken;
    NSString *strTwitterTokenSecret = kTwitterAccessTokenSecret;
    
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    if (strTwitterToken && strTwitterTokenSecret)
    {
        self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:kTwitterConsumerKey consumerSecret:kTwitterConsumerSecret oauthToken:kTwitterAccessToken oauthTokenSecret:kTwitterAccessTokenSecret];
        [self.twitter verifyCredentialsWithUserSuccessBlock:^(NSString *username, NSString *userID) {
            [SVProgressHUD dismiss];
            [self twitterMediaUpload];
        } errorBlock:^(NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"Twitter login error: %@", error.description);
        }];
    } else {
        [SVProgressHUD dismiss];
        NSLog(@"Twitter login error");
    }
}

-(void)twitterMediaUpload
{
    if ([self.postData objectForKey:@"video_url"]) {
        NSString *path = [self.postData objectForKey:@"video_url"];
        NSURL *url = [NSURL URLWithString:path];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        [SVProgressHUD dismiss];
        NSLog(@"Uploading now!");
        [APPDELEGATE ShowHUDWith:@"Uploading..."];
        
        [self.twitter postMediaUploadThreeStepsWithVideoURL:url uploadProgressBlock:^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
            [SVProgressHUD showProgress:(float)totalBytesWritten/(float)totalBytesExpectedToWrite status:@"Uploading..."];
        } successBlock:^(NSString *mediaID, NSString *size, NSString *expiresAfter, NSString *videoType) {
            [self postToTheTwitterWithMediaId:mediaID];
        } errorBlock:^(NSError *error) {
            NSLog(@"Twitter upload error: %@", error.description);
        }];
        
    } else {
        NSString *path = [_postData objectForKey:@"post_image"];
        NSURL *imageURL = [NSURL URLWithString:path];
        [SVProgressHUD dismiss];
        NSLog(@"Uploading now!");
        [APPDELEGATE ShowHUDWith:@"Uploading..."];
        [self.twitter postMediaUpload:imageURL uploadProgressBlock:^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
            [SVProgressHUD showProgress:(float)totalBytesWritten/(float)totalBytesExpectedToWrite status:@"Uploading..."];
        } successBlock:^(NSDictionary *imageDictionary, NSString *mediaID, NSString *size) {
            
            [self postToTheTwitterWithMediaId:mediaID];
        } errorBlock:^(NSError *error) {
            NSLog(@"Twitter upload error: %@", error.description);
        }];
    }
    
}

-(void)postToTheTwitterWithMediaId:(NSString *)mediaID
{
    NSData *strval = [Base64 decode:[_postData objectForKey:@"post_status"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    [APPDELEGATE ShowHUDWith:@"Posting..."];
    [self.twitter postStatusUpdate:strTextMsg inReplyToStatusID:nil mediaIDs:[NSArray arrayWithObject:mediaID] latitude:nil longitude:nil placeID:nil displayCoordinates:nil trimUser:nil successBlock:^(NSDictionary *status) {
        NSLog(@"Description %@",status.description);
        [SVProgressHUD dismiss];
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Status Shared on Twitter!"];
    } errorBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
        //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:[error description]];
        NSLog(@"Twitter posting error %@",error.description);
    }];
    
}

- (IBAction)instagramSharingAction:(id)sender
{
    [SVProgressHUD show];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [APPDELEGATE ShowHUDWith:@"Uploading..."];
        
        NSString *path = [_postData objectForKey:@"post_image"];
        NSURL *url = [NSURL URLWithString:path];
        NSData *tmpdata = [NSData dataWithContentsOfURL:url];
        UIImage *imageMain = [UIImage imageWithData:tmpdata];

        CGFloat cropVal = (imageMain.size.height > imageMain.size.width ? imageMain.size.width : imageMain.size.height);
        
        cropVal *= [imageMain scale];
        
        CGRect cropRect = (CGRect){.size.height = cropVal, .size.width = cropVal};
        CGImageRef imageRef = CGImageCreateWithImageInRect([imageMain CGImage], cropRect);
        
        NSData *imageData = UIImageJPEGRepresentation([UIImage imageWithCGImage:imageRef], 1.0);
        CGImageRelease(imageRef);
        
        NSString *writePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"instagram.igo"];
        if (![imageData writeToFile:writePath atomically:YES]) {
            // failure
            NSLog(@"image save failed to path %@", writePath);
            return;
        } else {
            // success.
        }
        
        [SVProgressHUD dismiss];
        
        NSData *strval = [Base64 decode:[_postData objectForKey:@"post_status"]];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        
        // send it to instagram.
        NSURL *fileURL = [NSURL fileURLWithPath:writePath];
        self.documentController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        self.documentController.delegate = self;
        [self.documentController setUTI:@"com.instagram.exclusivegram"];
        [self.documentController setAnnotation:@{@"InstagramCaption" : strTextMsg}];
        [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 480) inView:self.view animated:YES];
    } else {
        [SVProgressHUD dismiss];
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Instagram not found"];
    }
}


//
#pragma mark - FB sharing methods -

-(void)ShareNewPostOnFB
{
    if (!FBSession.activeSession.isOpen)
    {
        [self openSessionWithAllowLoginUI:YES];
    } else {
        NSString *fbGraphURL = @"me/photos";
        NSMutableDictionary * params = nil;
        if ([self.postData objectForKey:@"video_url"]) {
            fbGraphURL = @"me/videos";
            NSString *path = [self.postData objectForKey:@"video_url"];
            NSURL *url = [NSURL URLWithString:path];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            NSData *strval = [Base64 decode:[_postData objectForKey:@"post_status"]];
            NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];

            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                      @"Pello Video Sharing", @"title",
                      strTextMsg, @"description",
                      @"video/mp4",@"contentType",
                      data, @"video.mp4",
                      nil];
        } else {
            NSString *path = [_postData objectForKey:@"post_image"];
            NSURL *url = [NSURL URLWithString:path];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            NSData *strval = [Base64 decode:[_postData objectForKey:@"post_status"]];
            NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
            //[_postData objectForKey:@"post_status"]
            
            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                            strTextMsg, @"message",
                                            data, @"source",
                                            nil];
        }
        
        [FBRequestConnection startWithGraphPath:fbGraphURL
                                     parameters:params
                                     HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  
                                  if (error) {
                                      [SVProgressHUD dismiss];
                                  } else {
                                      [SVProgressHUD dismiss];
                                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE
                                                                                          message:@"Successfully posted"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                      [alertView show];
                                  }
                              }];
        
        

        
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI
{
    NSArray *permissions = @[@"publish_actions"];
    
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                             if (error) {
                                              } else {
                                                 [self ShareNewPostOnFB];
                                             }
                                         }];
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEDELETEPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {

                    [[NSNotificationCenter defaultCenter] postNotificationName:DeletePost object:nil];
                }
                else{
                 }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        [self slideOut];
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
        [self slideOut];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
    [self slideOut];
 }


- (IBAction)ReportPostAction:(id)sender {
    [self justCloseItAndDoNuffin];
    if ([self.delegate respondsToSelector:@selector(reportBtnDidPushed)]) {
        [self.delegate reportBtnDidPushed];
    }
}

- (void)justCloseItAndDoNuffin
{
    [UIView animateWithDuration:0.2 animations:^{
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            // Move this view to bottom of superview
            [self.view setAlpha:0];
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
        }];
    }];
}

@end
