//
//  SinglePostVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/11/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "SinglePostVC.h"
#import "FeedViewCell.h"
#import "UserProfileVC.h"
#import "TaggedUsersViewController.h"
#import "HashTagViewController.h"
#import "SinglePostTableViewCell.h"
#import "CustomUtilities.h"
#import "ReportVC.h"

@interface SinglePostVC ()
{
    NSString *strUserId;
}
@end

@implementation SinglePostVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // _tableViewOutlet.dataSource = nil;
    [self preferredStatusBarStyle];
    
    //calling a service for getting full data of post
    
    self.commentArray = [[NSMutableArray alloc] init];
    UINib * singleCommentCell = [UINib nibWithNibName:@"SinglePostTableViewCell" bundle:nil];
    [self.tableViewOutlet registerNib:singleCommentCell forCellReuseIdentifier:@"singleCommentCell"];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    
    strUserId=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    [self callSinglePostData];
}
-(void)viewDidAppear:(BOOL)animated{
}
-(void)callSinglePostData
{
    
    //{"photo_id":"105","user_id":"42"}
    if (![self.postDataDetail objectForKey:@"photo_id"]) {
        
        _tableViewOutlet.hidden = YES;
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[self.postDataDetail objectForKey:@"item_id"],@"photo_id",strUserId,@"user_id", nil];
        [[Connection sharedConnectionWithDelegate:self]getSinglePostData:dict];
    }
    else{
        _tableViewOutlet.hidden = YES;
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[self.postDataDetail objectForKey:@"photo_id"],@"photo_id",strUserId,@"user_id",nil];
        [[Connection sharedConnectionWithDelegate:self]getSinglePostData:dict];
    }
    
    
    
}
-(void)callCommentListDataForPhotoID:(NSString*)strPhotoID{
    //{"photo_id":"1"}
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strPhotoID,@"photo_id", nil];
    [[Connection sharedConnectionWithDelegate:self]GetComment:dict];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)moreBtnAction:(UIButton *)sender{
    //    self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
    //    self.sharingSheet.postData=self.postDataDetail;
    //    [[APPDELEGATE window] addSubview:self.sharingSheet.view];
    //    [self.sharingSheet presentActionSheet];
    
    
    int postUserId=[[self.postDataDetail objectForKey:@"user_id"] intValue];
    int userId=[[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"] intValue];
    
    if (postUserId==userId) {
        self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
        self.sharingSheet.postData=self.postDataDetail;
        self.sharingSheet.deleteBtnNeeded=YES;
        [[APPDELEGATE window] addSubview:self.sharingSheet.view];
        self.sharingSheet.delegate = self;
        [self.sharingSheet presentActionSheet];
    }
    else {
        self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
        self.sharingSheet.postData=self.postDataDetail;
        self.sharingSheet.deleteBtnNeeded=NO;
        [[APPDELEGATE window] addSubview:self.sharingSheet.view];
        self.sharingSheet.btnReportContent.intItemId = [[self.sharingSheet.postData valueForKey:@"photo_id"] integerValue];
        self.sharingSheet.btnReportContent.intUserId = postUserId;
        self.sharingSheet.delegate = self;
        [self.sharingSheet presentActionSheet];
    }
    
    
    
}



-(void)commentBtnAction:(UIButton *)sender{
    [self performSegueWithIdentifier:@"CommentSegue" sender:sender];
}
-(void)likeBtnAction:(UIButton *)sender{
    NSString *likeFlag;
    if (sender.tag==likeBtnTag) {
        likeFlag= @"1";
    }
    else if (sender.tag==disLikeBtnTag) {
        likeFlag= @"0";
    }
    // {"user_id":"","type_id":"photo","photo_id":"","like":"1"}
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSString *photoid=[self.postDataDetail objectForKey:@"photo_id"];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                        strUserId,@"user_id",
                        photoid,@"photo_id",
                        @"photo",@"type_id",
                        likeFlag,@"like",
                        nil];
    [[Connection sharedConnectionWithDelegate:self] likeDislike:dict];
}
-(void)userProfileNavigation:(UITapGestureRecognizer *)sender
{
    if ([[self.postDataDetail objectForKey:@"user_id"] isEqualToString:strUserId]) {
        [self.tabBarController setSelectedIndex:4];
        
        
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:sender];
    }
    
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        _tableViewOutlet.hidden = NO;
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            NSString *photoid = [self.postDataDetail objectForKey:@"photo_id"];
            if ([nState isEqualToString:STATELIKEDISLIKE]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    self.postDataDetail=[[dataDict objectForKey:@"data"] objectAtIndex:0];
                    [self.tableViewOutlet reloadData];
                    [SVProgressHUD dismiss];

                }
                else{
                    // [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                    [SVProgressHUD dismiss];
                }
            }
            else if([nState isEqualToString:STATESINGLEPOST]){
                if ([[dataDict valueForKey:@"IsSuccess"]boolValue]==YES) {
                    self.postDataDetail=[[dataDict objectForKey:@"data"] objectAtIndex:0];
                    //[self.tableViewOutlet reloadData];
                    //[SVProgressHUD dismiss];
                    
                    // Call comment
                    [self callCommentListDataForPhotoID:photoid];
                }
                else{
                    //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                    [SVProgressHUD dismiss];
                }
            }
            
            // Comment Conditions
            if ([nState isEqualToString:STATEGETCOMMENT]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES ) {
                    self.commentArray = [NSMutableArray arrayWithArray:[dataDict objectForKey:@"data"]];
                } else {
                    [self.commentArray removeAllObjects];
                }
                NSLog(@"before reload: %@", NSStringFromCGRect(self.tableViewOutlet.frame));
                [self.tableViewOutlet reloadData];
                NSLog(@"AFTER reload: %@", NSStringFromCGRect(self.tableViewOutlet.frame));
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEADDCOMMENT]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [self callCommentListDataForPhotoID:photoid];
                }
                else{
                    
                    [SVProgressHUD dismiss];
                }
            }
            else if ([nState isEqualToString:STATEDELETECOMMENT]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [self.tableViewOutlet reloadData];
                    [self callCommentListDataForPhotoID:photoid];
                }
                else{
                    
                    [SVProgressHUD dismiss];
                }
            }
            
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}

- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    _tableViewOutlet.hidden = NO;
    [SVProgressHUD dismiss];
    //    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:nData];
}
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if ([[arrayFeedData objectAtIndex:section] objectForKey:@"photo"]) {
    //        return [[[arrayFeedData objectAtIndex:section]objectForKey:@"photo"] count];
    //    }
    //    else{
    //        return 1;
    //    }
    
    NSInteger retVal = 1;
    if (self.commentArray && self.commentArray.count>0) {
        retVal = retVal + [self.commentArray count];
    }
    return retVal;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 55.0)];
    [v setBackgroundColor:[UIColor whiteColor]];
    UIImageView *imgV;
    
    imgV=[[UIImageView alloc]initWithImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"]];
    
    if ([[self.postDataDetail objectForKey:@"user_image"] length]>0) {
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[self.postDataDetail objectForKey:@"user_image"]];
        
        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [imgV setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
        
    }
    else{
        imgV=[[UIImageView alloc]initWithImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"]];
    }
    [imgV setFrame:CGRectMake(5, 10, 35, 35)];
    imgV.layer.borderWidth = 1.0f;
    imgV.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    imgV.layer.masksToBounds = NO;
    imgV.clipsToBounds = YES;
    imgV.layer.cornerRadius = imgV.frame.size.width/2;
    
    [v addSubview:imgV];
    
    
    UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(45, 12, 280, 30)];
    lblName.text=[NSString stringWithFormat:@"%@",[self.postDataDetail objectForKey:@"user_name"]];
    lblName.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
    [lblName setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]];
    lblName.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userProfileNavigation:)];
    [lblName addGestureRecognizer:tapGesture];
    [v addSubview:lblName];
    return v;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        NSDictionary *dictData=self.postDataDetail;
        
        CGFloat totalHeight=366;
        
        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        totalHeight = totalHeight * appdelegate.window.frame.size.width/320;
        
        CGFloat height=[[dictData objectForKey:@"post_status"] getHeightOfTextForFontSize:14.0 withLabelWidth:313.0*appdelegate.window.frame.size.width/320];
        
        if ([[dictData objectForKey:@"tag"] count]==0)
        {
            return totalHeight+height;
            
        }
        else{
            return totalHeight+height+20;
            
        }
    } else {
        CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
        NSDictionary *tmp = [self.commentArray objectAtIndex:(indexPath.row-1)];
        NSString *str = [tmp objectForKey:@"text"];
        CGFloat hight=[str getHeightOfTextForFontSize:12.0 withLabelWidth:271*fltMultiplyingFactor];
        return 80+hight;
        
        /*NSString *text = [[dataArray objectAtIndex:indexPath.row] objectForKey:@"text"];
        CGSize size = [CustomUtilities getSizeForText:text maxWidth:320 font:@"Helvetica Neue" fontSize:14];
        return 50 + size.height;*/
        
        //return 80.0f;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return [self processPostCell:tableView cellForRowAtIndexPath:indexPath];
    } else {
        return [self processCommentCell:tableView cellForRowAtIndexPath:indexPath];
    }
    return nil;
}

- (UITableViewCell*)processCommentCell:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    SinglePostTableViewCell * cell  = [tableView dequeueReusableCellWithIdentifier:@"singleCommentCell" forIndexPath:indexPath];
    
    NSInteger index = indexPath.row - 1;
    NSDictionary *curD = [self.commentArray objectAtIndex:index];
    cell.lblUserName.text=[curD objectForKey:@"user_name"];
    //[cell.btnUserNameBtn addTarget:self action:@selector(UserNameBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSData *strval = [Base64 decode:[curD objectForKey:@"text"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    cell.lblCommentText.text=strTextMsg;

    cell.lblCommentTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[curD objectForKey:@"date"]]];
    
    cell.imgProfilePic.layer.borderWidth = 1.0f;
    cell.imgProfilePic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    cell.imgProfilePic.layer.masksToBounds = NO;
    cell.imgProfilePic.clipsToBounds = YES;
    cell.imgProfilePic.layer.cornerRadius = cell.imgProfilePic.frame.size.width/2;
    
    
    if ([[curD objectForKey:@"user_images"] length]>0) {
        
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[curD objectForKey:@"user_images"]];
        
        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [cell.imgProfilePic setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
        
    }
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    CGRect frame=cell.lblCommentText.frame;
    
    [cell.lblCommentText setNumberOfLines:0];
    
    cell.lblCommentText.lineBreakMode = NSLineBreakByTruncatingTail;
    CGFloat hight=[[curD objectForKey:@"text"] getHeightOfTextForFontSize:12.0 withLabelWidth:frame.size.width*fltMultiplyingFactor];
    [cell.lblCommentText setFrame:CGRectMake(cell.lblCommentText.frame.origin.x,
                                             cell.lblUserName.frame.size.height+18,
                                             frame.size.width,
                                             hight+3)];
    
    
    return cell;
}

- (UITableViewCell *)processPostCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    FeedViewCell *cell = (FeedViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedViewCell" forIndexPath:indexPath];
    
    NSDictionary *dictData=self.postDataDetail;
    
    cell.lbluserName.text=[dictData objectForKey:@"user_name"];
    cell.btnComment.tag=indexPath.row;
    [cell.btnComment addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnLike.tag=indexPath.row;
    [cell.btnLike addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_image"]];
    
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgPet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"image_626"] success:nil failure:nil];
    
    
    cell.lblStatus.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_status"]];
    
    
    
    
    
    
    CGRect frame=cell.lblStatus.frame;
    [cell.lblStatus setNumberOfLines:0];
    CGFloat height=[[dictData objectForKey:@"post_status"] getHeightOfTextForFontSize:cell.lblStatus.font.pointSize withLabelWidth:frame.size.width];
    
    [cell.lblStatus setFrame:CGRectMake(cell.lblStatus.frame.origin.x,
                                        cell.lblStatus.frame.origin.y,
                                        frame.size.width,
                                        height+7)];
    
    
    cell.lblStatus.text=[NSString stringWithFormat:@"%@ %@",[dictData objectForKey:@"user_name"],[dictData objectForKey:@"post_status"]];
    
    NSData *strval = [Base64 decode:[dictData objectForKey:@"post_status"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    cell.lblStatus.text=[NSString stringWithFormat:@"%@ %@",[dictData objectForKey:@"user_name"],
                         [strTextMsg isEqualToString:@""]?[dictData objectForKey:@"post_status"]:strTextMsg];
    
    NSString *MainCommentString= cell.lblStatus.text;
    NSString *strUsername=[dictData objectForKey:@"user_name"];
    NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainCommentString];
    NSRange rangeStatus=  [MainCommentString rangeOfString:MainCommentString];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeStatus];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeStatus];
    NSRange rangeUsername=  [MainCommentString rangeOfString:[NSString stringWithFormat:@"%@",strUsername]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
    for (int i=0; i<[[dictData objectForKey:@"tag_hash"] count]; i++)
    {
        NSString *strHashTag;
        NSString *strHash;
        
        if ([[dictData objectForKey:@"tag_hash"]count]>0) {
            strHashTag=[NSString stringWithFormat:@"#%@",[[dictData objectForKey:@"tag_hash"] objectAtIndex:i] ];
            strHash=[[dictData objectForKey:@"tag_hash"] objectAtIndex:i];
            
        }
        
        NSRange rangeHashTag=  [MainCommentString rangeOfString:[NSString stringWithFormat:@"%@",strHashTag]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeHashTag];
        cell.lblStatus.delegate=self;
        cell.lblStatus.tag=1;
        NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                         , nil];
        NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
        NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        cell.lblStatus.linkAttributes = linkAttributes;
        [cell.lblStatus addLinkToPhoneNumber:strHash withRange:rangeHashTag];
    }
    
    cell.lblStatus.delegate=self;
    cell.lblStatus.tag=1;
    [cell.lblStatus addLinkToAddress:dictData withRange:rangeUsername];
    
    cell.lblStatus.attributedText=string;
    
    
    
    cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"total_like"]];
    cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"total_comment"]];
    
    if ([[dictData objectForKey:@"like"] boolValue]==YES) {
        [cell.btnLike setTag:likeBtnTag];
        [cell.btnLike setImage:nil forState:UIControlStateNormal];
        [cell.btnLike setImage:[UIImage imageWithContentsOfBundleFileName:@"dark_likeBox"] forState:UIControlStateNormal];
    }
    else{
        [cell.btnLike setTag:disLikeBtnTag];
        [cell.btnLike setImage:nil forState:UIControlStateNormal];
        [cell.btnLike setImage:[UIImage imageWithContentsOfBundleFileName:@"light_likeBox"] forState:UIControlStateNormal];
    }
    cell.lblTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[dictData objectForKey:@"date"]]];
    cell.btnMore.tag=indexPath.row;
    [cell.btnMore addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //    NSDate *object = _objects[indexPath.row];
    
    
    if ([[dictData objectForKey:@"tag"] count]==0)
    {
        cell.lblTaggedUsers.hidden=YES;
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.operationView.frame.origin.y+cell.operationView.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
        
    }
    else
    {
        
        cell.lblTaggedUsers.hidden=NO;
        
        NSString *strUserName=[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"];
        
        
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.lblTaggedUsers.frame.origin.y+cell.lblTaggedUsers.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
        
        if ([[dictData objectForKey:@"tag"] count]==1) {
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ is tagged in this post",[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"] ];
            
            
            
            
            if ([[dictData objectForKey:@"tag"] count]==1)
            {
                cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ is tagged",strUserName];
                
                
                NSString *MainPostString=[NSString stringWithFormat:@"%@ is tagged",strUserName];
                
                
                NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
                
                
                NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
                
                
                NSRange rangeIsTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" is tagged"]];
                
                
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeIsTagged];
                [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeIsTagged];
                
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
                [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
                
                
                cell.lblTaggedUsers.delegate=self;
                cell.lblTaggedUsers.tag=2;
                
                
                NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                                 , nil];
                
                NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
                NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
                
                cell.lblTaggedUsers.linkAttributes = linkAttributes;
                [cell.lblTaggedUsers addLinkToAddress:dictData withRange:rangeUsername];
                
                cell.lblTaggedUsers.attributedText=string;
                
            }
            
            
            
        }
        else{
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ and %u more are tagged in this post",[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"],[[dictData objectForKey:@"tag"]count]-1];
            
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ and %u more are tagged",strUserName,[[dictData objectForKey:@"tag"]count]-1];
            
            NSString *MainPostString=[NSString stringWithFormat:@"%@ and %u more are tagged",strUserName,[[dictData objectForKey:@"tag"]count]-1];
            
            
            NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
            
            
            NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
            NSRange rangeNumberofTaggedUsers=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%u more",[[dictData objectForKey:@"tag"]count]-1]];
            
            NSRange rangeAnd=  [MainPostString rangeOfString:[NSString stringWithFormat:@" and "]];
            NSRange rangeAreTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" are tagged"]];
            
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAnd];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAnd];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAreTagged];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAreTagged];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeNumberofTaggedUsers];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeNumberofTaggedUsers];
            
            
            cell.lblTaggedUsers.delegate=self;
            cell.lblTaggedUsers.tag=2;
            
            NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                             , nil];
            
            NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            cell.lblTaggedUsers.linkAttributes = linkAttributes;
            [cell.lblTaggedUsers addLinkToPhoneNumber:nil withRange:rangeNumberofTaggedUsers];
            
            [cell.lblTaggedUsers addLinkToAddress:dictData withRange:rangeUsername];
            
            
            cell.lblTaggedUsers.attributedText=string;
        }
        
    }
    
    {
        cell.imgPet.frame = CGRectMake(cell.imgPet.frame.origin.x, cell.imgPet.frame.origin.y, cell.imgPet.frame.size.width, cell.imgPet.frame.size.width);
        cell.operationView.frame = CGRectMake(cell.operationView.frame.origin.x, cell.imgPet.frame.origin.y + cell.imgPet.frame.size.height + 3, cell.operationView.frame.size.width, cell.operationView.frame.size.height);
        
        if (!cell.lblTaggedUsers.hidden)
        {
            cell.lblTaggedUsers.frame = CGRectMake(cell.lblTaggedUsers.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblTaggedUsers.frame.size.width, cell.lblTaggedUsers.frame.size.height);
            cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.lblTaggedUsers.frame.origin.y + cell.lblTaggedUsers.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
        }
        else
        {
            cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
        }
    }
    
    
    return cell;
}


#pragma mark - Delegate Methods of TTTAttributedLabel -


- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithPhoneNumber:(NSString *)strHashTag{
    
    if (label.tag==1){
        HashTagViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewController"];
        vc.strHashTag=strHashTag;
        [self.navigationController pushViewController:vc animated:YES];
        
        //        [self performSegueWithIdentifier:@"HashTagSegue" sender:self];
    }else if(label.tag==2){
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        [self performSegueWithIdentifier:@"TaggedUserSegue" sender:indexPath];
    }
}
-(IBAction)commentToFeed:(UIStoryboardSegue *)segue{
    if ([segue.sourceViewController isKindOfClass:[CommentVC class]]) {
        CommentVC *commentVC = segue.sourceViewController;
    }
}
- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithAddress:(NSDictionary *)addressComponents
{
    
    if (label.tag==1) {
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        if ([[addressComponents objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
            [self.tabBarController setSelectedIndex:4];
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
            
        }else{
            
            [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
        }
    }
    else if(label.tag==2) {
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        
        if ([[[[self.postDataDetail objectForKey:@"tag"] objectAtIndex:0] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
            [self.tabBarController setSelectedIndex:4];
            
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
            
        }else{
            [self performSegueWithIdentifier:@"TaggedUser" sender:indexPath];
        }
    }
    
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"CommentSegue"]) {
        
        CommentVC *commentVC=(CommentVC *)[segue destinationViewController];
        commentVC.strPhotoID=[self.postDataDetail objectForKey:@"photo_id"];
        
    }
    else if ([segue.identifier isEqualToString:@"SingleProfileVC"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        userProfile.userDetails=self.postDataDetail;
    }
    else if([segue.identifier isEqualToString:@"TaggedUser"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        userProfile.userDetails=[[self.postDataDetail objectForKey:@"tag"] objectAtIndex:0];
    }
    else if([segue.identifier isEqualToString:@"TaggedUserSegue"]){
        TaggedUsersViewController *tagUsers=(TaggedUsersViewController *)[segue destinationViewController];
        tagUsers.arrayData=[_postDataDetail objectForKey:@"tag"];
    } else if ([segue.identifier isEqualToString:@"reportVC"]) {
        ReportVC *rVC = [segue destinationViewController];
        rVC.intItemId = [[self.sharingSheet.postData valueForKey:@"photo_id"] integerValue];
        rVC.intUserId = [[self.postDataDetail objectForKey:@"user_id"] intValue];
    } 
}


- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SharingActionSheet Delegate Methods
- (void)reportBtnDidPushed
{
    [self performSegueWithIdentifier:@"reportVC" sender:self];
}

@end
