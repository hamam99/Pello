//
//  TaggedUsersViewController.h
//  PETSocial
//
//  Created by milap kundalia on 8/1/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaggedUsersViewController : UIViewController
- (IBAction)btnBackAcion:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *TaggedUsersTblOutlet;
@property (strong, nonatomic)  NSMutableArray *arrayData;
@property (strong, nonatomic) NSDictionary *userDetails;

@end
