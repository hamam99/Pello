//
//  ForgotPasswordVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/18/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController<UIAlertViewDelegate>
- (IBAction)backBtnAction:(UIButton *)sender;
- (IBAction)submitBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;

@end
