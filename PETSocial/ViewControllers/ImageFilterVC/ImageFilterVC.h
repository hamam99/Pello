//
//  ImageFilterVC.h
//  PETSocial
//
//  Created by Habibi on 12/4/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

typedef enum {
    GPUIMAGE_NORMAL,
    GPUIMAGE_ACV_BREEZE,
    GPUIMAGE_ACV_RETRO,
    GPUIMAGE_ACV_CLOUDY,
    GPUIMAGE_ACV_SUNSET,
    GPUIMAGE_ACV_TUNE,
    GPUIMAGE_ACV_VANILLA,
    GPUIMAGE_ACV_HAZE,
    GPUIMAGE_ACV_IRIS,
    GPUIMAGE_ACV_POPSTAR,
    GPUIMAGE_ACV_NOSTALGIA,
    GPUIMAGE_ACV_CLEAR,
    GPUIMAGE_ACV_SATURN,
    GPUIMAGE_ACV_EXPIRED,
    GPUIMAGE_ACV_COFFEE,
    GPUIMAGE_ACV_SUNNY,
    GPUIMAGE_ACV_LEMON,
    GPUIMAGE_ACV_80FILM,
    GPUIMAGE_ACV_80MAGENTA,
    GPUIMAGE_ACV_COBALT,
    GPUIMAGE_ACV_MORNINGBEACON,
    GPUIMAGE_ACV_MORNINGCOOL,
    GPUIMAGE_ACV_NEGATIVE,
    GPUIMAGE_ACV_STRONGREDDISH,
    GPUIMAGE_ACV_SUMMERRETRO,
    GPUIMAGE_ACV_SUNLIT,
    GPUIMAGE_ACV_WASHED,
    
    GPUIMAGE_NUMFILTERS
} GPUImageShowcaseFilterType;

@protocol ImageFilterVCDelegate <NSObject>
@required
- (void)imageFilterVCDidDismissWithImage:(UIImage*)filteredImage;
@end

@interface ImageFilterVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic, assign) id<ImageFilterVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, readwrite) UIImage *selectedImage;
@property (nonatomic, readwrite) UIImage *filteredImage;
@property (weak, nonatomic) IBOutlet UICollectionView *filterCollectionView;
@property (nonatomic, readwrite) NSMutableArray *mainArray;
@end
