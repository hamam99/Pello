//
//  ImagePickerVC.h
//  PETSocial
//
//  Created by Habibi on 12/19/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "TimerArcView.h"

@protocol ImagePickerVCDelegate <NSObject>
- (void)imagePickerVCDidCaptureImage:(UIImage*)image;
- (void)imagePickerVCDidRecordVideo:(NSURL*)videoURL;
@end

@interface ImagePickerVC : UIViewController<AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureFileOutputRecordingDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic, assign) id<ImagePickerVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *photoTabButton;
@property (weak, nonatomic) IBOutlet UIButton *videoTabButton;
@property (weak, nonatomic) IBOutlet UIButton *captureButton;
@property (nonatomic, readwrite) NSString *currentMode;
@property (weak, nonatomic) IBOutlet UIButton *galleryTabButton;
@property (weak, nonatomic) IBOutlet UIButton *switchCameraButton;

@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoDataOutput *sessionOutput;
@property (strong, nonatomic) AVCaptureDeviceInput *deviceInput;
@property (strong, nonatomic) AVCaptureMovieFileOutput *movieFileOutput;

@property (nonatomic, readwrite) UIImage *selectedImage;
@property (nonatomic, readwrite) BOOL isRecording;
@property (nonatomic, readwrite) NSString *outputMoviePath;
@property (strong, nonatomic) TimerArcView *timerArcView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, readwrite) NSMutableArray *galleryImages;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *galleryLoader;
@property (nonatomic, readwrite) NSIndexPath *selectedGalleryImageIndexPath;

@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic, readwrite) NSURL *recordingFileURLPath;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end
