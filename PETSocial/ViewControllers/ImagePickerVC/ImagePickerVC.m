//
//  ImagePickerVC.m
//  PETSocial
//
//  Created by Habibi on 12/19/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import "ImagePickerVC.h"
#import "UIImage-Extensions.h"
#import "SVProgressHUD.h"
#import "Constant.h"
#import "CircularProgressTimer.h"
#import "SingleImageGalleryCollectionViewCell.h"
#import "ImageFilterVC.h"
#import "PostVC.h"

#import <AVFoundation/AVFoundation.h>

#define kViewModePhoto @"photo"
#define kViewModeVideo @"video"
#define kViewModeGallery @"gallery"
#define kDefaultBlue [UIColor colorWithRed:50/255.0 green:159/255.0 blue:214/255.0 alpha:1.0]
#define kDefaultGray [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1.0]
#define kMaxFetchedGalleryImages 1000

@interface ImagePickerVC ()
{
    NSTimer *timer;
    NSInteger globalTimer;
    NSInteger counter;
    NSInteger minutesLeft;
    NSInteger secondsLeft;
    UIRefreshControl *refreshControl;
    CircularProgressTimer *progressTimerView;
}

@end

@implementation ImagePickerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    globalTimer = kDefaultVideoDuration;
    
    self.timerArcView = [[TimerArcView alloc] initWithFrame:self.captureButton.frame];
    self.timerArcView.percent = 100;
    [self.view addSubview:self.timerArcView];
    [self.timerArcView setHidden:YES];

    [self.collectionView setHidden:YES];
    //[self fetchGalleryImages];
    [self getAllPhotos];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self changeViewModeto:kViewModeGallery];
}

-(void)getAllPhotos{
    __block NSArray  *imageArray=[[NSArray alloc] init];
    __block NSArray  *assetArray=[[NSArray alloc] init];
    
    NSMutableArray *mutableArray =[[NSMutableArray alloc]init];
    NSMutableArray *mutableAssetArray =[[NSMutableArray alloc]init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    __block NSInteger totalcount = 0;
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                // NSLog(@"assestlibrarymurari%@",assetURLDictionaries);
                
                NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url
                         resultBlock:^(ALAsset *asset) {
                             UIImage *img = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
                             //UIImage *img = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
                             if (img) {
                                 [mutableArray addObject:img];
                                 [mutableAssetArray addObject:asset];
                             }
                             
                             //NSLog(@"current count: %lu", (unsigned long)mutableArray.count);
                             if ([mutableArray count]==totalcount)
                             {
                                 imageArray=[[NSArray alloc] initWithArray:mutableArray];
                                 assetArray=[[NSArray alloc] initWithArray:mutableAssetArray];
                                 [self allPhotosCollected:imageArray andAssetArray:assetArray];
                             }
                         }
                        failureBlock:^(NSError *error){ NSLog(@"operation was not successfull!"); } ];
                
            } else {
                totalcount -= 1;
                //NSLog(@"result is not a photo, skipped, tc: %ld", (long)totalcount);
            }
        } else {
            //NSLog(@"result is nil");
        }
    };
    
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            //[group enumerateAssetsUsingBlock:assetEnumerator];
            [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:assetEnumerator];
            [assetGroups addObject:group];
            //NSLog(@"AssetGroup%@",assetGroups);
            totalcount+=[group numberOfAssets];
            //NSLog(@"Totalcount: %ld", (long)totalcount);
        }
    };
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"There is an error");}];
    
    
    
}

- (void)allPhotosCollected:(NSArray*)imagesArray andAssetArray:(NSArray*)assetArray
{
    //NSLog(@"imagesArray count: %lu", (unsigned long)imagesArray.count);
    self.galleryImages = [[NSMutableArray alloc] init];
    [imagesArray enumerateObjectsUsingBlock:^(UIImage* obj, NSUInteger idx, BOOL * stop) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:obj forKey:@"image"];
        [dic setObject:obj forKey:@"fullImage"];
        [dic setObject:[assetArray objectAtIndex:idx] forKey:@"asset"];
        [self.galleryImages addObject:dic];
    }];
    
    [self.collectionView setHidden:NO];
    
    //first image
    NSDictionary *curD = [self.galleryImages objectAtIndex:0];
    
    self.selectedImage = [Constant cropImage:[curD objectForKey:@"fullImage"]];
    self.imageView.image = self.selectedImage;
    
    [self.collectionView reloadData];
    [self.galleryLoader setHidden:YES];
}

- (void)fetchGalleryImages
{
    self.galleryImages = [[NSMutableArray alloc] init];
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    __block NSInteger gallerycounter = 0;
    
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            NSLog(@"Group name: %@",[group valueForProperty:ALAssetsGroupPropertyName]);
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            //[group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result) {
                    
                    if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                        [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];

                        NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                        
                        
                        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *asset) {
                            @autoreleasepool {
                                gallerycounter+=1;
                                if ([self.galleryImages count] < kMaxFetchedGalleryImages) {
                                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                                    //UIImage *fullImg = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
                                    UIImage *tmpimg = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
                                    
                                    //dispatch_sync(dispatch_get_main_queue(), ^{
                                    [dic setObject:tmpimg forKey:@"image"];
                                    [dic setObject:tmpimg forKey:@"fullImage"];
                                    [dic setObject:url forKey:@"url"];
                                    [self.galleryImages addObject:dic];
                                    //});
                                    
                                }
                                
                                if (gallerycounter == kMaxFetchedGalleryImages) {
                                    [self.collectionView setHidden:NO];
                                    
                                    //first image
                                    NSDictionary *curD = [self.galleryImages objectAtIndex:0];
                                    
                                    self.selectedImage = [Constant cropImage:[curD objectForKey:@"fullImage"]];
                                    //self.selectedImage = [Constant cropImage:img];
                                    self.imageView.image = self.selectedImage;
                                    
                                    [self.collectionView reloadData];
                                    [self.galleryLoader setHidden:YES];
                                }
                            }
                        };
                        
                        [assetLibrary assetForURL:url resultBlock:resultblock failureBlock:^(NSError *error) {
                            NSLog(@"test:Fail");
                        }];
                    }

                    
                }
            }];
            [self.collectionView reloadData];
        } else {
            
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"error enumerating AssetLibrary groups %@\n", error);
    }];
}

- (void)changeViewModeto:(NSString*)viewMode
{
    self.currentMode = viewMode;
    if ([self.currentMode isEqualToString:kViewModePhoto]) {
        [self stopSession];
        [self setUpCaptureSession2];
        [self.photoTabButton setBackgroundColor:kDefaultBlue];
        [self.videoTabButton setBackgroundColor:kDefaultGray];
        [self.galleryTabButton setBackgroundColor:kDefaultGray];
        [self.captureButton setHidden:NO];
        [self.captureButton setImage:[UIImage imageNamed:@"photo-capture-icon.png"] forState:UIControlStateNormal];
        [self.collectionView setHidden:YES];
        self.isRecording = NO;
        [self.switchCameraButton setHidden:NO];
        [self.nextBtn setHidden:NO];
    } else if ([self.currentMode isEqualToString:kViewModeVideo]) {
        [self stopSession];
        [self setUpCaptureSession];
        [self.photoTabButton setBackgroundColor:kDefaultGray];
        [self.videoTabButton setBackgroundColor:kDefaultBlue];
        [self.galleryTabButton setBackgroundColor:kDefaultGray];
        [self.captureButton setHidden:NO];
        [self.captureButton setImage:[UIImage imageNamed:@"video-capture-icon.png"] forState:UIControlStateNormal];
        [self.collectionView setHidden:YES];
        [self.switchCameraButton setHidden:NO];
        [self.nextBtn setHidden:YES];
    } else if ([self.currentMode isEqualToString:kViewModeGallery]) {
        [self stopSession];
        [self.timerArcView setHidden:YES];
        self.imageView.image = self.selectedImage;
        [self.photoTabButton setBackgroundColor:kDefaultGray];
        [self.videoTabButton setBackgroundColor:kDefaultGray];
        [self.galleryTabButton setBackgroundColor:kDefaultBlue];
        [self.captureButton setHidden:YES];
        [self.collectionView setHidden:NO];
        [self.switchCameraButton setHidden:YES];
        [self.nextBtn setHidden:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)galleryTabButtonDidPushed:(id)sender {
    [self changeViewModeto:kViewModeGallery];
}

- (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (IBAction)photoTabButtonDidPushed:(id)sender {
    
    BOOL isCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (isCamera==NO) {
        [self showAlertWithTitle:ALERTTITLE andMessage:@"Camera unavailable"];
        return;
    }
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        [self changeViewModeto:kViewModePhoto];
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        [self showAlertWithTitle:ALERTTITLE andMessage:@"Photo access denied"];
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        [self showAlertWithTitle:ALERTTITLE andMessage:@"Photo access restricted"];
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                [self changeViewModeto:kViewModePhoto];
            } else { // Access denied ..do something
                [self showAlertWithTitle:ALERTTITLE andMessage:@"Photo access denied"];
            }
        }];
    }
}

- (IBAction)videoTabButtonDidPushed:(id)sender {
    
    BOOL isCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (isCamera==NO) {
        [self showAlertWithTitle:ALERTTITLE andMessage:@"Camera unavailable"];
        return;
    }
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        [self changeViewModeto:kViewModeVideo];
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        [self showAlertWithTitle:ALERTTITLE andMessage:@"Video access denied"];
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        [self showAlertWithTitle:ALERTTITLE andMessage:@"Video access restricted"];
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                [self changeViewModeto:kViewModeVideo];
            } else { // Access denied ..do something
                [self showAlertWithTitle:ALERTTITLE andMessage:@"Video access denied"];
            }
        }];
    }
}

- (IBAction)btnBackAcion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)captureButtonHoldedDown:(id)sender {
    if ([self.currentMode isEqualToString:kViewModeVideo]) {
        //NSLog(@"Touch down inside");
        
        if (self.isRecording==NO) {
            self.isRecording=YES;
            
            [self.timerArcView setHidden:NO];
            
            //[self startArcTimer];
            //Create temporary URL to record to
            NSString *outputPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"Pello-Recording.mov"];
            NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:outputPath])
            {
                NSError *error;
                if ([fileManager removeItemAtPath:outputPath error:&error] == NO) {
                    //Error - handle if requried
                }
            }
            
            //Start recording
            [self.movieFileOutput startRecordingToOutputFileURL:outputURL recordingDelegate:self];
            [SVProgressHUD showSuccessWithStatus:@"You are now recording..."];
            
            [self performSelector:@selector(stopRecording) withObject:nil afterDelay:kDefaultVideoDuration];
        }
    }
}


- (IBAction)captureButtonDidPushed:(id)sender {
    if ([self.currentMode isEqualToString:kViewModePhoto]) {
        self.selectedImage = [Constant cropImage:self.imageView.image];
        [self performSegueWithIdentifier:@"FilterSegue" sender:self];
    } else if ([self.currentMode isEqualToString:kViewModeVideo]){
        [self stopArcTimer];
        [self stopRecording];
        [self performSegueWithIdentifier:@"CaptionSegue" sender:self];
        //NSLog(@"Touch up inside");
    }
}

- (void)stopRecording
{
    self.isRecording=NO;
    
    [self.timerArcView setHidden:NO];
    
    [self.movieFileOutput stopRecording];
}

- (IBAction)switchCameraTapped:(id)sender
{
    //Change camera source
    if(self.captureSession)
    {
        //Indicate that some changes will be made to the session
        [self.captureSession beginConfiguration];
        
        //Remove existing input
        AVCaptureInput* currentCameraInput = [_captureSession.inputs objectAtIndex:0];
        [self.captureSession removeInput:currentCameraInput];
        
        //Get new input
        AVCaptureDevice *newCamera = nil;
        if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack)
        {
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
        }
        else
        {
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
        }
        
        //Add input to session
        NSError *err = nil;
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&err];
        if(!newVideoInput || err)
        {
            NSLog(@"Error creating capture device input: %@", err.localizedDescription);
        }
        else
        {
            if ([self.captureSession canAddInput:newVideoInput]) {
                [self.captureSession addInput:newVideoInput];
            } else {
                
            }
            
        }
        
        //Commit all the configuration changes at once
        [self.captureSession commitConfiguration];
    }
}

// Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position) return device;
    }
    return nil;
}

- (void)setUpCaptureSession
{
    // Refer to: ttp://www.ios-developer.net/iphone-ipad-programmer/development/camera/record-video-with-avcapturesession-2
    self.captureSession = [[AVCaptureSession alloc] init];
    
    //ADD VIDEO INPUT
    AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (VideoDevice)
    {
        NSError *error;
        self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:VideoDevice error:&error];
        if (!error)
        {
            if ([self.captureSession canAddInput:self.deviceInput]) {
                [self.captureSession addInput:self.deviceInput];
            } else {
                NSLog(@"Couldn't add video input");
            }
        } else {
            NSLog(@"Couldn't create video input");
        }
    } else {
        NSLog(@"Couldn't create video capture device");
    }
    
    //ADD AUDIO INPUT
    NSLog(@"Adding audio input");
    AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    NSError *error = nil;
    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
    if (audioInput) {
        [self.captureSession addInput:audioInput];
    }
    
    //ADD MOVIE FILE OUTPUT
    NSLog(@"Adding movie file output");
    self.movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    
    Float64 TotalSeconds = 60;			//Total seconds
    int32_t preferredTimeScale = 30;	//Frames per second
    CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale); //<<SET MAX DURATION
    self.movieFileOutput.maxRecordedDuration = maxDuration;
    
    self.movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024; //<<SET MIN FREE SPACE IN BYTES FOR RECORDING TO CONTINUE ON A VOLUME
    
    
    
    AVCaptureConnection *conn = [self.sessionOutput connectionWithMediaType:AVMediaTypeVideo];
    [conn setVideoOrientation:AVCaptureVideoOrientationPortrait];
    // Configure your output.
    dispatch_queue_t queue = dispatch_queue_create("myQueue", NULL);
    [self.sessionOutput setSampleBufferDelegate:self queue:queue];
    // Specify the pixel format
    self.sessionOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    
    //SET THE CONNECTION PROPERTIES (output properties)
    //[self CameraSetOutputProperties];
    
    //----- SET THE IMAGE QUALITY / RESOLUTION -----
    //Options:
    //	AVCaptureSessionPresetHigh - Highest recording quality (varies per device)
    //	AVCaptureSessionPresetMedium - Suitable for WiFi sharing (actual values may change)
    //	AVCaptureSessionPresetLow - Suitable for 3G sharing (actual values may change)
    //  AVCaptureSessionPreset352x288
    //	AVCaptureSessionPreset640x480 - 640x480 VGA (check its supported before setting it)
    //	AVCaptureSessionPreset1280x720 - 1280x720 720p HD (check its supported before setting it)
    //	AVCaptureSessionPresetPhoto - Full photo resolution (not supported for video output)
    NSLog(@"Setting image quality");
    [self.captureSession setSessionPreset:AVCaptureSessionPresetMedium];
    if ([self.captureSession canSetSessionPreset:AVCaptureSessionPreset352x288]) //Check size based configs are supported before setting them
        [self.captureSession setSessionPreset:AVCaptureSessionPreset352x288];
    
    if ([self.captureSession canAddOutput:self.movieFileOutput])
        [self.captureSession addOutput:self.movieFileOutput];
    
    
    //ADD VIDEO PREVIEW LAYER
    self.captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    CGRect bounds=self.imageView.layer.bounds;
    self.captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.captureVideoPreviewLayer.bounds=bounds;
    self.captureVideoPreviewLayer.position=CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    self.captureVideoPreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    [self.imageView.layer addSublayer:self.captureVideoPreviewLayer];
    
    [self.captureSession startRunning];
}

- (void)setUpCaptureSession2
{
    self.captureSession = [[AVCaptureSession alloc] init];
    self.captureSession.sessionPreset = AVCaptureSessionPresetMedium;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (!self.deviceInput) {
        NSLog(@"ERROR: trying to open camera: %@", error);
    }
    [self.captureSession addInput:self.deviceInput];

    
    // Create a VideoDataOutput and add it to the session
    self.sessionOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.captureSession addOutput:self.sessionOutput];
    
    AVCaptureDevice *audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    AVCaptureDeviceInput * audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:nil];
    [self.captureSession addInput:audioInput];

    // set orientation
    AVCaptureConnection *conn = [self.sessionOutput connectionWithMediaType:AVMediaTypeVideo];
    [conn setVideoOrientation:AVCaptureVideoOrientationPortrait];
    // Configure your output.
    dispatch_queue_t queue = dispatch_queue_create("myQueue", NULL);
    [self.sessionOutput setSampleBufferDelegate:self queue:queue];
    // Specify the pixel format
    self.sessionOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    
    self.captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    CGRect bounds=self.imageView.layer.bounds;
    self.captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.captureVideoPreviewLayer.bounds=bounds;
    self.captureVideoPreviewLayer.position=CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    self.captureVideoPreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    [self.imageView.layer addSublayer:self.captureVideoPreviewLayer];
    
    [self.captureSession startRunning];
}

- (void)stopSession
{
    [self.captureVideoPreviewLayer removeFromSuperlayer];
    [self.captureSession stopRunning];
    self.captureVideoPreviewLayer = nil;
    self.captureSession = nil;
}

- (IBAction)nextButtonDidPushed:(id)sender {
    if ([self.currentMode isEqualToString:kViewModeGallery] || [self.currentMode isEqualToString:kViewModePhoto]) {
        [self performSegueWithIdentifier:@"FilterSegue" sender:self];
    } else {
        if (self.isRecording==NO && self.recordingFileURLPath!=nil) {
            [self performSegueWithIdentifier:@"CaptionSegue" sender:self];
        }
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"FilterSegue"]) {
        ImageFilterVC *ifVC = [segue destinationViewController];
        ifVC.selectedImage = self.selectedImage;
    } else if([segue.identifier isEqualToString:@"CaptionSegue"]) {
        PostVC *postVC = [segue destinationViewController];
        postVC.recordingFileURLPath = self.recordingFileURLPath;
    }
}


#pragma mark - AVCaptureSession Delegate Methods
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    UIImage *image = [self imageFromSampleBuffer:sampleBuffer];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageView setImage:image];
    });
}

// Create a UIImage from sample buffer data
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}

#pragma mark - Recording Delegate Methods
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    BOOL successRecording = YES;
    [SVProgressHUD showWithStatus:@"Saving video..."];
    if (!error) {
        id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
        if (value) {
            successRecording = [value boolValue];
        }
        if (successRecording) {
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputFileURL]) {
                [library writeVideoAtPathToSavedPhotosAlbum:outputFileURL
                                            completionBlock:^(NSURL *assetURL, NSError *error) {
                     if (!error) {
                         [SVProgressHUD dismiss];
                         self.recordingFileURLPath =outputFileURL;
                     } else {
                         [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Save Record Error"];
                     }
                 }];
            }
        }
    } else {
        NSLog(@"Recording Error: %@", error);
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Recording Error"];
    }
}

#pragma mark - Circular Progress
// Draws the progress circles on top of the already painted backgroud
- (void)drawCircularProgressBarWithMinutesLeft:(NSInteger)minutes secondsLeft:(NSInteger)seconds
{
    // Init our view and set current circular progress bar value
    CGRect progressBarFrame = self.captureButton.frame;
    progressTimerView = [[CircularProgressTimer alloc] initWithFrame:progressBarFrame];
    //[progressTimerView setCenter:self.captureButton.center];
    [progressTimerView setPercent:seconds];
    if (minutes == 0 && seconds == 0) {
        [progressTimerView setInstanceColor:[UIColor redColor]];
    }
    
    // Here, setting the minutes left before adding it to the parent view
    [progressTimerView setMinutesLeft:minutesLeft];
    [progressTimerView setSecondsLeft:secondsLeft];
    [self.view addSubview:progressTimerView];
    progressTimerView = nil;
}

- (void)startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(updateCircularProgressBar)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)updateCircularProgressBar
{
    // Values to be passed on to Circular Progress Bar
    if (globalTimer > 0 && globalTimer <= 1200) {
        globalTimer--;
        minutesLeft = globalTimer / 60;
        secondsLeft = globalTimer % 60;
        
        [self drawCircularProgressBarWithMinutesLeft:minutesLeft secondsLeft:secondsLeft];
        NSLog(@"Time left: %02ld:%02ld", (long)minutesLeft, (long)secondsLeft);
    } else {
        [self drawCircularProgressBarWithMinutesLeft:0 secondsLeft:0];
        [timer invalidate];
    }
}

#pragma mark - ArcTimerView
- (void)startArcTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(decrementSpin)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)stopArcTimer
{
    [timer invalidate];
    timer = nil;
}

- (void)decrementSpin
{
    // If we can decrement our percentage, do so, and redraw the view
    if (self.timerArcView.percent > 0) {
        self.timerArcView.percent = self.timerArcView.percent - (100 / (kDefaultVideoDuration-1));
        [self.timerArcView setNeedsDisplay];
    } else {
        [timer invalidate];
        timer = nil;
    }
}

#pragma mark - UICollectionView Delegate & Data Source Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.galleryImages count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SingleImageGalleryCollectionViewCell *cell = (SingleImageGalleryCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"singleImageGalleryCell" forIndexPath:indexPath];
    //cell.petImageView.image = [UIImage imageNamed:@"image_placeholder.png"];

    NSDictionary *curD = [self.galleryImages objectAtIndex:indexPath.row];
    cell.petImageView.image = [curD objectForKey:@"image"];
    [cell.selectedLayerView setHidden:YES];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    SingleImageGalleryCollectionViewCell *oldcell = (SingleImageGalleryCollectionViewCell*)[collectionView cellForItemAtIndexPath:self.selectedGalleryImageIndexPath];
    [oldcell.selectedLayerView setHidden:YES];
    
    SingleImageGalleryCollectionViewCell *cell = (SingleImageGalleryCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    [cell.selectedLayerView setHidden:NO];
    
    self.selectedGalleryImageIndexPath = indexPath;
    NSDictionary *curD = [self.galleryImages objectAtIndex:self.selectedGalleryImageIndexPath.row];
    
    /*ALAsset *asset = [curD objectForKey:@"asset"];
    NSLog(@"asset: %@", asset);
    UIImage *img = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
    self.selectedImage = [Constant cropImage:img];*/
    
    self.selectedImage = [Constant cropImage:[curD objectForKey:@"fullImage"]];
    self.imageView.image = self.selectedImage;
    
    
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(70, 70);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

@end
