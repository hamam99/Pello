//
//  LoginVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/13/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController<BSKeyboardControlsDelegate>
- (IBAction)loginActionBtn:(UIButton *)sender;
- (IBAction)backBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewOutlet;

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@end
