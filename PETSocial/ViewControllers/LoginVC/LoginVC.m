//
//  LoginVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/13/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()
@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapForKeyboard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignAllKeyboards:)];
    [self.scrollViewOutlet addGestureRecognizer:tapForKeyboard];
    
    NSArray *arr = @[self.txtUserName, self.txtPassword];
    [self setKeyboardControls:[[BSKeyboardControls alloc]initWithFields:arr]];
    [self.keyboardControls setDelegate:self];
    self.txtUserName.autocorrectionType=UITextAutocorrectionTypeNo;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resignAllKeyboards:(id)sender {
    [self.txtUserName resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
    [UIView commitAnimations];
}
-(BOOL)validation{
    // this method is for Adding validation
    if([self.txtUserName.text length] ==0 && [self.txtPassword.text length] ==0)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please enter username and password"];
        return NO;
    }
    if([self.txtUserName.text length] ==0)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTUSERNAMEEMAILBLANK];
        return NO;
    }
    
    if([self.txtPassword.text length]==0)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTPASSWORDBLANK];
        return NO;
    }
    else{
        return YES;
    }
}
- (IBAction)loginActionBtn:(UIButton *)sender {
    
    if ([self validation]) {
        NSString *str=[NSString stringWithFormat:@"%@",[USERDEFAULTS objectForKey:@"Devicetoken"] ];
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:self.txtUserName.text,@"login",self.txtPassword.text,@"password",str,@"cf_device_token", nil];
        [[Connection sharedConnectionWithDelegate:self]loginWithData:dict];
    }
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark Connection Delegate methods -

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATELOGIN]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [USERDEFAULTS removeObjectForKey:KEYUSERINFO];
                    [USERDEFAULTS setObject:[dataDict objectForKey:@"data"] forKey:KEYUSERINFO];
                    [USERDEFAULTS setObject:KEYLOGEDINYES forKey:KEYISLOGEDIN];
                    [APPDELEGATE pushTabBar:self.navigationController];
                }
                else{
                    NSLog(@"%@", [dataDict valueForKey:@"message"]);
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
    
    
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    NSLog(@"%@", nData);
    [SVProgressHUD dismiss];
}


#pragma mark UITextField Delegate method
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControl{
    [keyboardControl.activeField resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0 );
    [UIView commitAnimations];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textfield1
{
    if (iPhone5) {
        [UIView beginAnimations:Nil context:nil];
        [UIView setAnimationDuration:0.3f];
        self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 45);
        [UIView commitAnimations];
    }else if (iPhone4){
        [UIView beginAnimations:Nil context:nil];
        [UIView setAnimationDuration:0.3f];
        self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 120);
        [UIView commitAnimations];
    }
    [self.keyboardControls setActiveField:textfield1];
}

@end
