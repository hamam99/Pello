//
//  NotificaitonVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "NotificaitonVC.h"
#import "NotificationCell.h"
#import "SinglePostVC.h"
#import "UserProfileVC.h"

@interface NotificaitonVC ()
{
    NSMutableArray *notificationData;
    BOOL isMoreData;
    UIView *viewFooter;
    UIActivityIndicatorView *pageLoader;
    BOOL isValidateData;
    UITableViewController *tableViewController;
}
@end

@implementation NotificaitonVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle   *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    
    notificationData=[[NSMutableArray alloc]init];
     
    [self setUpFooterView];
    
    tableViewController= [[UITableViewController alloc] init];
    tableViewController.tableView = self.tblNotificationList;
    
    if (!tableViewController.refreshControl) {
        
        
        UIRefreshControl *refreshControl = [UIRefreshControl.alloc init];
        [refreshControl setTintColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
        
        NSDictionary *refreshAttributes = @{
                                            NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],
                                            };
        NSString *s = @"Refreshing...";
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:s];
        
        [attributeString setAttributes:refreshAttributes range:NSMakeRange(0, attributeString.length)];
        refreshControl.attributedTitle=attributeString;
        [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
        
        tableViewController.refreshControl=refreshControl;
        
    }
    
    
     [APPDELEGATE ShowHUDWith:@"Loading..."];
    [self callfeedListData:1];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    if (messageLabel==nil) {
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.hidden=YES;
    }
    if ([APPDELEGATE isNotificationCount]==YES) {
        [[APPDELEGATE viewNewNotification] setHidden:YES];
        [[APPDELEGATE lblNotificationCount] setHidden:YES];
     }
    
}
-(void)callfeedListData:(int )pageId{
     if ([[[Connection sharedConnectionWithDelegate:self] getCurrentRequest] isEqualToString:STATENOTIFICATON]) {
        return;
    }
     {
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                            [[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",
                            [NSString stringWithFormat:@"%d",pageId],@"page",
                            nil];
        [[Connection sharedConnectionWithDelegate:self]getNotificationList:dict];
     }
}

-(void)setUpFooterView{
    viewFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50)];
    pageLoader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [pageLoader setColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    pageLoader.center = viewFooter.center;
    pageLoader.hidesWhenStopped = YES;
    [viewFooter addSubview:pageLoader];
}
-(void)pullToRefresh{
    isValidateData=NO;
    [self callfeedListData:1];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
        [tableViewController.refreshControl endRefreshing];
    });
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)userPrfoileNavigation:(UITapGestureRecognizer *)sender
{
    
    NSDictionary *dict=[notificationData objectAtIndex:sender.view.tag];
    if ([[dict objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]])
    {
        [self.tabBarController setSelectedIndex:4];
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:sender];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    // Return the number of sections.
    if ([notificationData count]!=0) {
        messageLabel.hidden=YES;
        return 1;
    }
    else
    {
        // Display a message when the table is empty
        messageLabel.hidden=NO;
        messageLabel.text = @"No notification yet";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        _tblNotificationList.backgroundView = messageLabel;
        _tblNotificationList.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notificationData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationCell *cell = (NotificationCell *) [tableView dequeueReusableCellWithIdentifier:@"NotificationCell" forIndexPath:indexPath];
    NSDictionary *dict=[notificationData objectAtIndex:indexPath.row];
    
    
    cell.lblUSername.text=[dict objectForKey:@"user_name"];
    cell.lblTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[dict objectForKey:@"date"]]];
    
    
    cell.imgUserOutlet.layer.borderWidth = 1.0f;
    cell.imgUserOutlet.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    cell.imgUserOutlet.layer.masksToBounds = NO;
    cell.imgUserOutlet.clipsToBounds = YES;
    cell.imgUserOutlet.layer.cornerRadius = cell.imgUserOutlet.frame.size.width/2;
    
    cell.lblUSername.tag=indexPath.row;
    cell.lblUSername.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
    [cell.lblUSername addGestureRecognizer:tapGest];
    tapGest.view.tag=indexPath.row;
    if ([[dict objectForKey:@"post_image"] length]>0) {
        
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"post_image"]];
        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [cell.imgPetOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_200"] success:nil failure:nil];
        
    }
    if ([[dict objectForKey:@"user_image"] length]>0){
        
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_image"]];
        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [cell.imgUserOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    }
    
    if ([[dict objectForKey:@"type_id"] isEqualToString:@"photo_like"]) {
        cell.imgLikeComment.image=[UIImage imageNamed:@"icn_like"];
        [cell.lblInfoOutlet setFrame:CGRectMake(cell.lblInfoOutlet.frame.origin.x, 45, cell.lblInfoOutlet.frame.size.width, cell.lblInfoOutlet.frame.size.height)];
        cell.lblInfoOutlet.text=@"Loved Your Photo";
        [cell.lblInfoOutlet setNumberOfLines:1];
     }
    else if ([[dict objectForKey:@"type_id"] isEqualToString:@"comment_photo"])
    {
        CGFloat height=[[dict objectForKey:@"comment_text"] getHeightOfTextForFontSize:10.0 withLabelWidth:195.0];
        
        cell.imgLikeComment.image=[UIImage imageNamed:@"icn_comment_green"];
        
        
        NSData *strval = [Base64 decode:[dict objectForKey:@"comment_text"]];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        cell.lblInfoOutlet.text=strTextMsg;
        [cell.lblInfoOutlet setFrame:CGRectMake(cell.lblInfoOutlet.frame.origin.x, cell.lblInfoOutlet.frame.origin.y, cell.lblInfoOutlet.frame.size.width, height)];
        [cell.lblInfoOutlet setNumberOfLines:4];
        
     }
    else if ([[dict objectForKey:@"type_id"] isEqualToString:@"tag_user"])
    {
        cell.imgLikeComment.image=[UIImage imageNamed:@"icn_tag"];
        [cell.lblInfoOutlet setFrame:CGRectMake(cell.lblInfoOutlet.frame.origin.x, 45, cell.lblInfoOutlet.frame.size.width, cell.lblInfoOutlet.frame.size.height)];
        cell.lblInfoOutlet.text=@"You are Tagged";
        [cell.lblInfoOutlet setNumberOfLines:1];
    }
    else if ([[dict objectForKey:@"type_id"] isEqualToString:@"follow_user"])
    {
        cell.imgLikeComment.image=[UIImage imageNamed:@"icn_follow"];
        [cell.lblInfoOutlet setFrame:CGRectMake(cell.lblInfoOutlet.frame.origin.x, 45, cell.lblInfoOutlet.frame.size.width, cell.lblInfoOutlet.frame.size.height)];
        cell.lblInfoOutlet.text=@"You are Followed";
        [cell.lblInfoOutlet setNumberOfLines:1];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dict=[notificationData objectAtIndex:indexPath.row];
    
    if ([[dict objectForKey:@"type_id"] isEqualToString:@"follow_user"]) {
        
    }
    else{
        [self performSegueWithIdentifier:@"SinglePostVC" sender:indexPath];
    }
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath=(NSIndexPath *)sender;
    if ([segue.identifier isEqualToString:@"SinglePostVC"]) {
        SinglePostVC *singlePost=[segue destinationViewController];
        singlePost.postDataDetail=[notificationData objectAtIndex:indexPath.item];
    }
    else if([segue.identifier isEqualToString:@"SingleProfileVC"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        UITapGestureRecognizer *temp=(UITapGestureRecognizer *)sender;
        UIView *view= temp.view;
        userProfile.userDetails=[notificationData objectAtIndex:view.tag];
    }
    
}
-(void)callSeenNotifications{
    
    NSMutableArray *notificationIdArray=[[NSMutableArray alloc]init];
    for (id dict in notificationData) {
        
        if ([[dict objectForKey:@"is_seen"] boolValue]==NO) {
            [notificationIdArray addObject:[dict objectForKey:@"notification_id"]];
        }
    }
    NSString *notificationIds=[notificationIdArray componentsJoinedByString:@","];
    if (notificationIds.length>0) {
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",notificationIds,@"notification_id", nil];
        [[Connection sharedConnectionWithDelegate:self] seenNotificationCount:dict];
    }
    
}
#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATENOTIFICATON])
            {
                [SVProgressHUD dismiss];
                [tableViewController.refreshControl endRefreshing];
                
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    if (!isValidateData) {
                        
                        notificationData=[[NSMutableArray alloc]init];
                        notificationData=[dataDict objectForKey:@"data"];
                        [tableViewController.refreshControl endRefreshing];
                    }
                    else{
                        
                        NSArray *arrNotification = [dataDict objectForKey:@"data"];
                        
                        if (arrNotification)
                            notificationData=[[notificationData arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                        
                        
                    }
                    
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    [self.tblNotificationList reloadData];
                    [self callSeenNotifications];
                }
                else{
                    [tableViewController.refreshControl endRefreshing];
                    [self.tblNotificationList reloadData];
                    [notificationData removeAllObjects];
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"No records found"];
                    
                 }
                self.tblNotificationList.tableFooterView=nil;
                [pageLoader stopAnimating];
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [tableViewController.refreshControl endRefreshing];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [tableViewController.refreshControl endRefreshing];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
        [tableViewController.refreshControl endRefreshing];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }
#pragma mark - Pagination -
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            [self callfeedListData:self.newPage];
            self.tblNotificationList.tableFooterView=viewFooter;
            [pageLoader startAnimating];
            
        }
     }
    
}

@end
