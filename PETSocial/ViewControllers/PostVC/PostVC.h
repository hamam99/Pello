//
//  PostVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Social/Social.h>
#import "ImageFilterVC.h"
#import "ImagePickerVC.h"
#import <AVKit/AVKit.h>

@interface PostVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,CLLocationManagerDelegate, ImageFilterVCDelegate, ImagePickerVCDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgPetPic;
@property (nonatomic, readwrite) UIImage *selectedImage;
@property (nonatomic, readwrite) NSURL *recordingFileURLPath;

- (IBAction)facebookBtnAction:(UIButton *)sender;
- (IBAction)twitterBtnAction:(UIButton *)sender;
- (IBAction)postBtnAction:(UIButton *)sender;
- (IBAction)tagBtnAction:(UIButton *)sender;
- (IBAction)locationBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextView *txtStatusOutlet;
@property (weak, nonatomic) IBOutlet UILabel *statusPlaceholder;
@property (nonatomic, strong)  NSString *strlatitude;
@property (nonatomic, strong) NSString *strlogitude;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIButton *btnLocationOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnTagOutlet;
@property (nonatomic, strong) ACAccountStore *accountStore;

@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnInstagram;
@property (weak, nonatomic) IBOutlet UIButton *btnPost;

@end