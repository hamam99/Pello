//
//  PostVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "PostVC.h"
#import "TagFollowingVC.h"


@interface PostVC ()
{
    NSString *imageNameString;
    BOOL isImageSelected;
    BOOL isActionSheetOpen;
    BOOL isLocationFound;
    NSString *savedImagePath;
    NSMutableArray *arrFollowingUsers;
    NSString *strTaggedUserId;
     CLGeocoder *geocoder;
    CLPlacemark *placemark;
    UILabel *titleLabel;
}
@end

@implementation PostVC
@synthesize btnTagOutlet;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageSelectAction:)];
    self.imgPetPic.userInteractionEnabled=YES;
    [self.imgPetPic addGestureRecognizer:tapGesture];
    
    if (self.recordingFileURLPath!=nil) {
        self.selectedImage = [UIImage imageNamed:@"image-video-icon.png"];
    }
    
    if (self.selectedImage) {
        [self processSelectedImage:self.selectedImage];
        self.imgPetPic.image = self.selectedImage;
    }
    
    self.locationManager=[[CLLocationManager alloc]init];
     geocoder = [[CLGeocoder alloc] init];
    self.locationManager.delegate=self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
   
    _accountStore = [[ACAccountStore alloc] init];
    isLocationFound=NO;
     titleLabel=    [[UILabel alloc] initWithFrame:CGRectMake(30, 0, self.btnLocationOutlet.frame.size.width-30, self.btnLocationOutlet.frame.size.height)];
    
    [self candyEyed];
}

- (void)candyEyed
{
    CGFloat ele = 214.0f/255.0f;
    UIColor *gray = [UIColor colorWithRed:ele green:ele blue:ele alpha:1.0f];
    NSArray *btnArray = @[self.btnTagOutlet, self.btnLocationOutlet, self.btnFacebook, self.btnTwitter, self.btnInstagram];
    [btnArray enumerateObjectsUsingBlock:^(UIButton* obj, NSUInteger idx, BOOL * stop) {
        [obj.layer setBorderColor:gray.CGColor];
        [obj.layer setBorderWidth:1.0f];
        [obj.layer setCornerRadius:2.0f];
    }];
    
    [self.btnPost.layer setCornerRadius:2.0f];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (!isActionSheetOpen) {
        isImageSelected=NO;
        self.txtStatusOutlet.text=@"";
        [self.statusPlaceholder setHidden:NO];
        //[self.imgPetPic setImage:[UIImage imageNamed:@"image_placeholder"]];
        [self.btnLocationOutlet setTitle:@"Location" forState:UIControlStateNormal];
        for (id obje in [self.btnLocationOutlet subviews]) {
            if ([obje isKindOfClass:[UILabel class]]) {
                UILabel *objeRef=obje;
                if (objeRef.tag==123456789) {
                    [obje removeFromSuperview];
                }
            }
        }
        isActionSheetOpen=NO;
        isLocationFound=NO;
    }
    isImageSelected=YES;
    
}

-(void)imageSelectAction:(UITapGestureRecognizer *)gesture{
    
    UIActionSheet *actionSheet = nil;
    if (self.recordingFileURLPath) {
        [self.txtStatusOutlet resignFirstResponder];
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Action"
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Play Recorded Video", nil];
                                         //otherButtonTitles:@"Camera", @"Select from Library",@"Play Recorded Video", nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        
        [actionSheet showInView:self.view];
    } /*else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select  Picture"
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Camera", @"Select from Library", nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    }*/
    
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (BOOL)stringContainsEmoji:(NSString *)string {
    __block BOOL returnValue = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
             
         } else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    return returnValue;
}

- (IBAction)btnBackAction:(id)sender {
    self.recordingFileURLPath = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Custom methods -
-(void)callPostData:(id)sender{
    
    [self.view endEditing:YES];
    
    NSString *locationStr;
    NSDictionary *dict;
    NSString *tag_status = @"";
    NSMutableArray *final=[[NSMutableArray alloc] init];
    if ([_txtStatusOutlet.text rangeOfString:@"#"].location == NSNotFound) {
        
    } else {
        NSRange hashrange=[_txtStatusOutlet.text rangeOfString:@"#"];
        NSArray *arr = [[_txtStatusOutlet.text substringFromIndex:hashrange.location] componentsSeparatedByString:@"#"];
        NSMutableArray *finalHashtag=[[NSMutableArray alloc]init];
        
        for (NSString *str in arr)
        {
            NSArray *strArray=[str componentsSeparatedByString:@" "];
            if (strArray.count!=0) {
                NSString *stringValue=[strArray firstObject];
                if (![stringValue isEqualToString:@" "]) {
                    [finalHashtag addObject:[strArray firstObject]];
                }
            }
            else{
                [finalHashtag addObjectsFromArray:strArray];
            }
        }
        
        for (NSString *str in finalHashtag) {
            NSString *test = str;
            NSMutableString *asciiCharacters = [NSMutableString string];
            for (NSInteger i = 32; i < 127; i++)  {
                [asciiCharacters appendFormat:@"%ld", (long)i];
            }
            NSCharacterSet *nonAsciiCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:asciiCharacters] invertedSet];
            test = [[test componentsSeparatedByCharactersInSet:nonAsciiCharacterSet] componentsJoinedByString:@""];
             if ([test length]>1) {
                [final addObject:test];
            }
        }
    }
    tag_status = [final componentsJoinedByString:@","];
    //to remove all the emoji from this array finalHashtag
    
    BOOL isemoji =[self stringContainsEmoji:_txtStatusOutlet.text];
    NSData *dataStatus=[_txtStatusOutlet.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *strStatus = [Base64 encode:dataStatus];
    // NSCharacterSet
    if (strTaggedUserId.length==0) {
        strTaggedUserId=@"";
    }
    if (isLocationFound) {
        for (id obj in self.btnLocationOutlet.subviews) {
            if ([obj isKindOfClass:[UILabel class]]) {
                UILabel *lbl=(UILabel *)obj;
                locationStr=lbl.text;
            }
        }
        
        dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strStatus,@"status",strTaggedUserId,@"tag",locationStr,@"location",[USERDEFAULTS objectForKey:@"LastLatitude"],@"latitude",[USERDEFAULTS objectForKey:@"LastLongitude"],@"longitude",imageNameString,@"post_image",tag_status,@"tag_status", nil];
    } else {
        
        dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strStatus,@"status",strTaggedUserId,@"tag",@"",@"location",@"",@"latitude",@"",@"longitude",imageNameString,@"post_image",tag_status,@"tag_status", nil];
    }
    
    if (self.recordingFileURLPath) {
        dict = [NSDictionary dictionaryWithObjectsAndKeys:
                [[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",
                strStatus,@"status",
                strTaggedUserId,@"tag",
                @"",@"location",
                @"",@"latitude",
                @"",@"longitude",
                [self.recordingFileURLPath absoluteString],@"post_video",
                tag_status,@"tag_status",
                nil];
        
        [[Connection sharedConnectionWithDelegate:self] postStatusWithVideo:dict];
    } else {
        [[Connection sharedConnectionWithDelegate:self] postStatusWithImage:dict];
    }
}
-(void)isValidate{
    if (self.txtStatusOutlet.text.length>0) {
        
    }
}

#pragma mark - Button action methods -

- (IBAction)facebookBtnAction:(UIButton *)sender {
    [self.txtStatusOutlet resignFirstResponder];
    
    if (self.txtStatusOutlet.text.length>0 && ![self.txtStatusOutlet.text isEqualToString:@""]) {
        if (isImageSelected) {
            //Prashant
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [APPDELEGATE ShowHUDWith:@"Loading..."];
            }];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self ShareNewPostOnFB];
            }];

            
        }
        else{
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please select pet image."];
        }
    }
    else{
        
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please enter status."];
    }
}

#pragma mark - FB sharing methods -

-(void)ShareNewPostOnFB
{
    if (!FBSession.activeSession.isOpen)
    {
        [self openSessionWithAllowLoginUI:YES];
    }
    else
    {
        NSData* imageData = UIImageJPEGRepresentation(_imgPetPic.image, 90);
        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        _txtStatusOutlet.text, @"message",
                                        imageData, @"source",
                                        nil];
        
        [FBRequestConnection startWithGraphPath:@"me/photos"
                                     parameters:params
                                     HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
         {
             if (error)
             {
                 [SVProgressHUD dismiss];
              }
             else
             {
                 [SVProgressHUD dismiss];
                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE
                                                                     message:@"Successfully posted"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                 [alertView show];
              }
         }];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI
{
    NSArray *permissions = @[@"publish_actions"];
    
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                             if (error) {
                                              } else {
                                                 [self ShareNewPostOnFB];
                                             }
                                         }];
}

- (IBAction)postBtnAction:(UIButton *)sender {
    
    if (self.txtStatusOutlet.text.length>0 && ![self.txtStatusOutlet.text isEqualToString:@""]) {
        if (isImageSelected || self.recordingFileURLPath!=nil) {
            
            [APPDELEGATE ShowHUDWith:@"Loading..."];
            [self performSelector:@selector(callPostData:) withObject:nil afterDelay:0.3];
        } else {
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please select pet image."];
        }
    }
    else{
        
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please enter status."];
    }
 }

- (IBAction)tagBtnAction:(UIButton *)sender
{
    [self.txtStatusOutlet resignFirstResponder];
    isActionSheetOpen=YES;
    
    [self performSegueWithIdentifier:@"TagFollowingVC" sender:self];
    
}

- (IBAction)locationBtnAction:(UIButton *)sender {
    [self.txtStatusOutlet resignFirstResponder];
    [APPDELEGATE ShowHUDWith:@"Loading..."];//Prashant

    if ([CLLocationManager locationServicesEnabled])
    {
        if (IS_OS_8_OR_LATER)
        {
            [_locationManager requestWhenInUseAuthorization];
            [_locationManager requestAlwaysAuthorization];
        }
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [self.locationManager startUpdatingLocation];
            
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Location services are not enabled please enabled from settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Location delegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
     CLLocation *currentLocation = newLocation;
    self.strlatitude =[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    self.strlogitude =[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    if ([self.strlatitude length]<=0||self.strlogitude.length<=0) {
        self.strlatitude=@"0.0";
        self.strlogitude=@"0.0";
    }
     if (([self.strlogitude length]>0) && ([self.strlatitude length]>0)) {
        [self.locationManager stopUpdatingLocation];
       // self.locationManager=nil;
        
        CLLocationDegrees newLatitude = newLocation.coordinate.latitude;
        CLLocationDegrees newLongitude = newLocation.coordinate.longitude;
        CLLocationDegrees oldLatitude = oldLocation.coordinate.latitude;
        CLLocationDegrees oldLongitude = oldLocation.coordinate.longitude;
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:oldLatitude longitude:oldLongitude];
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:newLatitude longitude:newLongitude];
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        if (distance > 100) {
            [USERDEFAULTS setObject:self.strlatitude forKey:@"LastLatitude"];
            [USERDEFAULTS setObject:self.strlogitude forKey:@"LastLongitude"];
        }
        // Reverse Geocoding
         [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
             if (error == nil && [placemarks count] > 0) {
                placemark = [placemarks lastObject];
                NSString* strAddress;
                strAddress= [NSString stringWithFormat:@"%@",placemark.locality];
                  [self.locationManager stopUpdatingLocation];
                if ([self.btnLocationOutlet.subviews containsObject:titleLabel] ){
                    titleLabel.text=strAddress;
                }else{
                 
                    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
                    titleLabel.textColor=[UIColor colorWithRed:73.0/255.0 green:73.0/255.0 blue:73.0/255.0 alpha:1.0];
                    titleLabel.text=@"";
                    titleLabel.text = strAddress;
                    titleLabel.numberOfLines = 2;
                    titleLabel.tag=123456789;
                    
                    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                     [self.btnLocationOutlet addSubview:titleLabel];
                }
                [self.btnLocationOutlet setTitle:@"" forState:UIControlStateNormal];
                isLocationFound=YES;
                 [SVProgressHUD dismiss];
            } else {
                 [SVProgressHUD dismiss];
            }
        } ];
    }
}

- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error {
    self.strlatitude= [USERDEFAULTS objectForKey:@"LastLatitude"];
    self.strlogitude= [USERDEFAULTS objectForKey:@"LastLongitude"];
    if ([self.strlatitude length]<=0||self.strlogitude.length<=0) {
        self.strlatitude=@"0.0";
        self.strlogitude=@"0.0";
    }
    NSString *errorString;
    [manager stopUpdatingLocation];
     switch([error code]) {
        case kCLErrorDenied:
            //Access denied by user
            errorString = @"Location services are not enabled please enabled from settings.";
            //Do something...
            break;
        case kCLErrorLocationUnknown:
            //Probably temporary...
            errorString = @"Location data unavailable";
            //Do something else...
            break;
        default:
            errorString = @"An unknown error has occurred";
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
     [SVProgressHUD dismiss];
    
}

#pragma mark - UIActionSheetDelegate -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.txtStatusOutlet resignFirstResponder];
    switch(buttonIndex)
    {
        case 10:
        {
            if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
                /*UIImagePickerController * picker = [[UIImagePickerController alloc] init] ;
                picker.delegate = self;
                picker.allowsEditing=YES;
                isActionSheetOpen=YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:^{}];*/
                
                // Go to new Picker View Controller
                [self performSegueWithIdentifier:@"ImagePickerVC" sender:self];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"No Camera Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }
            break;
        case 11:
        {
            UIImagePickerController * picker = [[UIImagePickerController alloc] init] ;
            picker.delegate = self;
            picker.allowsEditing=YES;
            isActionSheetOpen=YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:^{}];
        }
        case 0:
        {
            AVPlayer *player = [AVPlayer playerWithURL:self.recordingFileURLPath];
            AVPlayerViewController *playerViewController = [AVPlayerViewController new];
            playerViewController.player = player;
            [self presentViewController:playerViewController animated:YES completion:nil];
        }
        default:
            // Do Nothing.........
            break;
    }
}

#pragma mark - UIImagePicker Delegate Methods -

- (void)processSelectedImage:(UIImage*)selectedImage
{
    isActionSheetOpen=YES;
    
    
    //current Date and time
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString *str = [formatter stringFromDate:date];
    
    //giving name to string with time stamp
    imageNameString =[NSString stringWithFormat:@"%@_%@.png",@"postImage",str];
    //code for storing selected image in document directory
    //selectedImage = [Constant scaleAndRotateImage:selectedImage];
    selectedImage = [Constant cropImage:selectedImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    savedImagePath = [documentsDirectory stringByAppendingPathComponent:imageNameString];
    NSData *imageData = UIImagePNGRepresentation(selectedImage);
    [imageData writeToFile:savedImagePath atomically:NO];
    isImageSelected = YES;
    
    self.imgPetPic.image=selectedImage;
    self.selectedImage=selectedImage;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *selectedImage=  [info objectForKey:@"UIImagePickerControllerEditedImage"];
    [self processSelectedImage:selectedImage];
    [self performSegueWithIdentifier:@"ImageFilterVC" sender:self];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    // Dismiss the image selection, hide the picker and
    //show the image view with the picked image
    isActionSheetOpen=YES;
    self.imgPetPic.image = image;
    isImageSelected=YES;
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Storyboard segue Methods -

-(IBAction)TaggedFollowingFriends:(UIStoryboardSegue *)sender{
    TagFollowingVC *tagfrds=sender.sourceViewController;
    
    arrFollowingUsers=[[NSMutableArray alloc]init];
    arrFollowingUsers=[tagfrds.arrayData mutableCopy];
    
    
    NSMutableArray *temp=[[NSMutableArray alloc]init];
    for (int i=0;i<[tagfrds.arrayData count]; i++) {
        NSDictionary *dict=[tagfrds.arrayData objectAtIndex:i];
        if ([[dict objectForKey:@"isSelected"] boolValue]==YES) {
            [temp addObject:dict];
        }
    }
    
    
    NSMutableString *strTagFrds=[[NSMutableString alloc] init];
    NSMutableString *followingFriendsID=[[NSMutableString alloc] init];
    
    if ([temp count]==1) {
        NSDictionary *dict=[temp objectAtIndex:0];
        NSString *lastTaggedFriend=[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_name"]];
        [strTagFrds appendString:[NSString stringWithFormat:@"%@ is tagged", lastTaggedFriend]];
        [followingFriendsID appendString:[NSString stringWithFormat:@"%@,", [dict objectForKey:@"user_id"]]];
        strTaggedUserId=[followingFriendsID substringToIndex:[followingFriendsID length]-1];
        
    }
    else if ([temp count]>1)
    {
        NSString *lastTaggedFriend;
        for (int i=0; i<[temp count]; i++) {
            NSDictionary *dict=[temp objectAtIndex:i];
            lastTaggedFriend=[NSString stringWithFormat:@"%@",[[temp objectAtIndex:[temp count]-1] objectForKey:@"user_name"]];
            [followingFriendsID appendString:[NSString stringWithFormat:@"%@,", [dict objectForKey:@"user_id"]]];
            strTaggedUserId=[followingFriendsID substringToIndex:[followingFriendsID length]-1];
        }
        [strTagFrds appendString:[NSString stringWithFormat:@"%@ and %lu other are tagged", lastTaggedFriend,[temp count]-1]];
    }
    
    
    if ([strTagFrds length]==0) {
        [btnTagOutlet setTitle:@"Tag" forState:UIControlStateNormal];
        
    }else{
        [btnTagOutlet setTitle:strTagFrds forState:UIControlStateNormal];
    }
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"TagFollowingVC"]) {
        TagFollowingVC *tagfrds=[segue destinationViewController];
        if ([arrFollowingUsers count]>0) {
            tagfrds.arrayData=[arrFollowingUsers mutableCopy];
        }
    } else if([segue.identifier isEqualToString:@"ImageFilterVC"]) {
        ImageFilterVC *ifVC = [segue destinationViewController];
        ifVC.delegate = self;
        ifVC.selectedImage = self.selectedImage;
    } else if ([segue.identifier isEqualToString:@"ImagePickerVC"]) {
        ImagePickerVC *ipVC = [segue destinationViewController];
        ipVC.delegate = self;
    }
}
#pragma mark - Connection Delegate methods -

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEPOSTSTATUS]) {
                 if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES ) {
                    isImageSelected=NO;
                    [self.statusPlaceholder setHidden:NO];
                    self.txtStatusOutlet.text=@"";
                    [btnTagOutlet setTitle:@"Tag" forState:UIControlStateNormal];
                    [arrFollowingUsers removeAllObjects];
                    strTaggedUserId=@"";
                    [self.imgPetPic setImage:[UIImage imageNamed:@"image_placeholder"]];
                    isLocationFound=NO;
                    [self.btnLocationOutlet setTitle:@"Location" forState:UIControlStateNormal];
                     for (id obje in [self.btnLocationOutlet subviews]) {
                        if ([obje isKindOfClass:[UILabel class]]) {
                            UILabel *objeRef=obje;
                            if (objeRef.tag==123456789) {
                                [obje removeFromSuperview];
                            }
                        }
                    }
                    [APPDELEGATE setIsNewFeed:YES];
                    [self.tabBarController setSelectedIndex:0];
                } else {
                    [SVProgressHUD dismiss];
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict valueForKey:@"message"]];
                }
             } else if([nState isEqualToString:STATEREVERSEPLACESEARCH]) {
                if ([dataDict objectForKey:@"status"]) {
                    if ([[dataDict objectForKey:@"status"] isEqualToString:@"OK"]) {
                        NSMutableArray *array = [dataDict objectForKey:@"results"];
                         if ([array count]>0) {
                            NSMutableDictionary * dictionary=[array objectAtIndex:0];
                            NSString * strCurLocAddress = [dictionary valueForKey:@"formatted_address"];
                            
                            UILabel *titleLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, self.btnLocationOutlet.frame.size.width-30, self.btnLocationOutlet.frame.size.height)];
                            titleLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
                            titleLabel1.textColor=[UIColor colorWithRed:73.0/255.0 green:73.0/255.0 blue:73.0/255.0 alpha:1.0];
                            titleLabel1.text = strCurLocAddress;
                            titleLabel1.numberOfLines = 2;
                            titleLabel1.tag=123456789;
                            titleLabel1.lineBreakMode = NSLineBreakByTruncatingTail;
                            [self.btnLocationOutlet addSubview:titleLabel1];
                            [self.btnLocationOutlet setTitle:@"" forState:UIControlStateNormal];
                            
                             isLocationFound=YES;
                        }
                    } else {
                        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Place not found."];
                    }
                }
             } else if ([nState isEqualToString:STATEPOSTVIDEO]) {
                 if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES ) {
                     isImageSelected=NO;
                     [self.statusPlaceholder setHidden:NO];
                     self.txtStatusOutlet.text=@"";
                     [btnTagOutlet setTitle:@"Tag" forState:UIControlStateNormal];
                     [arrFollowingUsers removeAllObjects];
                     strTaggedUserId=@"";
                     [self.imgPetPic setImage:[UIImage imageNamed:@"image_placeholder"]];
                     isLocationFound=NO;
                     [self.btnLocationOutlet setTitle:@"Location" forState:UIControlStateNormal];
                     for (id obje in [self.btnLocationOutlet subviews]) {
                         if ([obje isKindOfClass:[UILabel class]]) {
                             UILabel *objeRef=obje;
                             if (objeRef.tag==123456789) {
                                 [obje removeFromSuperview];
                             }
                         }
                     }
                     [APPDELEGATE setIsNewFeed:YES];
                     [self.tabBarController setSelectedIndex:0];
                 } else {
                     [SVProgressHUD dismiss];
                     [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict valueForKey:@"message"]];
                 }
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:nData];
}

#pragma mark - UITextView Delegate method -

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    
     if (newLength>0) {
        [self.statusPlaceholder setHidden:YES];
    }
    else{
        [self.statusPlaceholder setHidden:NO];
    }
    return  YES;
}

#pragma mark - ImageFilterVCDelegate Methods
- (void)imageFilterVCDidDismissWithImage:(UIImage*)filteredImage
{
    self.imgPetPic.image=filteredImage;
    self.selectedImage=filteredImage;
    [self processSelectedImage:filteredImage];
}

#pragma mark - ImagePickerVCDelegate Methods
- (void)imagePickerVCDidCaptureImage:(UIImage*)image
{
    [self processSelectedImage:image];
}
- (void)imagePickerVCDidRecordVideo:(NSURL*)videoURL
{
    self.selectedImage = [UIImage imageNamed:@"image-video-icon.png"];
    [self processSelectedImage:self.selectedImage];
    self.recordingFileURLPath = videoURL;
}

@end