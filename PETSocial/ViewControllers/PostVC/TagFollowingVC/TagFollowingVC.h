//
//  TagFollowingVC.h
//  PETSocial
//
//  Created by milap kundalia on 7/29/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagFollowingVC : UIViewController
- (IBAction)btnBackAcion:(id)sender;
- (IBAction)btnDoneAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *followingTableViewOutlet;
@property (strong, nonatomic)  NSMutableArray *arrayData;
@end

