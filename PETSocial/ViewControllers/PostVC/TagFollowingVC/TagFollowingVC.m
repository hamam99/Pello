//
//  TagFollowingVC.m
//  PETSocial
//
//  Created by milap kundalia on 7/29/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "TagFollowingVC.h"
#import "FollowFollowerCell.h"

@interface TagFollowingVC ()

@end

@implementation TagFollowingVC
@synthesize followingTableViewOutlet,arrayData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    if (arrayData.count>0) {
        
    }else{
        [self callserviceForList];
    }
    
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)callserviceForList{
    NSString *strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",strMyUserID,@"user_id" ,nil];
    
    [[Connection sharedConnectionWithDelegate:self] followingList:dict];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID =  @"FollowFollowerCell";
    
    FollowFollowerCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[FollowFollowerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.lblUserName.text=[[arrayData objectAtIndex:indexPath.row] objectForKey:@"user_name"];
    
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[[arrayData objectAtIndex:indexPath.row] objectForKey:@"user_image"]];
    
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgUsername setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
    
    cell.actionBtn.userInteractionEnabled=NO;
    NSDictionary * dict=[arrayData objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"isSelected"] boolValue])
    {
        cell.actionBtn.selected=YES;
    }
    else
    {
        cell.actionBtn.selected=NO;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSMutableDictionary * dict=[[arrayData objectAtIndex:indexPath.row] mutableCopy];
    [arrayData removeObjectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"isSelected"] boolValue]) {
        [dict setObject:@"0" forKey:@"isSelected"];
        [arrayData insertObject:dict atIndex:indexPath.row];
    }
    else{
        [dict setObject:@"1" forKey:@"isSelected"];
        [arrayData insertObject:dict atIndex:indexPath.row];
    }
    
    [tableView reloadData];
    
    
}

- (IBAction)btnBackAcion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnDoneAction:(id)sender {
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEFOLLOWING]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    arrayData =[dataDict objectForKey:@"data"];
                }
                else{
                }
                [followingTableViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
 }

@end
