//
//  PrivacyPolicyViewController.m
//  PETSocial
//
//  Created by milap kundalia on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "PrivacyPolicyViewController.h"

@interface PrivacyPolicyViewController ()

@end

@implementation PrivacyPolicyViewController
@synthesize activityLoader,webviewPrivacyPolicy;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    self.tabBarController.tabBar.hidden=YES;
    [APPDELEGATE imagefooter].hidden=YES;
    NSString *strHTMLFilePath=[[NSBundle mainBundle] pathForResource:@"privacy_policy" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:strHTMLFilePath encoding:NSUTF8StringEncoding error:nil];
    [webviewPrivacyPolicy loadHTMLString:htmlString baseURL:nil];
    // Do any additional setup after loading the view.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnAction:(UIButton *)sender{
    
    self.tabBarController.tabBar.hidden=NO;
    [APPDELEGATE imagefooter].hidden=NO;
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [activityLoader startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [activityLoader stopAnimating];
}
@end
