//
//  BlockUserListVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/5/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockUserListVC : UIViewController
- (IBAction)backBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblViewOutlet;
@property (nonatomic,readwrite)   int newPage;

@end
