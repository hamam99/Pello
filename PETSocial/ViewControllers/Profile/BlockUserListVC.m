//
//  BlockUserListVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/5/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "BlockUserListVC.h"
#import "FollowFollowerCell.h"


@interface BlockUserListVC ()
{
    NSMutableArray *arrayData;
    UIView *viewFooter;
    UIActivityIndicatorView *pageLoader;
    BOOL isMoreData;
    BOOL isValidateData;
    int mainPageId;
    UIButton *blockBtnRef;
}
@end

@implementation BlockUserListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    // Do any additional setup after loading the view.
    [self setUpFooterView];
    [self callBlockList:1];
}
-(void)setUpFooterView{
    viewFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50)];
    pageLoader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [pageLoader setColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    pageLoader.center = viewFooter.center;
    pageLoader.hidesWhenStopped = YES;
    [viewFooter addSubview:pageLoader];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)callBlockList:(int)pageNumber{
    arrayData=[[NSMutableArray alloc]init];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",[NSString stringWithFormat:@"%d",pageNumber],@"page", nil];
    [[Connection sharedConnectionWithDelegate:self] blockUserList:dict];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)unblockBtnAction:(UIButton *)sender{
    //
    blockBtnRef=sender;
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Pello" message:@"Do you want to unblock this user?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
    alert.tag=123;
    [alert show];
    
    
}
#pragma mark - Table View methods -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowFollowerCell *cell = (FollowFollowerCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowFollowerCell" forIndexPath:indexPath];
    NSDictionary *dict=[arrayData objectAtIndex:indexPath.row];
    
    cell.lblUserName.text=[dict objectForKey:@"user_name"];
    cell.btnUnblock.tag=indexPath.row;
    [cell.btnUnblock addTarget:self action:@selector(unblockBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.imgUsername.layer.borderWidth = 1.0f;
    cell.imgUsername.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    cell.imgUsername.layer.masksToBounds = NO;
    cell.imgUsername.clipsToBounds = YES;
    cell.imgUsername.layer.cornerRadius = cell.imgUsername.frame.size.width/2;
    
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_image"]];
    
    
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgUsername setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    
    return cell;
}

#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEBLOCKUSERLIST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    arrayData=[dataDict objectForKey:@"data"];
                    if (!isValidateData) {
                        
                        arrayData=[[NSMutableArray alloc]init];
                        arrayData=[dataDict objectForKey:@"data"];
                    }
                    else{
                        arrayData=[[arrayData arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    [self.tblViewOutlet reloadData];
                }
                else{
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"No record found"];
                }
                [self.tblViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEBLOCKUSER])
            {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [self callBlockList:self.newPage];
                    [APPDELEGATE setIsNewFeed:YES]; // Harshit
                }
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
}
#pragma UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==123) {
        if(buttonIndex==1){
            CGPoint buttonPosition = [blockBtnRef convertPoint:CGPointZero toView:self.tblViewOutlet];
            NSIndexPath *commentIndexPath = [self.tblViewOutlet indexPathForRowAtPoint:buttonPosition];
            
            NSString *strId=[[arrayData objectAtIndex:commentIndexPath.row] objectForKey:@"user_id"];
            
            [APPDELEGATE ShowHUDWith:@"Loading..."];
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strId,@"block_user_id", nil];
            [[Connection sharedConnectionWithDelegate:self]blockUser:dict];
        }
    }
    
}

#pragma mark - Pagination -
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            [self callBlockList:self.newPage];
            self.tblViewOutlet.tableFooterView=viewFooter;
            [pageLoader startAnimating];
            
        }
        
        //Put your load more data method here...
    }
    
}
@end
