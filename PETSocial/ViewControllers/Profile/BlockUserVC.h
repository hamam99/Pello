//
//  BlockUserVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/21/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockUserVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *blockView;
- (IBAction)blockBtnAction:(UIButton *)sender;
- (IBAction)cancelBtnAction:(UIButton *)sender;
- (void)presentActionSheet;
@property (strong, nonatomic) NSString *userData;
@property (strong, nonatomic) UINavigationController *navController;
@end
