//
//  BlockUserVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/21/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "BlockUserVC.h"

@interface BlockUserVC ()

@end

@implementation BlockUserVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    [super viewDidLoad];
    UIImageView *backImage=(UIImageView *)[[self view] viewWithTag:245];
    [backImage setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(outSideViewAction:)];
    [backImage addGestureRecognizer:tapGesture];

}
-(void)viewWillAppear:(BOOL)animated{
    self.blockView.hidden = TRUE;
    CGRect frame1 = self.blockView.frame;
    frame1.origin = CGPointMake(0.0, self.view.bounds.size.height+frame1.size.height);
    self.blockView.frame = frame1;

}
-(void)outSideViewAction:(UITapGestureRecognizer *)sender{
    [self slideOut];
}
- (void)presentActionSheet {
    [[[self view] viewWithTag:245] setAlpha:0];
    [UIView animateWithDuration:0.1 animations:^{
        [[[self view] viewWithTag:245] setAlpha:1];
    }];
    
	[UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.blockView.frame;
        frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
        self.blockView.frame = frame;
         frame.origin = CGPointMake(0.0, self.view.bounds.size.height - self.blockView.bounds.size.height);
        self.blockView.frame = frame;
        self.blockView.hidden = FALSE;

        [self.blockView setUserInteractionEnabled:YES];
    } completion:^(BOOL finished) {
        
    }];
 }
- (void) slideOut {
    [UIView animateWithDuration:0.2 animations:^{
        // Move this view to bottom of superview
        CGRect frame = self.blockView.frame;
        frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
        self.blockView.frame = frame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            // Move this view to bottom of superview
            [self.view setAlpha:0];
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
        }];
    }];
    
    
 }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 - (IBAction)blockBtnAction:(UIButton *)sender {
// http://inheritx.dnsdynamic.com:8590/petsocialnew/index.php?do=/webservice/block
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Are you sure you want to block this user?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=5000;
    [alert show];

 }


- (IBAction)cancelBtnAction:(UIButton *)sender {
    [self slideOut];
    
}
#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEBLOCKUSER]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                     [self slideOut];
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Pello" message:[dataDict objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alert.tag=123;
                    [alert show];

                    [APPDELEGATE setIsNewFeed:YES]; // Harshit
                  
                }
                else{
                 }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }
#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==123) {
        [self.navController popToRootViewControllerAnimated:YES];
    }
    
    if (alertView.tag==5000) {
        if (buttonIndex==1) {
             [APPDELEGATE ShowHUDWith:@"Loading..."];
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",self.userData,@"block_user_id", nil];
            [[Connection sharedConnectionWithDelegate:self] blockUser:dict];
            
        }
    }

}

 @end
