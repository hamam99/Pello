//
//  ChangePasswordVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController<BSKeyboardControlsDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
- (IBAction)changePasswordBtnAction:(UIButton *)sender;

- (IBAction)backBtnAction:(UIButton *)sender;

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
