//
//  EditProfileVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/25/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
- (IBAction)backBtnAction:(UIButton *)sender;
- (IBAction)doneBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileOutlet;
- (IBAction)changePasswordAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtPetName;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtMailId;
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *twitterTextField;
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (nonatomic, strong) NSDictionary *userDetailDict;
@end
