//
//  EditProfileVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/25/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "EditProfileVC.h"


@interface EditProfileVC ()
{
    BOOL isActionSheetOpen;
    BOOL isImageSelected;
    NSString *imageNameString;
    NSString *savedImagePath;
}
@end

@implementation EditProfileVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    
    self.imgProfileOutlet.layer.borderWidth = 2.0f;
    self.imgProfileOutlet.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    self.imgProfileOutlet.layer.masksToBounds = NO;
    self.imgProfileOutlet.clipsToBounds = YES;
    self.imgProfileOutlet.layer.cornerRadius = self.imgProfileOutlet.frame.size.width/2;
    [self.imgProfileOutlet setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileImageAction:)];
    [self.imgProfileOutlet addGestureRecognizer:tapGesture];
    [self setTextValue];
    
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    [self registerForKeyboardNotifications];
    
    
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    keyboardToolBar.barStyle = UIBarStyleDefault;
    [keyboardToolBar setItems: [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard:)],
                                nil]];
    self.descriptionTextView.inputAccessoryView = keyboardToolBar;
    
}

- (void)resignKeyboard:(id)sender
{
    [self.descriptionTextView resignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.twitterTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.twitterTextField.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}


-(void)setTextValue{
    NSDictionary *dict=self.userDetailDict;
    self.txtUsername.text=[dict objectForKey:@"user_name"];
    self.txtMailId.text=[dict objectForKey:@"email"];
    self.txtPetName.text=[dict objectForKey:@"pet_name"];
    
    self.twitterTextField.text = [dict objectForKey:@"twitter_username"];
    self.urlTextField.text = [dict objectForKey:@"website_url"];
    self.descriptionTextView.text = [dict objectForKey:@"description"];
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_image"]];
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [self.imgProfileOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
}
-(void)profileImageAction:(UITapGestureRecognizer *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Profile Picture"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Select from Library", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    actionSheet.tag=1;
    [actionSheet showInView:self.view];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveChangesButtonDidPushed:(id)sender {
    [self.txtPetName resignFirstResponder];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if (self.txtPetName.text.length>0) {
        [dic setObject:self.txtPetName.text forKey:@"cf_pet_name"];
    }
    
    if (self.txtPetName.text.length > 50) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (self.twitterTextField.text.length>0) {
        [dic setObject:self.twitterTextField.text forKey:@"cf_twitter_username"];
    }
    
    if (self.urlTextField.text.length>0) {
        [dic setValue:self.urlTextField.text forKey:@"cf_website_url"];
    }
    
    if (self.descriptionTextView.text.length>0) {
        [dic setObject:self.descriptionTextView.text forKey:@"cf_description"];
    }
    
    if ([dic count]>0) {
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        [dic setObject:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"] forKey:@"user_id"];
        if (imageNameString) {
            [dic setObject:imageNameString forKey:@"profile_image"];
        }
        [[Connection sharedConnectionWithDelegate:self] editProfileDetail:dic];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ChangePassword"]) {
        
    }
}


- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneBtnAction:(UIButton *)sender {
    //http://inheritx.dnsdynamic.com:8590/petsocialnew/index.php?do=/webservice/editprofile
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)changePasswordAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ChangePassword" sender:sender];
}
#pragma mark - UIActionSheetDelegate methods -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{UIImagePickerController * picker;
    switch(buttonIndex)
    {
        case 0:
        {
            if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
                picker = [[UIImagePickerController alloc] init] ;
                picker.delegate = self;
                picker.allowsEditing=YES;
                picker.view.tag=actionSheet.tag;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:^{}];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Pello" message:@"No Camara Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }
        break;
        case 1:
        {
            picker = [[UIImagePickerController alloc] init] ;
            picker.delegate = self;
            picker.allowsEditing=YES;
            picker.view.tag=actionSheet.tag;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:^{}];
        }
        default:
        break;
    }
}
#pragma mark - UIImagePicker Delegate Methods -

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    isActionSheetOpen=YES;
    UIImage *selectedImage=  [info objectForKey:@"UIImagePickerControllerEditedImage"];
    self.imgProfileOutlet.image=selectedImage;
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    //current Date and time
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString* str = [formatter stringFromDate:date];
    
    
    //giving name to string with time stamp
    imageNameString=[NSString stringWithFormat:@"%@_%@.png",@"profileImage",str];
    
    //code for storing selected image in document directory
    selectedImage = [Constant scaleAndRotateImage:selectedImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    savedImagePath= [documentsDirectory stringByAppendingPathComponent:imageNameString];
    NSData *imageData = UIImagePNGRepresentation(selectedImage);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    isImageSelected=YES;
    
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    // Dismiss the image selection, hide the picker and
    //show the image view with the picked image
    isActionSheetOpen=YES;
    self.imgProfileOutlet.image = image;
    isImageSelected=YES;
    [picker dismissViewControllerAnimated:YES completion:^{}];
}
#pragma mark UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark Connection Delegate methods -

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEEDITPROFILE]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [APPDELEGATE setIsProfileEdited:YES];

                    [USERDEFAULTS removeObjectForKey:KEYUSERINFO];
                    [USERDEFAULTS setObject:[dataDict objectForKey:@"data"] forKey:KEYUSERINFO];
                    [self setTextValue];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else{
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict valueForKey:@"message"]];
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }
@end
