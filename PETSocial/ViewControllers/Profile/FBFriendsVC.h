//
//  FBFriendsVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBFriendsVC : UIViewController
- (IBAction)backBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblViewOutlet;

@end
