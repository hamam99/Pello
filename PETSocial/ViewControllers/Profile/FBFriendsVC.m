//
//  FBFriendsVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "FBFriendsVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "FollowFollowerCell.h"

@interface FBFriendsVC ()
{
    NSString *finalFBfriendStr;
    NSMutableArray *friendList;
}
@end

@implementation FBFriendsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(findFriendsFB:) name:FBFindFriendsNotification object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [APPDELEGATE ShowHUDWith:@"Loading.."];
    [APPDELEGATE openSessionWithFriends:YES];
}
- (void)findFriendsFB:(NSNotification*)notification {
    
    if (FBSession.activeSession.isOpen) {
        
        [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection,id results,NSError *error)
         {
             if (!error) {
                 NSDictionary *dict= (NSDictionary *)results;
                 
                 NSArray *fbFriends=(NSArray *)[dict objectForKey:@"data"];
                 NSMutableString *str=[[NSMutableString alloc]init];
                 
                 for (NSDictionary *dict in fbFriends) {
                     [str appendString:[NSString stringWithFormat:@"%@,",[dict objectForKey:@"id"]]];
                 }
                 
                 if ([str length] > 0) {
                     finalFBfriendStr = [str substringToIndex:[str length] - 1];
                 }
                 if (finalFBfriendStr.length) {
                     [self callFBFriendsService:finalFBfriendStr];
                 }
                 
             }
         }];
    }
}
-(void)callFBFriendsService:(NSString *)fbIdStr{
    
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"my_user_id",fbIdStr,@"friends_fb_ids", nil];
    [[Connection sharedConnectionWithDelegate:self] getFBFriendList:dict];
}
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:FBFindFriendsNotification];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)followUnfollowBtnAction:(UIButton *)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblViewOutlet];
    NSIndexPath *indexPath = [self.tblViewOutlet indexPathForRowAtPoint:buttonPosition];
    
    [APPDELEGATE ShowHUDWith:@"Loading.."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",[[friendList objectAtIndex:indexPath.row]objectForKey:@"user_id"],@"follow_id",nil];
    
    [[Connection sharedConnectionWithDelegate:self] FollowUnFollowUser:dict];
}

#pragma mark - Table View methods -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return friendList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowFollowerCell *cell = (FollowFollowerCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowFollowerCell" forIndexPath:indexPath];
    NSDictionary *dict=[friendList objectAtIndex:indexPath.row];
    
    cell.lblUserName.text=[dict objectForKey:@"user_name"];
    cell.actionBtn.tag=indexPath.row;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([[dict objectForKey:@"following"] boolValue]==NO) {
        [cell.actionBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
        [cell.actionBtn addTarget:self action:@selector(followUnfollowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }else {
        [cell.actionBtn setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
        [cell.actionBtn addTarget:self action:@selector(followUnfollowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    cell.imgUsername.layer.borderWidth = 1.0f;
    cell.imgUsername.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    cell.imgUsername.layer.masksToBounds = NO;
    cell.imgUsername.clipsToBounds = YES;
    cell.imgUsername.layer.cornerRadius = cell.imgUsername.frame.size.width/2;
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_image"]];
    
    //for caching image is necessory then use this.
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgUsername setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEFBFriends]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    friendList=[dataDict objectForKey:@"data"];
                    [self.tblViewOutlet reloadData];
                }
                else{
                }
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEFOllOWUNFOLLOW]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [self callFBFriendsService:finalFBfriendStr];
                }
                else{
                }
                [self.tblViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
}

@end
