//
//  FindPeopleVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/24/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "FindPeopleVC.h"
#import "FBFriendsVC.h"


@interface FindPeopleVC ()

@end
 
@implementation FindPeopleVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
//    [APPDELEGATE openSessionWithFriends:NO];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"FacebookFriend"]) {

        
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)findFacebookBtnAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"FacebookFriend" sender:sender];
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
