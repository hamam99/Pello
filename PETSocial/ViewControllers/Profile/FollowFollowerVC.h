//
//  FollowFollowerVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/24/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowFollowerVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTopHeader;
@property (weak, nonatomic) IBOutlet UITableView *tblViewOutlet;
- (IBAction)backBtnAction:(UIButton *)sender;
@property (weak, nonatomic) NSString *strScreenName;
@property (strong, nonatomic) NSString *strUserId;

@end
