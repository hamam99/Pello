//
//  FollowFollowerVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/24/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "FollowFollowerVC.h"
#import "FollowFollowerCell.h"
#import "UserProfileVC.h"

@interface FollowFollowerVC ()
{
    NSMutableArray *arrayData;
    NSString *strMyUserID;
    NSDictionary *dictObject;
}
@end

@implementation FollowFollowerVC
@synthesize strUserId;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    if ([self.strScreenName isEqualToString:@"Follower"]) {
        self.lblTopHeader.text=@"FOLLOWERS";
    }
    else{
        self.lblTopHeader.text=@"FOLLOWING";
    }
    strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    
    [self callserviceForList];
    
    
}
-(void)callserviceForList{
    [APPDELEGATE ShowHUDWith:@"Loading.."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",strUserId,@"user_id" ,nil];
    if ([self.strScreenName isEqualToString:@"Follower"]) {
        [[Connection sharedConnectionWithDelegate:self] followerList:dict];
    }
    else{
        [[Connection sharedConnectionWithDelegate:self] followingList:dict];
    }
}

-(IBAction)BtnFollowUnfollowAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblViewOutlet];
    NSIndexPath *indexPath = [self.tblViewOutlet indexPathForRowAtPoint:buttonPosition];
    dictObject=[arrayData objectAtIndex:indexPath.row];
    
    if ([[dictObject objectForKey:@"following"] boolValue]==YES) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Do you want to unfollow?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
        alert.tag=123;
        [alert show];
    }
    else{
        [APPDELEGATE ShowHUDWith:@"Loading.."];
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"user_id",
                            [dictObject objectForKey:@"user_id"],@"follow_id",nil];
        
        [[Connection sharedConnectionWithDelegate:self] FollowUnFollowUser:dict];
    }
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Table View methods -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowFollowerCell *cell = (FollowFollowerCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowFollowerCell" forIndexPath:indexPath];
    NSDictionary *dict=[arrayData objectAtIndex:indexPath.row];
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"user_image"]];
    //for caching image is necessory then use this.
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgUsername setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    
    cell.lblUserName.text=[dict objectForKey:@"user_name"];
    
    if ([[dict objectForKey:@"following"] boolValue]==NO) {
        [cell.actionBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
        [cell.actionBtn addTarget:self action:@selector(BtnFollowUnfollowAction:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.actionBtn setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
        [cell.actionBtn addTarget:self action:@selector(BtnFollowUnfollowAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([[dict objectForKey:@"user_id"]isEqualToString:strMyUserID]) {
        [cell.actionBtn setImage:nil forState:UIControlStateNormal];
    }
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.imgUsername.layer.borderWidth = 1.0f;
    cell.imgUsername.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    cell.imgUsername.layer.masksToBounds = NO;
    cell.imgUsername.clipsToBounds = YES;
    cell.imgUsername.layer.cornerRadius = cell.imgUsername.frame.size.width/2;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict=[arrayData objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"user_id"]isEqualToString:strMyUserID]) {
        [self.tabBarController setSelectedIndex:4];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"SingleProfileVC"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        userProfile.userDetails=[arrayData objectAtIndex:indexPath.row];
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
#pragma mark UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==123) {
        if (buttonIndex==1) {
            
            [APPDELEGATE ShowHUDWith:@"Loading.."];
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"user_id",
                                [dictObject objectForKey:@"user_id"],@"follow_id",nil];
            
            [[Connection sharedConnectionWithDelegate:self] FollowUnFollowUser:dict];
        }
    }
}
#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEFOLLOWER]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    arrayData=[dataDict objectForKey:@"data"];
                }
                else{
                    [[arrayData mutableCopy] removeAllObjects];
                }
                [self.tblViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEFOLLOWING]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    arrayData=[dataDict objectForKey:@"data"];
                }
                else{
                    [arrayData removeAllObjects];
                }
                [self.tblViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEFOllOWUNFOLLOW]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [self callserviceForList];
                }
                else{
                }
                [self.tblViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
}
@end
