//
//  ProfileCollectionCell.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/25/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPetOutlet;

@end
