//
//  ProfileVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilepic;
@property (weak, nonatomic) IBOutlet UIView *viewPost;
@property (weak, nonatomic) IBOutlet UIView *viewFollower;
@property (weak, nonatomic) IBOutlet UIView *viewFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblPostNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowerNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowingNumber;
- (IBAction)moreBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UILabel *profileHeader;
- (IBAction)settingBtnAction:(UIButton *)sender;
- (IBAction)editProfile:(UIButton *)sender;
- (IBAction)segmentBtnAction:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *segmentImageOutlet;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *profileNameHeader;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentBtnOutlet;
@property (nonatomic, readwrite) int newPage;


@end
