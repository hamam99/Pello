//
//  SeetingVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/24/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeetingVC : UIViewController
- (IBAction)backBtnAction:(UIButton *)sender;
- (IBAction)logoutBtnAction:(UIButton *)sender;
- (IBAction)shareSettingBtnAction:(UIButton *)sender;
- (IBAction)termsConditionBtnAction:(UIButton *)sender;
- (IBAction)privacyBtnAction:(UIButton *)sender;
- (IBAction)findPeopleBtnAction:(UIButton *)sender;
- (IBAction)manageUSer:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
