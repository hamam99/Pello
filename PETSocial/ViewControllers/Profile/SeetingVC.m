//
//  SeetingVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/24/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "SeetingVC.h"
#import <FacebookSDK/FacebookSDK.h>

@interface SeetingVC ()

@end

@implementation SeetingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
}
-(void)viewWillAppear:(BOOL)animated
{
    if (iPhone4)
        _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 405);
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)logoutBtnAction:(UIButton *)sender {
    
    NSString *deviceToken=[NSString stringWithFormat:@"%@",[USERDEFAULTS objectForKey:@"Devicetoken"]];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [USERDEFAULTS setObject:KEYLOGEDINNO forKey:KEYISLOGEDIN];
    [USERDEFAULTS setObject:deviceToken forKey:@"Devicetoken"];

    
    [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
    [[APPDELEGATE alertTimer] invalidate];
}

- (IBAction)shareSettingBtnAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ShareSettingsSegue" sender:self];

}

- (IBAction)termsConditionBtnAction:(UIButton *)sender {

    [self performSegueWithIdentifier:@"TermsVC" sender:self];

}

- (IBAction)privacyBtnAction:(UIButton *)sender {

    [self performSegueWithIdentifier:@"PrivacySegue" sender:self];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([segue.identifier isEqualToString:@"PrivacySegue"]) {
    }
    else if ([segue.identifier isEqualToString:@"TermsVC"]) {
    }
    else if ([segue.identifier isEqualToString:@"ShareSettingsSegue"]) {
    }
    else if ([segue.identifier isEqualToString:@"BlockUserVC"]) {
    }
}
- (IBAction)findPeopleBtnAction:(UIButton *)sender {
    
}

- (IBAction)manageUSer:(UIButton *)sender {
    [self performSegueWithIdentifier:@"BlockUserVC" sender:self];
}
@end
