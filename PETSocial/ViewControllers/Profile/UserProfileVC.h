//
//  UserProfileVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/21/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlockUserVC.h"

@interface UserProfileVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *userProfilePic;
- (IBAction)followUnfollowBtnAction:(UIButton *)sender;
- (IBAction)moreBtnActio:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *topProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblpostNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFolloerNumbr;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowingNumber;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;
- (IBAction)segmentAction:(UISegmentedControl *)sender;
- (IBAction)backBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *followUnFollowBtnOutlet;

@property (strong, nonatomic) IBOutlet UIView *blockView;
@property (strong, nonatomic) NSDictionary *userDetails;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentBtnOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *segmentImageOutlet;

@property (strong, nonatomic) BlockUserVC *blockUserVC;

@property (nonatomic, readwrite) int newPage;

@property (weak, nonatomic) IBOutlet UIView *viewPost;
@property (weak, nonatomic) IBOutlet UIView *viewFollower;
@property (weak, nonatomic) IBOutlet UIView *viewFollowing;
@end
