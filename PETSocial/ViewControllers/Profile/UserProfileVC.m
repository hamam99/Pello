//
//  UserProfileVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/21/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "UserProfileVC.h"
#import "ProfileCollectionCell.h"
#import "SinglePostVC.h"
#import "FollowFollowerVC.h"
#import "FeedVC.h"
#import "ExploreCell.h"
#import "ProfileUpperViewCell.h"


@interface UserProfileVC ()
{
    NSString *strMyUserID;
    NSMutableArray *usersPost;
    NSMutableArray *taggedPost;
    NSString *strScreenName;
    BOOL isMoreData;
    BOOL isValidateData;
}
@end

@implementation UserProfileVC
@synthesize segmentBtnOutlet,segmentImageOutlet;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    
    [self singleProfile];
    [self setUpViewForDisplay];
    
    strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    
    
    
    UITapGestureRecognizer *followTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followTapAction:)];
    [self.viewFollower addGestureRecognizer:followTap];
    
    UITapGestureRecognizer *followingTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followingTapAction:)];
    [self.viewFollowing addGestureRecognizer:followingTap];
    // Do any additional setup after loading the view.
    if (usersPost.count>0) {
        UITapGestureRecognizer *allPost=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(allPostTapAction:)];
        [self.viewPost addGestureRecognizer:allPost];
    }
 }
-(void)singleProfile{
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[self.userDetails objectForKey:@"user_name"],@"search",[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id", nil];
    [[Connection sharedConnectionWithDelegate:self] findPeopleUsingName:dict];
}
-(void)setUpViewForDisplay{
    usersPost=[[NSMutableArray alloc]init];
    taggedPost=[[NSMutableArray alloc]init];
    self.topProfileName.text=[[self.userDetails objectForKey:@"user_name"] uppercaseString];
    self.lblpostNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_posts"]];
    self.lblFollowingNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_following"]];
    self.lblFolloerNumbr.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_follower"]];
    
    
    
    //for caching image is necessory then use this.
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"user_image"]]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [self.userProfilePic setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    
    
    self.userProfilePic.layer.borderWidth = 2.0f;
    self.userProfilePic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    self.userProfilePic.layer.masksToBounds = NO;
    self.userProfilePic.clipsToBounds = YES;
    self.userProfilePic.layer.cornerRadius = self.userProfilePic.frame.size.width/2;
    
    if ([[self.userDetails objectForKey:@"following"] boolValue]==YES) {
        [self.followUnFollowBtnOutlet setImage:[UIImage imageWithContentsOfBundleFileName:@"btn_following_profile"] forState:UIControlStateNormal];
    }
    else{
        [self.followUnFollowBtnOutlet setImage:[UIImage imageWithContentsOfBundleFileName:@"btn_follow_profile"] forState:UIControlStateNormal];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (IBAction)followUnfollowBtnAction:(UIButton *)sender {
    
     if ([[self.userDetails objectForKey:@"following"] boolValue]==YES) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Do you want to unfollow?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
        alert.tag=123;
        [alert show];
    }
    else{
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        
        strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"user_id",
                            [self.userDetails objectForKey:@"user_id"],@"follow_id",nil];
        
        [[Connection sharedConnectionWithDelegate:self] FollowUnFollowUser:dict];
    }
    
    
}
- (IBAction)moreBtnActio:(UIButton *)sender {
    
    self.blockUserVC= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"BlockUserVC"];
    //    self.blockUserVC.postData=[arrayFeedData objectAtIndex:indexPath.section];
    self.blockUserVC.userData=[self.userDetails objectForKey:@"user_id"];
    self.blockUserVC.navController=self.navigationController;
    [[APPDELEGATE window] addSubview:self.blockUserVC.view];
    [self.blockUserVC presentActionSheet];
    
}
-(void)callForPostAndTaggedPost:(int) pageNumber{
    
     if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",[self.userDetails objectForKey:@"user_id"],@"user_id",[NSString stringWithFormat:@"%d",pageNumber],@"page", nil];
        [[Connection sharedConnectionWithDelegate:self] getPostOfUser:dict];
        
    }
    else if (self.segmentBtnOutlet.selectedSegmentIndex==1) {
        //{"user_id":"1","my_user_id":"","page":"1"}
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",[self.userDetails objectForKey:@"user_id"],@"user_id",[NSString stringWithFormat:@"%d",pageNumber],@"page", nil];
        [[Connection sharedConnectionWithDelegate:self] getTaggedPostOfUser:dict];
    }
}


- (IBAction)segmentAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex==0) {
        [self.segmentImageOutlet setImage:[UIImage imageNamed:@"segment1"]];
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        [self callForPostAndTaggedPost:1];
    }
    else if(sender.selectedSegmentIndex==1){
        [self.segmentImageOutlet setImage:[UIImage imageNamed:@"segment2"]];
        isValidateData=NO;
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        [self callForPostAndTaggedPost:1];
    }
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==123) {
        if (buttonIndex==1) {
            [APPDELEGATE ShowHUDWith:@"Loading..."];
            
            strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"user_id",
                                [self.userDetails objectForKey:@"user_id"],@"follow_id",nil];
            
            [[Connection sharedConnectionWithDelegate:self] FollowUnFollowUser:dict];
        }
    }
}

#pragma mark - UICollectionView Datasource and Delegate -
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    
   
    if (self.segmentBtnOutlet.selectedSegmentIndex==0){
        if (indexPath.section==0) {
            return CGSizeMake(320*fltMultiplyingFactor, 271*fltMultiplyingFactor);
        }
        else{
            if (indexPath.row==usersPost.count && usersPost.count>collectionItems) {
                return CGSizeMake(300*fltMultiplyingFactor, 50*fltMultiplyingFactor);
            }
            else{
                return CGSizeMake(152*fltMultiplyingFactor, 152*fltMultiplyingFactor);
            }
        }
        
    }else{
        if (indexPath.section==0) {
            return CGSizeMake(320*fltMultiplyingFactor, 271*fltMultiplyingFactor);
        }
        else{
            if (indexPath.row==taggedPost.count && taggedPost.count>collectionItems) {
                return CGSizeMake(300*fltMultiplyingFactor, 50*fltMultiplyingFactor);
            }
            else{
                return CGSizeMake(152*fltMultiplyingFactor, 152*fltMultiplyingFactor);
            }
        }
        
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        if (section==0) {
            return 1;
        }
        else{
            if (usersPost.count) {
                if (isMoreData) {
                    return usersPost.count+1;
                }
                else{
                    return usersPost.count;
                }
            }
            else{
                return 0;
            }
        }
        
    }
    else{
        if (section==0) {
            return 1;
        }
        else{
            if (taggedPost.count) {
                if (isMoreData) {
                    return taggedPost.count+1;
                }
                else{
                    return taggedPost.count;
                }
            }
            else{
                return 0;
            }
        }
    }
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section!=0) {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else{
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
}
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        if (indexPath.section==0) {
            ProfileUpperViewCell *cell=(ProfileUpperViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileUpperViewCell" forIndexPath:indexPath];
            
            if ([self.userDetails objectForKey:@"total_following"]) {
                cell.lblFollowerNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_follower"]];
            }
            if ([self.userDetails objectForKey:@"total_following"]) {
                cell.lblFollowingNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_following"]];
            }
            if ([self.userDetails objectForKey:@"total_posts"]) {
                cell.lblPostNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_posts"]];
            }
            
            if ([[self.userDetails objectForKey:@"user_image"] length]>0) {
                 NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[self.userDetails objectForKey:@"user_image"]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
                [cell.imgProfilepic setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
            }
            
            cell.imgProfilepic.layer.borderWidth = 2.0f;
            cell.imgProfilepic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
            cell.imgProfilepic.layer.masksToBounds = NO;
            cell.imgProfilepic.clipsToBounds = YES;
            cell.imgProfilepic.layer.cornerRadius = cell.imgProfilepic.frame.size.width/2;
            
            UITapGestureRecognizer *followTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followTapAction:)];
            [cell.viewFollower addGestureRecognizer:followTap];
            
            UITapGestureRecognizer *followingTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followingTapAction:)];
            [cell.viewFollowing addGestureRecognizer:followingTap];
            
            if (usersPost.count>0) {
                UITapGestureRecognizer *allPost=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(allPostTapAction:)];
                [cell.viewPost addGestureRecognizer:allPost];
                
            }
            
            [cell.segmentBtnOutlet addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
            self.segmentBtnOutlet= cell.segmentBtnOutlet;
            self.segmentImageOutlet=cell.segmentImageOutlet;
            self.followUnFollowBtnOutlet=cell.followUnfollowBtn;
            
            [cell.followUnfollowBtn addTarget:self action:@selector(followUnfollowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else{
            if (indexPath.item==usersPost.count)
            {
                ExploreCell *cell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SeeMoreCell" forIndexPath:indexPath];
                 cell.grayIndicatorOutlet.frame=CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, cell.grayIndicatorOutlet.frame.size.width, cell.grayIndicatorOutlet.frame.size.height);
                [cell.grayIndicatorOutlet startAnimating];
                return cell;
                
            }
            else{
                ProfileCollectionCell *myCell = (ProfileCollectionCell *)[collectionView
                                                                          dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                          forIndexPath:indexPath];
                NSDictionary *dict=[usersPost objectAtIndex:indexPath.row];
                   NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"post_image"]];
                //for caching image is necessory then use this.
                NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
                [myCell.imgPetOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_300"] success:nil failure:nil];
                return myCell;
            }
        }
        
    }
    else if(self.segmentBtnOutlet.selectedSegmentIndex==1){
        if (indexPath.section==0) {
            ProfileUpperViewCell *cell=(ProfileUpperViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileUpperViewCell" forIndexPath:indexPath];
            cell.lblFollowerNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_follower"]];
            cell.lblFollowingNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_following"]];
            cell.lblPostNumber.text=[NSString stringWithFormat:@"%@",[self.userDetails objectForKey:@"total_posts"]];
            
            if ([[self.userDetails objectForKey:@"user_image"] length]>0) {
                [cell.imgProfilepic setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.userDetails objectForKey:@"user_image"]]]]];
            }
            cell.imgProfilepic.layer.borderWidth = 2.0f;
            cell.imgProfilepic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
            cell.imgProfilepic.layer.masksToBounds = NO;
            cell.imgProfilepic.clipsToBounds = YES;
            cell.imgProfilepic.layer.cornerRadius = cell.imgProfilepic.frame.size.width/2;
            
            UITapGestureRecognizer *followTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followTapAction:)];
            [cell.viewFollower addGestureRecognizer:followTap];
            
            UITapGestureRecognizer *followingTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followingTapAction:)];
            [cell.viewFollowing addGestureRecognizer:followingTap];
            
            if (usersPost.count>0) {
                UITapGestureRecognizer *allPost=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(allPostTapAction:)];
                [cell.viewPost addGestureRecognizer:allPost];
            }
            
            return cell;
        }
        else{
            if (indexPath.item==taggedPost.count)
            {
                ExploreCell *cell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SeeMoreCell" forIndexPath:indexPath];
                 cell.grayIndicatorOutlet.frame=CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, cell.grayIndicatorOutlet.frame.size.width, cell.grayIndicatorOutlet.frame.size.height);
                [cell.grayIndicatorOutlet startAnimating];
                return cell;
                
            }
            else{
                ProfileCollectionCell *myCell = (ProfileCollectionCell *)[collectionView
                                                                          dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                          forIndexPath:indexPath];
                NSDictionary *dict=[taggedPost objectAtIndex:indexPath.row];
                NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"post_image"]];
                NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
                [myCell.imgPetOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_300"] success:nil failure:nil];
                return myCell;
            }
        }
        
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section!=0) {
        [self performSegueWithIdentifier:@"SinglePostVC" sender:indexPath];
    }
  }

-(void)allPostTapAction:(UITapGestureRecognizer *)sender{
 
    NSIndexPath*indxPath=[NSIndexPath indexPathForRow:0 inSection:1];
    [_collectionViewOutlet scrollToItemAtIndexPath:indxPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];

}
-(void)followTapAction:(UITapGestureRecognizer *)sender{
    if ([[self.userDetails objectForKey:@"total_follower"] integerValue]>0) {
        strScreenName=@"Follower";
        [self performSegueWithIdentifier:@"profileToFreind" sender:sender];
    }
    
}

-(void)followingTapAction:(UITapGestureRecognizer *)sender{
    if ([[self.userDetails objectForKey:@"total_following"] integerValue]>0) {
        strScreenName=@"Following";
        [self performSegueWithIdentifier:@"profileToFreind" sender:sender];
    }
}

#pragma mark Pagination
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            [self callForPostAndTaggedPost:self.newPage];
         }
        //Put your load more data method here...
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SinglePostVC"]) {
        NSIndexPath *indexPath=(NSIndexPath *)sender;
        SinglePostVC *singlePost=[segue destinationViewController];

        if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
            singlePost.postDataDetail=[usersPost objectAtIndex:indexPath.item];
            
        }else if (self.segmentBtnOutlet.selectedSegmentIndex==1){
            singlePost.postDataDetail=[taggedPost objectAtIndex:indexPath.item];
        }
    }else if ([[segue identifier] isEqualToString:@"profileToFreind"]) {
        FollowFollowerVC *followFolloerVC=[segue destinationViewController];
        followFolloerVC.strUserId=[self.userDetails objectForKey:@"user_id"];
         [followFolloerVC setStrScreenName:strScreenName];
    }
    else if ([[segue identifier] isEqualToString:@"AllPostForUsers"]) {
        FeedVC *feedVC=[segue destinationViewController];
        feedVC.allPostForUser=YES;
        feedVC.userProfileId=[self.userDetails objectForKey:@"user_id"];
     }
 }

#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATESEARCHBYNAME]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    self.userDetails=[[dataDict objectForKey:@"data"] objectAtIndex:0];
                    [self setUpViewForDisplay];
                    [self callForPostAndTaggedPost:1];
                    
                }
                else{
                 }
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEFOllOWUNFOLLOW]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [SVProgressHUD dismiss];
                     [self singleProfile];
                }
                else{
                 }
            }
            if ([nState isEqualToString:STATEALLPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    if (!isValidateData) {
                        
                        usersPost=[[NSMutableArray alloc]init];
                        usersPost=[dataDict objectForKey:@"data"];
                        
                    }
                    else{
                        usersPost=[[usersPost arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }else{
                        isMoreData=NO;
                    }
                    [self.collectionViewOutlet reloadData];
                }
                else{
                    //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                }
                [self.collectionViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATETAGGEDPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    
                    if (!isValidateData) {
                        taggedPost=[[NSMutableArray alloc]init];
                        taggedPost=[dataDict objectForKey:@"data"];
                        
                    }
                    else{
                        taggedPost=[[taggedPost arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    else{
                        isMoreData=NO;
                    }
                    
                }
                else{
                 }
                [self.collectionViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }

@end
