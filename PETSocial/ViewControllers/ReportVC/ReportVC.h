//
//  ReportVC.h
//  PETSocial
//
//  Created by Habibi on 12/25/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *reportTextView;

@property (nonatomic, readwrite) NSMutableDictionary *reportData;

@property (assign, nonatomic) NSInteger intUserId;
@property (assign, nonatomic) NSInteger intItemId;
@property (nonatomic, readwrite) BOOL textHasChanged;

@end
