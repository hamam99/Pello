//
//  ReportVC.m
//  PETSocial
//
//  Created by Habibi on 12/25/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import "ReportVC.h"
#import "UITextView+Placeholder.h"

@interface ReportVC ()

@end

@implementation ReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    self.reportData = [[NSMutableDictionary alloc] init];
    
    [self fetchReportCategory];
    
    [self registerForKeyboardNotifications];
    
    self.textHasChanged = NO;
    
    self.reportTextView.placeholder = @"Why this content need to be reported";
    self.reportTextView.placeholderColor = [UIColor lightGrayColor];
    self.reportTextView.textColor = [UIColor blackColor];
}
- (IBAction)categoryDidPushed:(UIButton*)sender {
    [self.reportData setValue:[NSString stringWithFormat:@"%ld", sender.tag] forKey:@"report"];
    
    for (int i=4;i<7;i++) {
        UIButton *tmp = [self.view viewWithTag:i];
        [tmp setImage:nil forState:UIControlStateNormal];
    }
    
    [sender setImage:[UIImage imageNamed:@"checkmark-icon.png"] forState:UIControlStateNormal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.reportTextView.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.reportTextView.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)fetchReportCategory
{
    [[Connection sharedConnectionWithDelegate:self] getReportCategory:nil];
}

- (void)showAlertViewWithTitle:(NSString*)title andMessage:(NSString*)message
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:message
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)sendReportDidPushed:(id)sender {
//    NSMutableDictionary *dictParams = [NSMutableDictionary dictionary];
//    [dictParams setObject:[NSString stringWithFormat:@"%ld",(long)_intUserId] forKey:@"user_id"];
//    [dictParams setObject:[NSString stringWithFormat:@"%ld",(long)_intItemId] forKey:@"item_id"];
    
    
    
    if (self.textHasChanged==NO) {
        [self showAlertViewWithTitle:@"PELLO" andMessage:@"Report text cannot be empty"];
        return;
    }
    
    if (self.reportTextView.text.length == 0) {
        [self showAlertViewWithTitle:@"PELLO" andMessage:@"Report text cannot be empty"];
        return;
    }
    
    if (![self.reportData objectForKey:@"report"]) {
        [self showAlertViewWithTitle:@"PELLO" andMessage:@"Report category cannot be empty"];
        return;
    }
    
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    [self.reportData setObject:[NSString stringWithFormat:@"%ld",(long)_intUserId] forKey:@"user_id"];
    [self.reportData setObject:[NSString stringWithFormat:@"%ld",(long)_intItemId] forKey:@"item_id"];
    [self.reportData setObject:@"photo" forKey:@"type"];
    [self.reportData setObject:self.reportTextView.text forKey:@"feedback"];
    [self.reportData setObject:@"127.0.0.1" forKey:@"ip_address"];
    
    [[Connection sharedConnectionWithDelegate:self] postReportPost:self.reportData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextView Delegate Methods
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if(textView.tag == 0) {
        //textView.text = @"";
        //textView.textColor = [UIColor blackColor];
        textView.tag = 1;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if([textView.text length] == 0)
    {
        self.textHasChanged = NO;
        //textView.text = @"Why this content need to be reported";
        //textView.textColor = [UIColor lightGrayColor];
        textView.tag = 0;
        
    }
    self.textHasChanged = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEREPORTABUSE]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [Alerts showAlertWithMessage:@"Post reported successfully" withBlock:nil andButtons:@"Ok", nil];
                    
                    // [[NSNotificationCenter defaultCenter] postNotificationName:DeletePost object:nil];
                }
                else{
                    // [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                }
                [SVProgressHUD dismiss];
            } else if ([nState isEqualToString:STATEREPORTPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    [Alerts showAlertWithMessage:@"Post reported successfully" withBlock:nil andButtons:@"Ok", nil];
                    
                    // [[NSNotificationCenter defaultCenter] postNotificationName:DeletePost object:nil];
                }
                else{
                    // [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                }
                [SVProgressHUD dismiss];
            } else if ([nState isEqualToString:STATEGETREPORTCATEGORY]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    NSLog(@"%@", dataDict);
                }
                else{
                    
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
        
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
    //    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:nData];
}

@end
