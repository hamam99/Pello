//
//  ShareSettingsViewController.h
//  PETSocial
//
//  Created by milap kundalia on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareSettingsViewController : UIViewController
- (IBAction)backBtnAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnFacebook;
- (IBAction)btnFacebookAction:(id)sender;

//Prashant 8/7/15
@property (strong, nonatomic) IBOutlet UIButton *btnTwitter;
- (IBAction)btnTwitterAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblTwitterStatus;
//-----

@property (strong, nonatomic) IBOutlet UILabel *lblFacebookStatus;
@end
