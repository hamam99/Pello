//
//  ShareSettingsViewController.m
//  PETSocial
//
//  Created by milap kundalia on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "ShareSettingsViewController.h"
#import <FacebookSDK/FacebookSDK.h>
@interface ShareSettingsViewController ()

@end

@implementation ShareSettingsViewController
@synthesize lblFacebookStatus,btnFacebook;

@synthesize btnTwitter,lblTwitterStatus;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    if ([FBSession activeSession].isOpen)
    {
        lblFacebookStatus.text=@"Connected";
     }
    else
    {
        lblFacebookStatus.text=@"Not Connected";
     }
    
 }
-(void)viewDidAppear:(BOOL)animated
{
   
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btnFacebookAction:(id)sender
{
    if (!FBSession.activeSession.isOpen)
    {
        [FBSession openActiveSessionWithAllowLoginUI: YES];
         lblFacebookStatus.text=@"Connected";
    }
    else
    {
         [Alerts showAlertWithMessage:@"Unlink your Facebook account?" withBlock:^(NSInteger buttonIndex) {
            
            switch (buttonIndex)
            {
                case 0:
                {
                    FBSession* session = [FBSession activeSession];
                    [session closeAndClearTokenInformation];
                    [session close];
                    [FBSession setActiveSession:nil];
                    //
                    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://m.facebook.com/"]];
                    
                    for (NSHTTPCookie* cookie in facebookCookies) {
                        [cookies deleteCookie:cookie];
                    }
                    
                    [FBSession.activeSession closeAndClearTokenInformation];
                    lblFacebookStatus.text=@"Not Connected";
                    
                }
                    break;
                case 1:
                {
                  
                }
                    break;
                    
                default:
                    break;
            }
            
        } andButtons:@"Yes,I'm sure",@"Cancel", nil];
    }
}


-(void)viewWillDisappear:(BOOL)animated{
}
-(void)StatuswithNotification:(NSNotification *)notification{
    if ([FBSession activeSession].isOpen)
    {
        lblFacebookStatus.text=@"Connected";
     }
    else
    {
        lblFacebookStatus.text=@"Not Connected";
     }
}
-(void)Status
{
    
}

#pragma mark UIAlerView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex ==0) {
        
    }else if (buttonIndex ==1){
        
    }
}

@end
