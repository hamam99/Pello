//
//  Terms&ConditionsViewController.m
//  PETSocial
//
//  Created by milap kundalia on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "Terms&ConditionsViewController.h"

@interface Terms_ConditionsViewController ()

@end

@implementation Terms_ConditionsViewController
@synthesize activityLoader,webviewTermsAndCondition;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    self.tabBarController.tabBar.hidden=YES;
    [APPDELEGATE imagefooter].hidden=YES;
    NSString *strHTMLFilePath=[[NSBundle mainBundle] pathForResource:@"termsAndCondition" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:strHTMLFilePath encoding:NSUTF8StringEncoding error:nil];
    [webviewTermsAndCondition loadHTMLString:htmlString baseURL:nil];
    [activityLoader startAnimating];
    // Do any additional setup after loading the view.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnAction:(UIButton *)sender{
    self.tabBarController.tabBar.hidden=NO;
    [APPDELEGATE imagefooter].hidden=NO;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [activityLoader stopAnimating];
}
@end
