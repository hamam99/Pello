//
//  FeedCommentCell.h
//  PETSocial
//
//  Created by Habibi on 12/21/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCommentText;

@end
