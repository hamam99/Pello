//
//  FeedCommetViewMoreTableViewCell.h
//  PETSocial
//
//  Created by Habibi on 1/8/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCommetViewMoreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *viewAllLabel;
@property (nonatomic, readwrite) NSInteger item_id;
@end
