//
//  FeedHeaderView.h
//  PETSocial
//
//  Created by Habibi on 12/21/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *timeButton;
@end
