//
//  TimerArcView.h
//  PETSocial
//
//  Created by Habibi on 12/23/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerArcView : UIView
@property (nonatomic, readwrite) NSInteger percent;
@end
