//
//  TimerArcView.m
//  PETSocial
//
//  Created by Habibi on 12/23/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import "TimerArcView.h"

@interface TimerArcView () {
    CGFloat startAngle;
    CGFloat endAngle;
}
@end

@implementation TimerArcView

// Refer to : ttp://stackoverflow.com/questions/13573676/circular-progress-bars-in-ios

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 1.5;
        endAngle = startAngle + (M_PI * 2);
        
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    // Display our percentage as a string
    //NSString* textContent = [NSString stringWithFormat:@"%ld", (long)self.percent];
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    NSInteger normalPercent = 100 - self.percent;
    
    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:36
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (normalPercent / 100.0) + startAngle
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 7.5;
    UIColor *strokeColor = [UIColor colorWithRed:14.0f/255.0f green:98.0f/255.0f blue:129.0f/255.0f alpha:1.0f];
    [strokeColor setStroke];
    [bezierPath stroke];
    
    // Text Drawing
    //CGRect textRect = CGRectMake((rect.size.width / 2.0) - 71/2.0, (rect.size.height / 2.0) - 45/2.0, 71, 45);
    //[[UIColor blackColor] setFill];

    /*UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:42.5];
    // Make a copy of the default paragraph style
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [textContent drawInRect:textRect withAttributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle}];*/
}


@end
