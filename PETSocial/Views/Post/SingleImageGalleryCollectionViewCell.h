//
//  SingleImageGalleryCollectionViewCell.h
//  PETSocial
//
//  Created by Habibi on 1/20/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleImageGalleryCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *petImageView;
@property (weak, nonatomic) IBOutlet UIView *selectedLayerView;

@end
