//
//  PostTableViewCell.m
//  PETSocial
//
//  Created by Habibi on 2/3/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import "PostTableViewCell.h"

@implementation PostTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)awakeFromNib
{
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(playerItemDidReachEnd:)
     name:AVPlayerItemDidPlayToEndTimeNotification
     object:[self.avPlayer currentItem]];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

@end
